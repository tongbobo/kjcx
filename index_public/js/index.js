function customDropDown(ele){
	this.dropdown=ele;
	this.placeholder=this.dropdown.find(".placeholder");
	this.options=this.dropdown.find("ul.dropdown-menu > li,ul.dropdown-menu1 > li,ul.dropdown-menu2 > li,ul.dropdown-menu3 > li,ul.dropdown-menu4 > li");
	this.val='';
	this.index=-1;//默认为-1;
	this.initEvents();
}
customDropDown.prototype={
	initEvents:function(){
		var obj=this;
		//这个方法可以不写，因为点击事件被Bootstrap本身就捕获了，显示下面下拉列表
		obj.dropdown.on("click",function(event){
			$(this).toggleClass("active");
		});
		
		//点击下拉列表的选项
		obj.options.on("click",function(){
			var opt=$(this);
			obj.text=opt.find("a").text();
			obj.val=opt.attr("value");
			obj.index=opt.index();
			obj.placeholder.text(obj.text);
		});
	},
	getText:function(){
		return this.text;
	},
	getValue:function(){
		return this.val;
	},
	getIndex:function(){
		return this.index;
	}
}
$(document).ready(function(){
	var mydropdown=new customDropDown($("#dropdown1"));
	var mydropdown2=new customDropDown($("#dropdown2"));
	var mydropdown3=new customDropDown($("#dropdown3"));
	var mydropdown4=new customDropDown($("#dropdown4"));
	var mydropdown5=new customDropDown($("#dropdown5"));
});
//用于图坐标等分
function getMax(str){
	var max1 = parseInt(Math.max.apply(null, str));//获取最大值
	var len = max1.toString().length; 
	var first = max1.toString().substring(0,1);
	var result = new Array();
	if(first>=5){
		first = 10;
	}else{
		first=5;
	}
	var max = first*(Math.pow(10,len-1));
	var inteval = Math.ceil(max/5);
	result['max'] = max;
	result['inteval'] = inteval;
	return result;
}