$(document).ready(function () {
//backtop
    $(window).scroll( function() {
        var height=$(this).scrollTop();
        // console.log(height);
        if( height > 100 ){
            $(".backtop").css("display","block");
        }else if(height < 100){
            $(".backtop").css("display","none");
        }
    });

    $(".backtop").click(function () {
        $('body,html').animate({scrollTop:0},1000);
    });

// 固定头部搜索框
//     $(".fixsearch").focusin(function () {
//         $(this).parents(".fix-search-con").addClass("is_focused");
//     });
//     $(".fixsearch").focusout(function () {
//         $(this).parents(".fix-search-con").removeClass("is_focused");
//     });

//聚类
    $(".icon-rights").click(function () {
        $(".main-right").toggleClass("fixed");
    });

// show-more
    $(".choses").each(function () {
        var listNum = $(this).find(".chose-line-con").find(".chose-line");
        var $more = $(this).find(".show-more");
        if (listNum.length < 6) {
            $more.hide();
        } else {
            listNum.eq(4).nextAll().hide();
        }
    });

    $(".show-more").click(function () {
        var $li = $(this).parent().find(".chose-line");
        var count = $li.length;
        if (count >= 5) {
            $li.eq(4).nextAll().toggle();
        }
        if (!$(this).parent().find(".chose-line").is(":hidden")) {
            $(this).text("收起");
        } else {
            $(this).text("更多");
        }

    });

    $(".chose-tit").click(function () {
        $(this).siblings().toggle();
    });


//login 首屏中间高度
    var foot_h=$(".foot").height();
    var w_h=$(window).height()- 60 -foot_h;
    $(".login-warp").height(w_h);

//checkbox
    $(".check").click(function () {
        $(this).toggleClass("open");
        var len=$(".outputs-line .check").length;
        var lened=$(".outputs-line .check.open").length;
        if(len == lened){
            $(".checkall").addClass("open");
        }else{
            $(".checkall").removeClass("open");
        }
    });

    $(".checkall").click(function () {
        $(this).toggleClass("open");
       if($(this).hasClass("open")){
           $(this).parent().parent().find(".outputs-line .check").addClass("open");
       }else{
           $(this).parent().parent().find(".outputs-line .check").removeClass("open");
       }
    });

//colle
    $(".colle").click(function () {
        $(this).toggleClass("colle-active");
    })






})