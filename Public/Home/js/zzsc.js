//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches
$(".next").click(function(){//第一页继续填写按钮
    var fanwei=$(":checked[name='cxfw']").val();//查新范围按钮选择
    var en_name=$("input[name='xmmcen']").val();//项目名称英文
    var t1=$("input[name='xmmc']").val();
    var t3=$("input[name='wtrname']").val();
    var t4=$("input[name='wtrphone']").val();//联系方式
    var t5=$("input[name='lxrname']").val();
    var t6=$("input[name='phone']").val();//联系方式
    var t7=$("input[name='wtdw']").val();
    var t8=$("input[name='txdz']").val();
    //var t9=$("input[name='lxryzbm']").val();
    var t0=$("input[name='wtremail']").val();//邮箱
    if(fanwei=='2'||fanwei=='3'){
        if(!en_name||!t1||!t3||!t4||!t5||!t6||!t7||!t8||!t0) {
            alert('请填写完整带*号必填字段！')
        }else{
            if(animating) return false;
            animating = true;
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //activate next step on progressbar using the index of next_fs
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50)+"%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale('+scale+')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 600,
                complete: function(){
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        }
    }else{
        if(!t1||!t3||!t4||!t5||!t6||!t7||!t8||!t0) {
            alert('请填写完整带*号必填字段！')
        }else{
            if(animating) return false;
            animating = true;
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //activate next step on progressbar using the index of next_fs
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50)+"%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale('+scale+')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 600,
                complete: function(){
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        }
    }
});
$(".next_2").click(function(){//第二页继续填写按钮
    var m1=$("input[name='cxmd[]']:checked").val();
    var m2=$("textarea[name='xxcxmd']").val()//详细查新目的
    var m4=$("input:radio[name='xmjb']").val();
    var m5=$("textarea[name='show_add_cxd']").val();
    var m6=$("input:text[name='lqrq']").val();
    var m7=$("textarea[name='jsd']").val();
    var m8=$("input[name='cxyq[]']:checked").val();
    if(m1=='1'){
        if(!m2||!m4||!m5||!m6||!m7||!m8) {
            alert('请填写完整带*号必填字段！')
        }else{
            if(animating) return false;
            animating = true;
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //activate next step on progressbar using the index of next_fs
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50)+"%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale('+scale+')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 600,
                complete: function(){
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        }
    }else{
        if(!m1||!m4||!m5||!m6||!m7||!m8) {
            alert('请填写完整带*号必填字段！')
        }else{
            if(animating) return false;
            animating = true;
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //activate next step on progressbar using the index of next_fs
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50)+"%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale('+scale+')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 600,
                complete: function(){
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        }
    }
});

$(".previous").click(function(){//返回按钮
	if(animating) return false;
	animating = true;

	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();

	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

	//show the previous fieldset
	previous_fs.show();
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		},
		duration: 000,
		complete: function(){
			current_fs.hide();
			animating = false;
		},
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
})
function xk(){//失去光标就判断。选择了学科就不得输入其他学科
    $("option:selected").each(function(){
        var xk_select=$(this).val();
        var xk_qt=$("input[name='qtxk']").val();
        if(xk_select){
            $(".xk").attr({disabled:true});
        }else{
            $(".xk").removeAttr("disabled");
        }
        if(xk_qt){
            $(".xk_s").attr({disabled:true});
        }else{
            $(".xk_s").removeAttr("disabled");
        }
    });
}

$(function(){
    $(":radio[name='qbfs']").click(function(){//领取报告方式
        var r1=$(this).val();
        if(r1=='2'){
            $(".address").removeAttr("disabled");
            $(".address").attr({required:true});
        }else{
            $(".address").attr({disabled:true});
            $(".address").removeAttr("required");
        }
    });
    $(":radio[name='dwsh']").click(function(){//是否需要发票
        var r2=$(this).val();
        if(r2=='2'){
            $(".fapiao").removeAttr("disabled");
            $(".fapiao").attr({required:true});
        }else{
            $(".fapiao").attr({disabled:true});
            $(".fapiao").removeAttr("required");
        }
    });
    $(":radio[name='fkfs']").click(function(){//缴费方式
        var r3=$(this).val();
        if(r3=='2'){
            $(".pingzheng").removeClass("hidden");
            //$(".file_1").attr({required:true});
        }else{
            $(".pingzheng").addClass("hidden");
            //$(".file_1").removeAttr("required");
        }
    });
    $(":radio[name='cxfw']").click(function(){//查新范围选中国外、国内外提示填写项目英文名称以及外文检索字段为必填
        var fanwei=$(this).val();
        $("input[name='glw']").val(fanwei);
        if(fanwei=='2'||fanwei=='3'){
            var en_name=$("input[name='xmmcen']").val();
            if(en_name==''){
                alert('请填写项目英文名称、外文检索词！')
            }
            $(".waiwen").attr({required:true});
        }else{
            $(".waiwen").removeAttr("required");
        }
    });
    $(":checkbox[name='cxmd[]']").click(function(){//选中科研立项提示填写查新目的
        var kylx=$(this).val();
        if(kylx=='1'){
            var m2=$("textarea[name='xxcxmd']").val()
            if(m2==''){
                alert('请填写详细查新目的！')
            }
        }
    });
});
function showcontent(){//委托人姓名和委托人签名同步
    document.getElementById("second").value=document.getElementById("first").value;
}


