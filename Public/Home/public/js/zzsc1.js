
 $(function(){

     $("#bu1").click(function(){
         $("#buzhou1").show();
         $("#buzhou2").hide();
         $("#buzhou3").hide();
         $("#bu2").removeClass("active");
         $("#bu3").removeClass("active");
		 $("#bu1").addClass("active");
     });
     $("#bu2").click(function(){
         if(first_form()) {
             $("#buzhou1").hide();
             $("#buzhou2").show();
             $("#buzhou3").hide();
             $("#bu1").removeClass("active");
             $("#bu3").removeClass("active");
             $("#bu2").addClass("active");
         }
     });

     $("#bu3").click(function(){
         if(first_form() && second_form()){
             $("#buzhou1").hide();
             $("#buzhou3").show();
             $("#buzhou2").hide();
             $("#bu1").removeClass("active");
             $("#bu2").removeClass("active");
             $("#bu3").addClass("active");
         }
     });
 })

    //替换提示框
     function msg_alert(msg,obj){
         layer.confirm(msg, {
             btn: ['确认'] //按钮
         }, function(){
             layer.msg('好的', {icon: 1});
             if(obj){
                 obj.focus();
             }
         }, function(){

         });
     }
     //第一页继续填写按钮判断
     function first_form(){
         var fanwei =$("input[name='xmmc']");//项目名称中文
         var name   =$("input[name='xmmcen']");//项目名称英文
         var t3     =$("input[name='wtrname']");
         var t4     =$("input[name='wtrphone']");//联系方式
         var t5     =$("input[name='lxrname']");
         var t6     =$("input[name='phone']");//联系方式
         var t7     =$("input[name='wtdw']");
         var t8     =$("input[name='txdz']");
         var t9     =$("input[name='wtremail']");
         var guo    = $('input[name="cxfw"]:checked').val();
         var sub    = $('select[name="xkfl"]');

         if(fanwei.val() == ''){
             msg_alert('请填写项目中文名称',fanwei);
             return false;
         }
         if(guo != 1){  //点击的是国内
             if(name.val() == ''){
                 msg_alert('请填写项目英文名称',name);
                 return false;
             }
         }
        // if(typeof(sub.val()) == 'undefined'){
         //    msg_alert('请选择所属学科');
         //    return false;
        // }
         if(t3.val() == ''){
             msg_alert('请填写委托人姓名',t3);
             return false;
         }
         var num = /^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57])[0-9]{8}$/;
         //if(!num.test(t4.val())){
         //    msg_alert('请填写正确联系方式', t4);
         //    return false;
         //}
         if(t5.val() == ''){
             msg_alert('请填写联系人姓名',t5);
             return false;
         }
         //if(!num.test(t6.val())){
          //   msg_alert('请填写正确联系人方式',t6);
          //   return false;
        // }
         if(t7.val() == ''){
             msg_alert('请填写委托单位',t7);
             return false;
         }
         if(t8.val() == ''){
             msg_alert('请填写通讯地址',t8);
             return false;
         }
         var email  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
         if(!email.test(t9.val())){
             msg_alert('请填写正确联系邮箱',t9);
             return false;
         }
         $("#buzhou1").hide();
         $("#buzhou2").show();
         $("#buzhou3").hide();
         $("#bu1").removeClass("active");
         $("#bu3").removeClass("active");
         $("#bu2").addClass("active");

     }
     //第二页判断
     function second_form(){
        var show1   = $("#show_add_cxd");
        var jsd    = $("#jsd");
        var xxcxmd = $("input[name='xxcxmd']");
        var lqrq   = $("input[name='lqrq']").val();
        var cxmd   = $("input[name='cxmd']:checked");
        var cxyq   = $("input[name='cxyq[]']:checked");
         if(!cxmd.length){
             msg_alert('请选择查新目的');
             return false;
         }
         //判断是否要输入目的
         if($('.show_action').attr('data_dd') == 2){
             //if(xxcxmd.val()==''){
             //    msg_alert('请填写查新目的',xxcxmd);
              //   return false;
             //}
         }
         if(show1.val() == ''){
             msg_alert('请填写查新点',show1);
             return false;
         }
         if(jsd.val() == ''){
             msg_alert('请填写技术要求',jsd);
             return false;
         }
         if(!cxyq.length){
             msg_alert('请选择查新要求');
             return false;
         }
         var cxss = $('#qtyq');
         if(cxyq.val() == 3 && cxss.val() == ''){
             msg_alert('请填写查新委托人的其它愿望');
             return false;
         }
         if(lqrq == ''){
             msg_alert('请填写完成日期');
             return false;
         }

         $("#buzhou1").hide();
         $("#buzhou3").show();
         $("#buzhou2").hide();
         $("#bu2").removeClass("active");
         $("#bu1").removeClass("active");
         $("#bu3").addClass("active");
     }

    //最后提交按钮
    function last_form(){
        var ss = 1;
        var keywords_cn = $('textarea[name="keywords_cn"]');
        var keywords_en = $('textarea[name="keywords_en"]')
        var dwsh        = $("input[name='dwsh']:checked").val();
        var qbfs        = $("input[name='qbfs']:checked").val();
        var upload_icoA = $('#upload_icoA').val();
        var guo         = $('input[name="cxfw"]:checked').val();
        if(keywords_cn.val() == ''){
            msg_alert('请填写中文检索词',keywords_cn);
            ss = 0;
        }
        if(guo != 1){
            if(keywords_en.val() == ''){
                msg_alert('请填写外文检索词',keywords_en);
                keywords_en.focus();
                ss = 0;
            }
        }
        if($("input[name='fkfs']:checked").val() == 2) {
            if (upload_icoA == '') {
                msg_alert('请上传汇款凭证');
                ss = 0;
            }
        }
        if(dwsh == 2){
            if($("input[name='kpdw']").val() == ''){
                msg_alert('请填写发票开头', $("input[name='kpdw']"));
                ss = 0;
            }
        }
        if(qbfs == 2){
            if($("input[name='kddz']").val() == ''){
                msg_alert('请填写收件地址',$("input[name='kddz']"));
                ss = 0;
            }
        }
        return ss;
    }
 //科研目的
 function research (obj) {
	 return false;
     var name= $(obj).attr('data_dd');
     if(name==1){
         msg_alert('请填写详细查新目的');
         $('#xxcxmd').show();
         $(obj).attr('data_dd','2')
     }else{
         $('#xxcxmd').hide();
         $(obj).attr('data_dd','1')
     }
 }


function xk(){//失去光标就判断。选择了学科就不得输入其他学科
    $("option:selected").each(function(){
        var xk_select=$(this).val();
        var xk_qt=$("input[name='qtxk']").val();
        if(xk_select){
            $(".xk").attr({disabled:true});
        }else{
            $(".xk").removeAttr("disabled");
        }
        if(xk_qt){
            $(".xk_s").attr({disabled:true});
        }else{
            $(".xk_s").removeAttr("disabled");
        }
    });
}

 function xk_(){
     $("option:selected").each(function(){
         var xk_select=$(this).val();
         var xk_qt=$("input[name='show_cx_type']").val();
         if(xk_select){
             $(".xk").attr({disabled:true});
         }else{
             $(".xk").removeAttr("disabled");
         }
         if(xk_qt){
             $(".xk_s").attr({disabled:true});
         }else{
             $(".xk_s").removeAttr("disabled");
         }
     });
 }
 //显示中英文
 function click_name(){
     var name = $("input[name='cxfw']:checked").val();
     if(name == 1){
         $('.zhong_name').show();
         $('.wai_name').hide();
     }else if(name == 2){
         $('.zhong_name').show();
         $('.wai_name').show();
     }else{
         $('.zhong_name').show();
         $('.wai_name').show();
     }
 }

 //显示发票
 function fp_show(){
     var radio_val = $('input[name="dwsh"]:checked').val();
     if(radio_val==2){
         $('.fpxx_edit').show();
     }else{
         $('.fpxx_edit').hide();
     }

 }
 //报告领取
 function bg_show(){
     var radio_val = $('input[name="qbfs"]:checked').val();
     if(radio_val==2){
         $('input[name="kddz"]').show();
     }else{
         $('input[name="kddz"]').hide();
     }

 }
 //科研目的
 function research (obj) {
	 return false;
     var name= $(obj).attr('data_dd');
     if(name==1){
         msg_alert('请填写详细查新目的');
         $('#xxcxmd').show();
         $('.xxcxmd').attr('required',true);
         $(obj).attr('data_dd','2')
     }else{
         $('#xxcxmd').hide();
         $('#xxcxmd').removeAttr('required');
         $(obj).attr('data_dd','1')
     }
 }

 //同步显示姓名
 function show_content () {
     $('#second').val($('#first').val());
 }


