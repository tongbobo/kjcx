/**
 * Created by kevin on 2015/11/4.
 */
;$(function() {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "1500",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    //全选的实现
    $(".check-all").click(function(){
        $(".ids").prop("checked", this.checked);
    });
    $(".ids").click(function(){
        var option = $(".ids");
        option.each(function(i){
            if(!this.checked){
                $(".check-all").prop("checked", false);
                return false;
            }else{
                $(".check-all").prop("checked", true);
            }
        });
    });

    //ajax get请求
    $('.ajax-get').click(function(){
        var target;
        var that = this;
        if ( $(this).hasClass('confirm') ) {
            if(!confirm('确认要执行该操作吗?')){
                return false;
            }
        }
		
        if ( (target = $(this).attr('href')) || (target = $(this).attr('url')) ) {
            $.get(target).success(function(data){
				var data_new=JSON.parse(data);
				//console.log(typeof data_new);
				//console.log(data_new['state']);return false;
                if (data_new['state']=='1') {
					
                    if (data.url) {
                        toastr.success(data.info + ' 页面即将自动跳转~','操作成功');
                    }else{
                        toastr.success(data.info,'操作成功');
                    }
                    setTimeout(function(){
                        if (data.url) {
                            location.href=data.url;
                        }else if( $(that).hasClass('no-refresh')){
                            $('#top-alert').find('button').click();
                        }else{
                            location.reload();
                        }
                    },1500);
                }else{
                    //toastr.error(data.info,'123');
                    setTimeout(function(){
                        if (data.url) {
                            location.href=data.url;
                        }else{
                            $('#top-alert').find('button').click();
                        }
                    },1500);
                }
            });

        }
        return false;
    });




    //ajax post submit请求
    $('.ajax-post').click(function(){
        var target,query,form;
        var target_form = $(this).attr('target-form');
        var that = this;
        var nead_confirm=false;
        if( ($(this).attr('type')=='submit') || (target = $(this).attr('href')) || (target = $(this).attr('url')) ){
            form = $('.'+target_form);

            if ($(this).attr('hide-data') === 'true'){//无数据时也可以使用的功能
                form = $('.hide-data');
                query = form.serialize();
            }else if (form.get(0)==undefined){
                return false;
            }else if ( form.get(0).nodeName=='FORM' ){
                if ( $(this).hasClass('confirm') ) {
                    if(!confirm('确认要执行该操作吗?')){
                        return false;
                    }
                }
                if($(this).attr('url') !== undefined){
                    target = $(this).attr('url');
                }else{
                    target = form.get(0).action;
                }
                query = form.serialize();
            }else if( form.get(0).nodeName=='INPUT' || form.get(0).nodeName=='SELECT' || form.get(0).nodeName=='TEXTAREA') {
                form.each(function(k,v){
                    if(v.type=='checkbox' && v.checked==true){
                        nead_confirm = true;
                    }
                })
                if ( nead_confirm && $(this).hasClass('confirm') ) {
                    if(!confirm('确认要执行该操作吗?')){
                        return false;
                    }
                }
                query = form.serialize();
            }else{
                if ( $(this).hasClass('confirm') ) {
                    if(!confirm('确认要执行该操作吗?')){
                        return false;
                    }
                }
                query = form.find('input,select,textarea').serialize();
            }
            //此处特殊位置用于查新委托提交时的判断
            var ss = 1
            var sub = $(this).attr('target-submit');
            if(sub == 'submit'){
               ss = last_form();
            }
            if(ss == 1){
                $(that).addClass('disabled').attr('autocomplete','off').prop('disabled',true);
                $.post(target,query).success(function(data){
                    if (data.status==1) {
                        if (data.url) {
                            toastr.success(data.info + ' 页面即将自动跳转~','操作成功');
                        }else{
                            toastr.success(data.info ,'操作成功');
                        }
                        setTimeout(function(){
                            $(that).removeClass('disabled').prop('disabled',false);
                            if (data.url) {
                                location.href=data.url;
                            }else if( $(that).hasClass('no-refresh')){
                                $('#top-alert').find('button').click();
                            }else{
                                location.reload();
                            }
                        },1500);
                    }else{
                        toastr.warning(data.info,'出错信息');
                        setTimeout(function(){
                            $(that).removeClass('disabled').prop('disabled',false);
                            if (data.url) {
                                location.href=data.url;
                            }else{
                                $('#top-alert').find('button').click();
                            }
                        },1500);
                    }
                });
            }

        }
        return false;
    });
})



//ajax post submit请求
$('.ajax-post-form').click(function(){
    var target,query,form;
    var target_form = $(this).attr('target-form');
    var that = this;
    var nead_confirm=false;
    if( ($(this).attr('type')=='submit') || (target = $(this).attr('href')) || (target = $(this).attr('url')) ){
        form = $('.'+target_form);

        if ($(this).attr('hide-data') === 'true'){//无数据时也可以使用的功能
            form = $('.hide-data');
            query = form.serialize();
        }else if (form.get(0)==undefined){
            return false;
        }else if ( form.get(0).nodeName=='FORM' ){
            if ( $(this).hasClass('confirm') ) {
                if(!confirm('确认要执行该操作吗?')){
                    return false;
                }
            }
            if($(this).attr('url') !== undefined){
                target = $(this).attr('url');
            }else{
                target = form.get(0).action;
            }
            query = form.serialize();
        }else if( form.get(0).nodeName=='INPUT' || form.get(0).nodeName=='SELECT' || form.get(0).nodeName=='TEXTAREA') {
            form.each(function(k,v){
                if(v.type=='checkbox' && v.checked==true){
                    nead_confirm = true;
                }
            })
            if ( nead_confirm && $(this).hasClass('confirm') ) {
                if(!confirm('确认要执行该操作吗?')){
                    return false;
                }
            }
            query = form.serialize();
        }else{
            if ( $(this).hasClass('confirm') ) {
                if(!confirm('确认要执行该操作吗?')){
                    return false;
                }
            }
            query = form.find('input,select,textarea').serialize();
        }
        $(that).addClass('disabled').attr('autocomplete','off').prop('disabled',true);
        $.post(target,query).success(function(data){
            if (data.status==1) {
                if (data.url) {
                    toastr.success(data.info + ' 页面即将自动跳转~','操作成功');
                }else{
                    toastr.success(data.info ,'操作成功');
                }
                setTimeout(function(){
                    $(that).removeClass('disabled').prop('disabled',false);
                    if (data.url) {
                        location.href=data.url;
                    }else if( $(that).hasClass('no-refresh')){
                        $('#top-alert').find('button').click();
                    }else{
                        location.reload();
                    }
                },1500);
            }else{
                toastr.warning(data.info,'出错信息');
                setTimeout(function(){
                    $(that).removeClass('disabled').prop('disabled',false);
                    if (data.url) {
                        location.href=data.url;
                    }else{
                        $('#top-alert').find('button').click();
                    }
                },1500);
            }
        });
    }
    return false;
});
//导航高亮
function highlight_subnav(url) {
    $('#side-menu').find('a[href="' + url + '"]').parent('li').each(function(){
        $(this).addClass('active').parent('ul').parent('li').addClass('active');
    });
}

//用于新版的高亮
function highlight_nav3(url){
    $('.left-panelbar').find('a[href="'+url+'"]').closest('li').addClass('active');
    //$('.side-sub-menu-hight').find('a[href="'+url+'"]').closest('ul').css('display','block');
    //$('.side-sub-menu-hight').find('a[href="'+url+'"]').parents('li').closest('li').addClass('open');
}
//用于新版的高亮
function highlight_nav2(url){
    $('.side-sub-menu-hight').find('a[href="'+url+'"]').closest('li').addClass('active');
    $('.side-sub-menu-hight').find('a[href="'+url+'"]').closest('ul').css('display','block');
    $('.side-sub-menu-hight').find('a[href="'+url+'"]').parents('li').closest('li').addClass('open');
}


function send_url_message(url){
    var a=document.createElement("script");
    a.setAttribute("type","text/javascript");
    a.setAttribute("src",url);
    document.body.appendChild(a);
}
//刷新
function simpleLoad(btn, state) {
    if (state) {
        btn.children().addClass('fa-spin');
        btn.contents().last().replaceWith(" 读取中");
    } else {
        setTimeout(function () {
            btn.children().removeClass('fa-spin');
            btn.contents().last().replaceWith("刷新");
        }, 2000);
    }
}

// 弹出层
$.extend({

    /**
     * alert弹出信息框，与layer一致
     * @param alertMsg 信息内容（文本）
     * @param alertType 提示图标（整数，0-10的选择）
     * @param alertTit 标题（文本）
     * @param alertYes 按钮的回调函数
     * @returns
     */
    alertPlus: function (alertMsg , alertType, alertTit , alertYes) {
        return layer.alert(alertMsg,alertType,alertTit,alertYes);
    },

    /***
     * layer 关闭弹出层
     * index 索引值
     */
    closeDialog: function (index) {
        return layer.close(index);
    },

    /**
     * 打开弹出层的方法
     * 0：信息框（默认），1：页面层，2：iframe层，3：加载层，4：tips层。
     * */
    alertDialog: function (setting) {
        return $.layer(setting);
    },
    /***
     *conMsg：信息内容（文本）
     * conYes：按钮一回调
     * conTit：标题（文本）
     * conNo : 关闭按钮回调(函数)
     */
    layerConfirm: function (conMsg  , conYes , conTit , conNo) {
        return layer.confirm(conMsg,conYes,conTit,conNo);
    },
    /**
     * 加载tip层
     * content ：文本内容
     * follow : 要吸引的dom对象, 对象
     * parme : parme允许传这些属性{time: 自动关闭所需秒,
         * maxWidth: 最大宽度, guide: 指引方向, style: tips样式（参加api表格一中的style属性）
         */
    loadTip: function (content, follow, parme) {
        return layer.tips(content,follow,parme);
    },
    /***
     * 关闭 tip 层
     */
    closeTip: function () {
        return layer.closeTips();
    },
    /***
     * 加载层
     *loadTime：自动关闭所需等待秒数(0时则不自动关闭)，
     *loadgif：加载图标（整数，0-3的选择），
     *loadShade：是否遮罩（true 或 false）
     */
    loadTier: function (loadTime ,loadgif ,loadShade) {
        layer.load(loadTime,loadgif,loadShade);
    },
    /**
     * 修改层的标题
     * @param content 内容
     * @param index 层索引
     */
    updateTitle: function (content, index) {
        layer.title(content,index);
    }

});

//标签页切换
function showTab() {
    $(".tab-nav1 li").click(function(){
        var self = $(this), target = self.data("tab");
        self.addClass("current").siblings(".current").removeClass("current");
        window.location.hash = "#" + target.substr(3);
        $(".tab-pane.in").removeClass("in");
        $("." + target).addClass("in");
    }).filter("[data-tab=tab" + window.location.hash.substr(1) + "]").click();
}

