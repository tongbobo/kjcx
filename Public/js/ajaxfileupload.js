function ajaxFileUpload(tmp_name,tmp_can,tmp_u,x){
	
	if(tmp_can==1){
		var tmp_url = "&can=1";
		var na		= '查看图片';
	}else{
		var tmp_url = '';
		var na		= '查看文档';
	}
	if(tmp_u == 1){
		var url = "../../upload/?tmp_name="+tmp_name+tmp_url;
	}else{
		var url = "./upload/?tmp_name="+tmp_name+tmp_url;
	}
	$.ajaxFileUpload({
		id:tmp_name,
		url:url,
		dataType: 'json', //返回值类型 一般设置为json
		success:function(data){

			if(data.state=="ok")
			{
				//alert('文件上传成功');
				if($('#upload_'+tmp_name).val().length>0)
				{
					$('#upload_'+tmp_name).val($('#upload_'+tmp_name).val()+','+data.upload_id);
					//$('#file_name'+tmp_name).val($('#file_name'+tmp_name).val()+','+data.base_file_n);
				}
				else
				{
					$('#upload_'+tmp_name).val(data.upload_id);
					//$('#file_name'+tmp_name).val(data.base_file_n);
				}
				var tmp_upload_file_id	 = $('#upload_'+tmp_name).val();
				//var tmp_upload_file_name	 = $('#file_name'+tmp_name).val();
				var tmp_s = tmp_upload_file_id.split(",");
				//var tmp_f = tmp_upload_file_name.split(",");						
				$('#show_upload_message_'+tmp_name).html('');

				if(tmp_u == 1){
					var u_down = '../../down_file';
				}else{
					var u_down = 'down_file';
				}
				if(tmp_can==1){
					for(i=0;i<tmp_s.length;i++)
					{
						$('#show_upload_message_'+tmp_name).append('<div id="file_id_'+tmp_s[i]+'"><a style="color:blue;" href="'+u_down+'/?file_id='+tmp_s[i]+'" target="_blank">'+na+'</a>&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:blue;" href="javascript:del_file('+tmp_s[i]+',\''+tmp_name+'\',1)">删除</a></div>');
					}
					$('#show_message_1').html('<font color="red" style="cursor:pointer;">上传成功点击查看</font>');
				}else{
					for(i=0;i<tmp_s.length;i++)
					{
						$('#show_upload_message_'+tmp_name).append('<div id="file_id_'+tmp_s[i]+'"><a style="color:blue;" href="'+u_down+'/?file_id='+tmp_s[i]+'" target="_blank">'+na+'</a>&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:blue;" href="javascript:del_file('+tmp_s[i]+',\''+tmp_name+'\')">删除</a></div>');
					}
					$('#show_message_2').html('<font color="red" style="cursor:pointer;">上传成功点击查看</font>');
				}

			}
			else
			{
				alert(data.state);
			}
		},
		failed:function(err){
			alert(err);
		}
	})
}

function ajaxFileUpload_edit(tmp_name){
	$.ajaxFileUpload({
		id:tmp_name,
		url:"../../upload",
		dataType: 'json', //返回值类型 一般设置为json
		success:function(data){
			if(data.state=="ok")
			{
				//alert('文件上传成功');
				if($('#upload_'+tmp_name).val().length>0)
				{
					$('#upload_'+tmp_name).val($('#upload_'+tmp_name).val()+','+data.upload_id);
					//$('#file_name'+tmp_name).val($('#file_name'+tmp_name).val()+','+data.base_file_n);
				}
				else
				{
					$('#upload_'+tmp_name).val(data.upload_id);
					//$('#file_name'+tmp_name).val(data.base_file_n);
				}
				var tmp_upload_file_id	 = $('#upload_'+tmp_name).val();
				//var tmp_upload_file_name	 = $('#file_name'+tmp_name).val();
				var tmp_s = tmp_upload_file_id.split(",");
				//var tmp_f = tmp_upload_file_name.split(",");						
				$('#show_upload_message_'+tmp_name).html('');
				for(i=0;i<tmp_s.length;i++)
				{
					$('#show_upload_message_'+tmp_name).append('<div id="file_id_'+tmp_s[i]+'"><a href="down_file/?file_id='+tmp_s[i]+'" target="_blank">查看文档</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:del_file('+tmp_s[i]+',\''+tmp_name+'\')">删除</a></div>');
				}
			}
			else
			{
				alert(data.state);
			}
		},
		failed:function(err){
			alert(err)
		}
	})
}

function ajaxFileUpload_gy(tmp_name){
	$.ajaxFileUpload({
		id:tmp_name,
		url: ajaxupload,
		dataType: 'json', //返回值类型 一般设置为json
		success:function(data){

			if(data.state=="ok")
			{
				change_file_zt(data,tmp_name)
			}
			else
			{
				alert(data.state);
			}
		},
		failed:function(err){
			alert(err)
		}
	})
}

$.extend({
	ajaxFileUpload:function(args){
		function uploadCallback(){
			var res={state:'ok',upload_id:1,file_name:1,base_file_n:'',url:"&#19978;&#20256;&#22833;&#36133;&#65281;"},responseText="",resBody=iframe.contents().find("body");
			responseText=resBody[0]?resBody.html():"",responseText&&(res=eval("("+responseText+")"),res.state=='ok'?args.success(res):args.failed(res.state),iframe.remove(),form.remove(),jQuery("#_tmpfile_"+id).attr({id:id}).val(""),jQuery(".btn4").val("上传文件").prop("disabled",!1))
		}
		var id=args.id,form=jQuery("#_tmpform_"+id),iframe=jQuery("#ajaxuploadframe"+id);
		form[0]||(form=jQuery("<form>"),form.attr({id:"_tmpform_"+id}).appendTo("body")),jQuery.extend(form[0],{target:"ajaxuploadframe"+id,action:args.url,method:"post",encoding:"multipart/form-data"}),iframe[0]||(jQuery('<iframe width=0 height=0 id="ajaxuploadframe'+id+'" name="ajaxuploadframe'+id+'"></iframe').appendTo("body").load(uploadCallback),iframe=jQuery("#ajaxuploadframe"+id));
		var file=jQuery("#"+id);
		var tmpfile=file.clone(!0).attr({id:"_tmpfile_"+id});
		form.insertBefore(file).append(file).hide().css("display","none"),tmpfile.insertBefore(form),jQuery("#btn_"+id).val("\u6b63\u5728\u4e0a\u4f20...").prop("disabled",!0),form.submit()
		return true;
	}
})

function del_file(file_id,tmp_name,type)
{
	var value1 = $('#upload_'+tmp_name).val();
	//var value2 = $('#file_nameupload').val();
	value = value1.split(',');
	//value_n = value2.split(',');
	var message = new Array();
	//var message_name = new Array();
	if(confirm('是否确认删除，删除后无法恢复'))
	{
		$("#file_id_"+file_id).remove();
		var num=0;
		for(i=0;i<value.length;i++)
		{
			if(value[i] == file_id)
			{
				
			}
			else
			{
				message[num] = value[i];
				//message_name[num] = value_n[i];
				num++
			}
		}
		$('#upload_'+tmp_name).val(message);
		//$('#file_nameupload').val(message_name);
		if(type == 1){
			$('#show_message_1').html(' ');
		}else{
			$('#show_message_2').html(' ');
		}
	}
}



function show_f()
{
	$("#Float").toggle();
	var offsetTop = $(window).scrollTop() + 280 +"px";
        $("#Float").animate({top : offsetTop},{duration:500 , queue:false});
}
function set_fj_id_name(value,value1,type)
{
	fj_id_name_value = value;
	upload_type = type;
	pubdate  = value1
}
