//ajax post submit请求
$(function(){
    //全选的实现
    $(".check-all").click(function(){
        $(".ids").prop("checked", this.checked);
    });
    $(".ids").click(function(){
        var option = $(".ids");
        option.each(function(i){
            if(!this.checked){
                $(".check-all").prop("checked", false);
                return false;
            }else{
                $(".check-all").prop("checked", true);
            }
        });
    });

    //ajax get请求
    $('.ajax-get').click(function(){
        var target;
        var that = this;
        if ( $(this).hasClass('confirm') ) {
            if(!confirm('确认要执行该操作吗?')){
                return false;
            }
        }
        if ( (target = $(this).attr('href')) || (target = $(this).attr('url')) ) {
            $.get(target).success(function(data){
                if (data.status==1) {
                    if (data.url) {
                        updateAlert(data.info + ' 页面即将自动跳转~','alert-success');
                    }else{
                        updateAlert(data.info,'alert-success');
                    }
                    setTimeout(function(){
                        if (data.url) {
                            location.href=data.url;
                        }else if( $(that).hasClass('no-refresh')){
                            $('#top-alert').find('button').click();
                        }else{
                            location.reload();
                        }
                    },1500);
                }else{
                    updateAlert(data.info);
                    setTimeout(function(){
                        if (data.url) {
                            location.href=data.url;
                        }else{
                            $('#top-alert').find('button').click();
                        }
                    },1500);
                }
            });

        }
        return false;
    });


})

/**顶部警告栏*/
var top_alert = $('#top-alert');
    top_alert.find('.close').on('click', function () {
    top_alert.removeClass('block').slideUp(200);
});

window.updateAlert = function (text,c) {
    text = text||'default';
    c = c||false;
    if ( text!='default' ) {
        top_alert.find('.alert-content').text(text);
        if (top_alert.hasClass('block')) {
        } else {
            top_alert.addClass('block').slideDown(200);
            // content.animate({paddingTop:'+=55'},200);
        }
    } else {
        if (top_alert.hasClass('block')) {
            top_alert.removeClass('block').slideUp(200);
            // content.animate({paddingTop:'-=55'},200);
        }
    }
    if ( c!=false ) {
        top_alert.removeClass('alert-error alert-warn alert-info alert-success').addClass(c);
    }
};

//导航高亮
function highlight_subnav(url){
    $('.side-sub-menu').find('a[href="'+url+'"]').closest('li').addClass('active');
}

function ajax_post (){
    var target,query,form;
    var target_form = $(this).attr('target-form');
    var that = this;
    if($(this).attr('href')||($(this).attr('type')=='submit')){
        form = $('.'+target_form);
        query = form.serialize();
        $(that).addClass('disabled').attr('autocomplete','off').prop('disabled',true);
        target = form.get(0).action;
        $.post(target,query).success(function(data){
            if (data.status==1) {
                if (data.url) {
                    updateAlert(data.info + ' 页面即将自动跳转~','alert-success');
                }else{
                    updateAlert(data.info ,'alert-success');
                }
                setTimeout(function(){
                    $(that).removeClass('disabled').prop('disabled',false);
                    if (data.url) {
                        location.href=data.url;
                    }else if( $(that).hasClass('no-refresh')){
                        $('#top-alert').find('button').click();
                    }else{
                        location.reload();
                    }
                },1500);
            }else{
                updateAlert(data.info);
                setTimeout(function(){
                    $(that).removeClass('disabled').prop('disabled',false);
                    if (data.url) {
                        location.href=data.url;
                    }else{
                        $('#top-alert').find('button').click();
                    }
                },1500);
            }
        });
    }
    return false;
}