<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-23
 * Time: 下午2:20
 */
namespace Admin\Model;
use Think\Model;
class KjcxModel extends Model{
    /*
     * 获取任务数组
     * $array() 过滤数组
     */
    public function getAll($array = null){
        $res = $this->where($array)->select();
        return $res;
    }

    /*
     * 撤销任务处理
     */
    public function callback($id){
        $where = array(
            'id'=>$id
        );
        $data = array(
            'process_id'=>-2,
            'department_id'=>0
        );
        $res = $this->where($where)->save($data);
        return $res;
    }

    /*
     * 指派任务
     * $id  查新ID号
     * $department_id 机构
     */
    public function call($id,$department_id){
        $data = array(
            'id'=>$id,
            'process_id'=>20,
            'department_id'=>$department_id
        );
        return  $this->save($data);

    }
}
