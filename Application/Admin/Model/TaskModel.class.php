<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-23
 * Time: 下午2:20
 */
namespace Admin\Model;
use Think\Model;
class TaskModel extends Model{
    /*
     * 获取任务数组
     * $array() 过滤数组
     */
    public function getAll($array = null){
        $res = $this->where($array)->select();
        return $res;
    }

    /*
     * 撤销任务处理
     */
    public function callback($task){
        $res = $this->data($task)->save();
        return $res;
    }
}
