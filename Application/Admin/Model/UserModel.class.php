<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-17
 * Time: 下午5:31
 */

namespace Admin\Model;
use Think\Model;

class UserModel extends Model {
    /* 用户模型自动验证 */
    protected $_validate = array(
        /* 验证用户名 */
        array('name', '1,16', -14, self::EXISTS_VALIDATE, 'length'), //姓名长度不合法
        array('username', '1,30', -1, self::EXISTS_VALIDATE, 'length'), //用户名长度不合法
        array('username', '', -3, self::EXISTS_VALIDATE, 'unique'), //用户名被占用

        array('city', '1,100', -13, self::EXISTS_VALIDATE, 'length'), //城市不能超过100个字符

        /* 验证手机号码 */
        array('tel','/^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57])[0-9]{8}$/',-9,self::EXISTS_VALIDATE),//手机号码不合法

        /* 验证邮箱 */
        array('email', 'email', -5, self::EXISTS_VALIDATE), //邮箱格式不正确
        array('email', '1,32', -6, self::EXISTS_VALIDATE, 'length'), //邮箱长度不合法
        array('email', '', -8, self::EXISTS_VALIDATE, 'unique'), //邮箱被占用
        array('address', '1,255', -12, self::EXISTS_VALIDATE, 'length'), //通讯地址不能超过200个字符
    );
    /*
    * 添加机构用户
    */
    public function register($arr,$pad){
        $data = array(
            'username'=>$arr['username'],
            'password'=>md5($pad),
            'email'=>$arr['email'],
            'tel'=>$arr['tel'],
            'status'=>1,
            'typeid'=>1,  //机构用户组
            'department'=>$arr['id']
        );
        if($this->create($data)){
            $uid = $this->add();
            return $uid ? $uid : 0;
        }else{
            return $this->getError();
        }
    }

    /*
    * 添加普通用户
    */
    public function register_com($arr,$pad){
        $data = array(
            'username'=>$arr['username'],
            'password'=>md5($pad),
            'email'=>$arr['email'],
            'tel'=>$arr['tel'],
            'name' => $arr['name'],
            'address' => $arr['address'],
            'typeid' => $arr['typeid'],
            'status'=>1,
        );
        $rode = M('AuthGroup')->where('title LIKE "%普通用户%"')->getField('id');
        if(!$rode)
        {
            return false;      //普通用户组不存在
        }
        if($this->create($data)){
            $uid = $this->add();
            if($uid)
            {
                $a_arr = array('group_id'=>$rode,'uid'=>$uid);
                if($id = M('AuthGroupAccess')->add($a_arr))
                {
                    return $id;
                }
                else
                {
                    return -16;
                }
            }
            else
            {
                return -15;
            }
        }else{
            return $this->getError();
        }
    }
    /**
     * @param $data 要修改的信息
     * @return bool|string
     * @teng 2016.6.27
     */
    public  function update($data){
        $list = $this->create($data);
        if($list)
        {
            $result = $this->data($data)->save();
            if($result)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return $this->getError();
        }


    }
    /*
        * 用户登录
        */
    public function login($username,$password){
        $map['username'] = $username;
        //获取用户数据
        $user  = $this->where($map)->find();
        if(is_array($user) )
        {
            if($user['status']){
                if(md5($password) === $user['password']){
                    $allow = M('AuthGroupAccess')->where('uid='.$user['id'])->getField('group_id');
                    $where['description'] = array('LIKE','%普通用户%');
                    $arr   = M('AuthGroup')->where($where)->getField('id');
                    if($arr == $allow){
                        return -4;
                    }
                    return $user['id'];
                }else{
                    return -2;  //密码错误
                }
            }else{
                return -3; //用户禁用
            }
        }
        else
        {
            return -1;//用户不存在
        }
    }



    public function a_login($uid){
        /* 检测是否在当前应用注册 */
        $Model = M('User');
        $user = $Model->find($uid);
        if(!$user || 1 != $user['status']) {
            $this->error = '用户不存在或已被禁用！'; //应用级别禁用
            return false;
        }
        //记录行为
        action_log('user_login', 'member', $uid, $uid);
        /* 登录用户 */
        $this->autoLogin($user);
        return true;
    }

    private function autologin($user){
        $auth = array(
            'uid'=>$user['id'],
            'username'=>$user['username']
        );
        session('user_auth',$auth);
    }



    public function change_isIp($uid){
        $data['is_ip'] =1;
        $map['id'] = $uid;
        $this->where($map)->save($data);
        return true;
    }

    public function logout($uid){
        action_log('user_logout', 'member', $uid, $uid);
        session('user_auth', null);
    }
}