<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-16
 * Time: 上午9:18
 */
namespace Admin\Model;
use Think\Model;
class TrainingModel extends Model {

    /* 用户模型自动验证 */
    protected $_validate = array(
        /* 验证用户名 */
        array('title', '1,255', -1, self::EXISTS_VALIDATE, 'length'), //标题长度不合法
        array('author', '1,100', -2, self::EXISTS_VALIDATE, 'length'), //作者长度不合法
        array('source', '1,50', -3, self::EXISTS_VALIDATE, 'length'), //来源长度不合法
        array('title', '', -5, self::EXISTS_VALIDATE, 'unique'), //来源长度不合法
        array('content', 'require', -6), //内容不能为空
    );

    //添加新闻
    public function news_add ($data)
    {
        $data = $this->create($data);
        if($data)
        {
            $id = $this->add($data);
            if($id)
            {
                return $id?$id:0;
            }
            else
            {
                return -4;
            }
        }
        else
        {
            return $this->getError();
        }
    }

    //修改新闻
    public function news_edit ($data)
    {
        $data = $this->create($data);
        if($data)
        {
            $id = $this->save($data);
            if($id)
            {
                return $id?$id:0;
            }
            else
            {
                return -4;
            }
        }
        else
        {
            return $this->getError();
        }
    }
}