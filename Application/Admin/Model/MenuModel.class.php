<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/8/29
 * Time: 16:24
 */
namespace Admin\Model;
use Think\model;
class MenuModel extends Model{
    //删除菜单
    Public function menu_del($id){
        if(empty($id)){
            return -2;
        }
        if($this->delete($id)){
            return true;
        }else{
            return false;
        }
    }
}