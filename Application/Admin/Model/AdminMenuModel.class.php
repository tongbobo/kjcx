<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-18
 * Time: 上午10:11
 */
namespace Admin\Model;
use Think\Model;
class AdminMenuModel extends Model{
    public function getPath($id){
        $path = array();
        $nav = $this->where("id={$id}")->field('id,pid,title')->find();
        $path[] = $nav;
        if($nav['pid'] >1){
            $path =array_merge($this->getPath($nav['pid']),$path);
		}
        return $path;
    }

    public function toogleHide($id, $value = 1){
        $data = array(
            'id'=> $id,
            'hide'=> $value
        );
        $res = $this->data($data)->save();
        return $res;
    }
}

