<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-17
 * Time: 上午11:05
 */
namespace Admin\Model;
use Think\Model;
class TasklogModel extends Model {
    /*
     * 获取日志
     * $map 查询参数
     */
    public function getList($map){
        $res = $this->where($map)->select();
        return $res;
    }

    /*
     * 插入日志
     */
    public function update($uid, $action='', $id=''){
        $auth = session(user_auth);
        $array = array(
            'uid'=>$uid,
            'username'=>$auth['username'],
            'action'=>$action.'科技查新任务ID'.$id,
            'time'=>NOW_TIME
        );
        $this->create();
        $log_id =$this->add($array);
        return $log_id;
    }
}