<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/8/22
 * Time: 16:34
 */
namespace Admin\Controller;

class AreaController extends AdminController{
    public function index(){
        $map =array();
        $Area = M('Area');
        $list = $this->lists($Area,$map);
        $this->assign('_list', $list);
        $this->assign('meta_title', '城市管理');
        $this->display();
    }

    public function add(){
        if(IS_POST){
            $Area = D('Area');
            $data = $Area->create();
            if($data['title'] == '')
            {
                $this->error('名称不能为空');
            }
            $city_arr = M('Area')->where("title = '$data[title]'")->find();
            if($city_arr['area_id'])
            {
                $this->error('城市名称已经存在');
            }
            if($data){
                $id = $Area->add();
                if(0<$id) {
                    $this->success('新增成功', U('index'));
                }else{
                    $this->error('新增失败');
                }
            }else{
                $this->error($Area->getError());
            }
        }else{
            $this->display('edit');
        }
    }

    //编辑
    public function edit($area_id=0){
        if(IS_POST){
            $Area = D('Area');
            $data = $Area->create();
            if($data['title'] == '')
            {
                $this->error('名称不能为空');
            }
            $city_arr = M('Area')->where("title = '$data[title]'"." and area_id != ".$data['area_id'])->find();
            if($city_arr['area_id'])
            {
                $this->error('城市名称已经存在');
            }
            if($data){
                if($Area->save()){
                    $this->success('保存成功',U('index'));
                }else{
                    $this->error('保存失败');
                }
            }else{
                $this->error($Area->getError());
            }
        }else{
            if(empty($area_id)){
                $this->error('参数错误');
            }
            $map['area_id'] = $area_id;
            $info = M('Area')->where($map)->find();
            $this->assign('info', $info);
            $this->display();
        }
    }

    /**
     * 删除  待修改
     */
    public function del(){
        $area_id = I('area_id');
        if(empty($area_id)){
            $this->error('请选择需要操作的数据');
        }

        $map = array('area_id'=>array('in',$area_id));
        if(M('Area')->where($map)->delete()){
            $this->success('删除成功',U('index'));
        }else{
            $this->error('删除失败');
        }
    }
}