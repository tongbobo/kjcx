<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/7/9
 * Time: 11:26
 */

namespace Admin\Controller;
use Think\Controller;

class TrendsController extends AdminController{

	/**
	 *  政策法规
	 * 2017.2.7 teng
	 */
	public function index()
	{
		$order = 'datetime desc';
		$model = M('Trends');
		$this->Trends_catch(1);  //更新缓存
		$list  = $this->lists($model,array(),$order);
		$this->assign('_list',$list);
		$this->assign('meta_title', '政策法规');
		$this->display();

	}
	/**
	 *  政策添加和修改
	 * 2017.2.7 teng
	 */
	public function news_menu ()
	{
		if(IS_POST)
		{
			$data    = $_POST;
			$model	 = D('Trends');
			if($data['id'])      //修改
			{
				$data['id'] = intval($data['id']);
				$info 		= M('Trends')->find($data['id']);
				if($info)
				{
					$n_id   = $model->news_edit($data);
					if($n_id>0)
					{
						$this->Trends_catch(1);  //更新缓存
						$this->success('修改成功',U('index'));
					}
					else
					{
						$this->error($this->showRegError($n_id));
					}
				}
				else
				{
					$this->error('数据不存在');
				}
			}
			else                //添加
			{
				$data['datetime']   = time();  //添加时间
				$n_id	= $model->news_add($data);
				if($n_id>0)
				{
					$this->Trends_catch(1);  //更新缓存
					$this->success('添加成功',U('index'));
				}
				else
				{
					$this->error($this->showRegError($n_id));
				}
			}
		}
		else
		{
			if(I('id'))
			{
				$id 	= intval(I('id'));
				$info 	= M('Trends')->find($id);
				if($info)
				{
					$this->assign('info',$info);
				}
				else
				{
					JsLocation('数据不存在');
				}
			}
			$this->assign('meta_title', '政策法规');
			$this->display();
		}
	}

//新闻删除
	public function news_del(){
		if(I('id'))
		{
			$id 	= intval(I('id'));
			$info 	= M('Trends')->find($id);
			if($info)
			{
				if(M('Trends')->delete($info['id']))
				{
					$this->Trends_catch(1);  //更新缓存
					$this->success('删除成功',U('index'));
				}
				else
				{
					$this->error('删除失败');
				}
			}
			else
			{
				$this->error('数据不存在');
			}
		}
		else
		{
			$this->error('参数错误');
		}
	}

	private function showRegError($code = 0){
		switch ($code) {
			case -1:  $error = '标题长度必须在1-255个字符以内！'; break;
			case -2:  $error = '作者长度必须在1-100个字符以内！'; break;
			case -3:  $error = '来源长度必须在1-50个字符以内！'; break;
			case -4:  $error = '操作失败！'; break;
			case -5:  $error = '标题名称已被占用！'; break;
			case -6:  $error = '动态内容不能为空！'; break;
			default:  $error = '未知错误';
		}
		return $error;
	}

	private function Trends_catch($tag=''){
		if(empty($tag))		//读取缓存
		{
			return S('Trends_catch');
		}
		else				//写入缓存
		{
			S('Trends_catch',null);
			$order = 'datetime desc';
			$model = M('Trends');
			$list  = $model->order($order)->getField('id,id,title,content,datetime,author,source');
			S('Trends_catch',$list);
			return true;
		}
	}

}
