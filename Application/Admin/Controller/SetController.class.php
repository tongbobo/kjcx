<?php
/**
 * Created by PhpStorm.
 * User: sl
 * Date: 2016/4/1
 * Time: 14:15
 */
namespace Admin\Controller;
use Think\Controller;
class SetController extends AdminController {
    //抢单默认设置页面
    public function qd_set(){
        $default = C('SCRAMBLE_TASK');
        if(IS_POST){
            $task = I('post.task');
            $where['id'] = (int)trim($task);
            $name = M('Option')->where($where)->getField('name');
            switch($name){
                case "dingshi":
                    $value = I('post.dingshi');
                    break;
                case "weichuli":
                    $value = I('post.weichuli');
                    break;
            }
            //保存config表
            $configWhere = array(
                'name' =>'SCRAMBLE_TASK'
            );
            $configData = array(
                'value'=>$task
            );
            $configSave = M('Config')->where($configWhere)->save($configData);
            if($task != 1){
                //保存option表
                $data = array(
                    'id'=>$task,
                    'value'=>$value
                );
                $res = M('Option')->save($data);
            }
            //删除缓存
            S('DB_CONFIG_DATA',NULL);
            $this->success('保存成功');
        }else{
            $Option = M('Option');
            $where['type'] = 1;
            $order= array('sort'=>'desc');
            $list = $this->lists($Option,$where,$order);
            $this->meta_title = '抢单默认设置';
            $this->assign('list',$list);
            $this->assign('chose',$default);
            $this->display('qd_set');
        }
    }

    //学校配置
    public function school_set(){
        $default = C('SCHOOL_CONFIG');
        if(IS_POST){
            $count['success'] = $count['error'] =0;
            $data = I('post.');
//            //config表保存
            $configData = array(
                'value'=>$data['chose']
            );
            M('Config')->where('name = "school_config"')->save($configData);
            //option表保存
            $arr_update = array();
            $arr_add = array();
            if($data['chose']!=1){
                foreach($data['it']['1']['id'] as $key=>$item){//重组更新数据
                    $arr_update[]['id']=$item;
                    $arr_update[$key]['start_day']=$data['it']['1']['start_day'][$key];
                    $arr_update[$key]['end_day']=$data['it']['1']['end_day'][$key];
                    $arr_update[$key]['value']=$data['it']['1']['school'][$key];
                }
                foreach($data['it']['2']['start_day'] as $key=>$item){//重组添加数据
                    $arr_add[]['start_day']=$data['it']['2']['start_day'][$key];
                    $arr_add[$key]['end_day']=$data['it']['2']['end_day'][$key];
                    $arr_add[$key]['value']=$data['it']['2']['school'][$key];
                    $arr_add[$key]['type']='2';
                    $arr_add[$key]['name']='school_config';
                    $arr_add[$key]['desc']='学校设置';
                }
                if($arr_add){
                    //添加
                    foreach($arr_add as $key0=>$item0) {
                        if($item0['start_day']!=0 && $item0['end_day']!=0 && $item0['value']!=0){
                            if($item0['start_day'] <= $item0['end_day'])
                            {
                                $res = M('Option')->add($item0);
                                if($res){
                                    $count['success']++;
                                }
                            }

                        }
                    }
                }
                if($arr_update){
                        //保存
                    foreach($arr_update as $key1=>$item1) {
                        if($item1['start_day']!=0 && $item1['end_day']!=0 && $item1['value']!=0){
                            if($item0['start_day'] <= $item0['end_day'])
                            {
                                $res = M('Option')->save($item1);
                                if($res){
                                    $count['success']++;
                                }
                            }
                        }
                    }
                }
                //删除缓存
                S('DB_CONFIG_DATA',NULL);
                $this->success($count['success'].'条信息更新成功');
            }
            else
            {
                $id = M('Option')->where('name = "school_config"')->delete();
                //删除缓存
                S('DB_CONFIG_DATA',NULL);
                $this->success('设置成功');
            }

        }else{
//            print_r(S('DB_CONFIG_DATA'));die;
            $this->assign('chose',$default);
            $list = M('Option')->where('type=2')->getField('id,value,start_day,end_day');
            $this->assign('list',$list);

            $schoolList = M('Department')->field('id,name')->select();
            $this->assign('schoolList',$schoolList);
            $this->assign('chose',$default);
            $this->meta_title = '抢单默认设置';
            $this->display('school_set');
        }
    }
    //学校配置删除
    public  function  del(){
        $id = I('id');
        $rs = M('Option')->where(array('id'=>$id))->find();
        if($rs){
            if(M('Option')->where(array('id'=>$id))->delete()){
                $this->success('删除成功');
            }else{
                $this->error('删除失败');
            }
        }else{
            $this->error('该数据不存在');
        }
    }
}