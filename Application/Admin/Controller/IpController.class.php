<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-18
 * Time: 下午4:36
 */
namespace Admin\Controller;
use Think\Controller;
class IpController extends AdminController{
    public function userList(){
        $list = M('User')->where('is_ip=1')->select();
        $this->assign('list', $list);
        $this->display();
    }

    public function add(){
        if(IS_POST){
            $data = I('post.');
            $ip = array(
                'uid'=>$data['id'],
                'ip'=>$data['ip']
            );
            $res = M('Taskip')->add($ip);
            $User = D('User');
            $User->change_isIp($ip['uid']);
            if(!$res){
                $this->error('没有添加成功');
            }else{
                $this->success('添加成功',U('Ip/userList'));
            }
        }else{
            $list = M('User')->where('is_ip=0')->select();
            $this->assign('list', $list);
            $this->display();
        }
    }

    public function config(){
        if(IS_POST){
            $ip = I('post.');
            $map['uid'] = $ip['id'];
            $data = array(
                'ip'=>$ip['ip']
            );
            $res = M('Taskip')->where($map)->data($data)->save();
            if($res){
                $this->success('保存成功',U('Ip/userList'));
            }else{
                $this->error('对不起，保存失败');
            }
        }else{
            $id = I('id');
            $map['id'] = $id;
            $user_info = M('User')->where($map)->find();
            $where['uid'] = $id;
            $ip = M('Taskip')->where($where)->getField('ip');
            $this->assign('ip',$ip);
            $this->assign('user',$user_info);
            $this->display();
        }
    }
}