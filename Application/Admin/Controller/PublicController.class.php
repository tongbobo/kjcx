<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-17
 * Time: 下午4:10
 */
namespace Admin\Controller;
use User\Api\UserApi;

class PublicController extends \Think\Controller {
    /*
     * 用户登录
     */
    public function login($username = null, $password = null, $verify = null){
        if(IS_POST){
            $uid = D('User')->login($username, $password);
            if (0 < $uid) {
                if (D('User')->a_login($uid)) {
                    $this->success('登录成功',U('Task/index'));
                } else {
                    $this->error('登录失败');
                }
            } else {
                switch ($uid) {
                    case -1:
                        $error = '用户不存在';
                        break;
                    case -2:
                        $error = '用户密码不正确';
                        break;
                    case -3:
                        $error = '用户被禁用';
                        break;
                    case -4:
                        $error = '该用户组不允许登录';
                        break;
                    default :
                        $error = '未知错误';
                        break;
                }
                $this->error($error);
            }
        }else{
            $value2 = cookie('remember_password_a');

            $value3 = cookie('remember_account_a');

            if($value2&&$value3)
            {
                $this->assign('pass_a',$value2);

                $this->assign('account_a',$value3);
            }

            $this->display();
        }
    }

    /*
     * 用户退出
     */
    public function logout(){
        if(is_login()){
            D('User')->logout();
            session('[destroy]');
            $this->redirect('login');
        } else {
            $this->redirect('login');
        }
    }

    /*
     * 验证码
     */
    public function verify(){
        $verify = new \Think\Verify();
        $verify->entry(1);
    }
    /**
     * 修改密码
     * @return void
     */

    public function changePwd(){
        $password = trim($_POST['code']);
        $user = M('User');
        $check = $user->find($_SESSION['user_auth']['uid']);
        if ($check['password'] != md5($password)) {
            $this->error('原始密码输入不正确');
        }
        $data['password'] = $_POST['password'];
        if (!$this->sec_check1($data['password'])) {
            $this->error('原始密码和新密码不能一样');
        }
        if(strlen($data['password'])<6 || strlen($password)>20)
        {
            $this->error('密码只允许在6-20位');
        }
        $repassword = $_POST['repassword'];
        if ($data['password'] !== $repassword) {
            $this->error('两次输入的密码不一样');
        }
        $data['password'] = md5($data['password']);
        $user = M('User');
        $id = session('user_auth.uid');
        $res = $user->where('id='.$id)->save($data);
        if ($res) {

            action_log('user_changePassword', 'user', $id, $id);
            $this->success('密码已经修改成功',__APP__.'/Admin/Public/logout');
//                $this->redirect(U('logout'));
        } else {
            $this->error('密码修改失败');
        }

    }
    //检查密码输入的是否正确
    protected function checkpwd1($pwd){
        $map['id'] = UID;
        $user = M('User');
        $check = $user->find(UID);
        if($check['password'] === md5($pwd)){
            return true;
        }else{
            return false;
        }
    }
    //验证密码是否与旧密码相同
    protected function sec_check1($newPassword){
        $user = M('User');
        $check = $user->find(UID);
        if($check['password'] === md5($newPassword)){
            return false;
        }else{
            return true;
        }
    }
}