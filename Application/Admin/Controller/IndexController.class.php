<?php
namespace Admin\Controller;
use Think\Controller;
class IndexController extends AdminController {
    public function index(){
        if(UID){
            $this->redirect('Task/index');
        }else{
            $this->redirect('Public/login');
        }
    }

    public function log(){
        if(UID){
            $this->assign('list', M('Log')->where(array('user_id'=>UID))->select());
            $this->display();
        }
    }

    public function message(){
        if(IS_POST){
            $userList = M('User')->getField('id',true);
            foreach($userList as $value){
                $data = array(
                    'uid'=>$value,
                    'title'=>'系统公告',
                    'body'=>I("post.content"),
                    'ctime'=>NOW_TIME,
                    'is_read'=>0
                );
                M('Notify_message')->add($data);
            }
            $this->success('发布成功','message');
        }else{
            if(UID){
                $this->display();
            }
        }
    }
}