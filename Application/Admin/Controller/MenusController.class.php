<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-8-18
 * Time: 下午2:22
 */
namespace Admin\Controller;

class MenusController extends AdminController{
    /*
     * 显示模块菜单
     */
    public function MenuList(){
        $name = I('name');
        if($name){
            $map['name'] = $name;
        }
        $list = $this->lists('Menus', $map);
        Cookie('__forward__',$_SERVER['REQUEST_URI']);
        $this->assign('_list', $list);
        $this->assign('name',$name);
        $this->assign('meta_title', '菜单管理');
        $this->display();
    }

    /*
     * 添加模块
     */
    public function group_add(){
        if(IS_POST){
            $Menus = D('Menus');
            $data = $Menus->create();
            if($data['name'] == '')
            {
                $this->error('菜单标示不能为空');
            }
            if($data){
                $id = $Menus->add();
                if($id){
                    action_log(UID,'新增导航');
                    $this->success('新增成功',Cookie('__forward__'));
                }else{
                    $this->error($Menus->getError());
                }
            }
        }else{
            $this->display('group_edit');
        }
    }

    /*
     * 编辑模块
     */
    public function group_edit($menu_id){
        if(IS_POST){
            $Menus = D('Menus');
            $data = $Menus->create();
            if($data['name'] == '')
            {
                $this->error('菜单标示不能为空');
            }
            if($data){
                if($Menus->save()!==false){
                    action_log(UID,'编辑导航成功');
                    $this->success('更新成功',Cookie('__forward__'));
                }else{
                    $this->error('更新失败');
                }
            }
        }else{
            $info = M('Menus')->field(true)->find($menu_id);
            if(false === $info){
                $this->error('获取导航信息错误');
            }
            $this->assign('info',$info);
            $this->meta_title = "编辑导航";
            $this->display();
        }
    }

    /*
     * 删除模块
     */
    public function group_del(){
        $id = array_unique((array)I('menu_id',0));
        if(empty($id)){
            $this->error('请选择需要操作的数据');
        }
        $map = array('menu_id'=>array('in', $id));
        //删除导航
        if(M('Menus')->where($map)->delete()){
            action_log(UID,'删除菜单');
            //删除导航的菜单项目(暂时没有做)删除上级菜单，子集菜单也要删除！！！！！
            $this->success('删除成功');
        }else{
            $this->error('删除失败！');
        }
    }
       /*
        * 菜单页面
        */
    Public function edit($menu_name,$pid=0){
        if(empty($menu_name)){
            $this->error('参数错误！');
        }
        $where['module'] = $menu_name;
        $where['pid'] = $pid;
        $list = M('Menu')->where($where)->select();
        $all_menu = M('Menu')->getField('id,title');
        $this->list = $list;
        $this->module = $menu_name;
        $this->meta_title='菜单明细管理';
        int_to_string($list,array('hide'=>array(1=>'是',0=>'否'),'is_dev'=>array(1=>'是',0=>'否')));
        if($list) {
            foreach($list as &$key){
                if($key['pid']){
                    $key['up_title'] = $all_menu[$key['pid']];
                }
            }
            $this->assign('list',$list);
        }
        Cookie('__forward__',$_SERVER['REQUEST_URI']);
        $this->display();
    }
    /*
     * 添加菜单
     */
    public function menu_add($module, $pid=null){
        if(IS_POST){
            $Menu = D('Menu');
            $data = $Menu->create();
            if($data['title'] == '')
            {
                $this->error('请输入标题');
            }
            if($data['url'] == '')
            {
                $this->error('请输入地址');
            }
            if($data){
                $id = $Menu->add();
                if($id){
                    session('ADMIN_MENU_LIST',null);
                    $this->success('新增成功', Cookie('__forward__'));
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($Menu->getError());
            }
        }else{
            $info['module'] = $module;
            $info['pid'] = $pid;
            $this->assign('info',$info);
            $where['module'] = $module;
            $menus = M('Menu')->where($where)->field(true)->select();
            $menus = D('Common/Tree')->toFormatTree($menus);
            $menus = array_merge(array(0=>array('id'=>0,'title_show'=>'顶级菜单')), $menus);
            $this->assign('Menus', $menus);
            $this->display('menu_add');
        }
    }
    /*
      * 菜单编辑
      */
    public function menu_edit($id){
        if(IS_POST){
            $Menu = D('Menu');
            $data = $Menu->create();
            if($data['title'] == '')
            {
                $this->error('请输入标题');
            }
            if($data['url'] == '')
            {
                $this->error('请输入地址');
            }
            if($data){
                if($Menu->save()!== false){
                    session('ADMIN_MENU_LIST',null);
                    $this->success('更新成功', Cookie('__forward__'));
                } else {
                    $this->error('更新失败');
                }
            } else {
                $this->error($Menu->getError());
            }
        }else{
            /* 获取数据 */
            $info = M('Menu')->field(true)->find($id);
            $where['menu_id'] = $info['menu_id'];
            $menus = M('Menu')->where($where)->field(true)->select();
            $menus = D('Common/Tree')->toFormatTree($menus);
            $menus = array_merge(array(0=>array('id'=>0,'title_show'=>'顶级菜单')), $menus);
            $this->assign('Menus', $menus);
            if(false === $info){
                $this->error('获取后台菜单信息错误');
            }
            $this->assign('info', $info);
//            $this->meta_title = '编辑后台菜单';
            $this->display('menu_add');
        }
    }
    /*
    *菜单删除
    */
    public function del(){
        $id = array_unique((array)I('id', 0));
        $id = implode(',', $id);
        if (empty($id)) {
            $this->error('请选择要操作的数据!');
        }
        $map['pid']=array('IN',$id);
        $rs=M('Menu')->where($map)->delete();
        $rs_p=M('Menu')->where(array('id' => $id))->delete();
        if ($rs||$rs_p) {
            session('ADMIN_MENU_LIST', null);
            //记录行为
            $this->success('删除成功');
        } else {
            $this->error('删除失败！');
        }
    }
}