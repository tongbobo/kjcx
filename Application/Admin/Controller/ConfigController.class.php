<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/7/7
 * Time: 20:48
 */
namespace Admin\Controller;
use User\Api\UserApi;
class ConfigController extends AdminController{

    public function index(){
        $map = array();
        $map  = array('status' => 1);
        if(isset($_GET['group'])){
            $map['group']   =   I('group',0);
        }
        if(isset($_GET['name'])){
            $map['name']    =   array('like', '%'.(string)I('name').'%');
        }

        $list = $this->lists('Config', $map,'sort,id');
        // 记录当前列表页的cookie
//        Cookie('__forward__',$_SERVER['REQUEST_URI']);

        $this->assign('group',C('CONFIG_GROUP_LIST'));
        $this->assign('group_id',I('get.group',0));
        $this->assign('list', $list);
        $this->meta_title = '配置管理';
        $this->display();
    }

    public function group(){
        $id     =   I('get.id',1);
        $type   =   C('CONFIG_GROUP_LIST');
        $list   =   M("Config")->where(array('status'=>1,'group'=>$id))->field('id,name,title,extra,value,remark,type')->order('sort')->select();
        if($list) {
            $this->assign('list',$list);
        }
        $this->assign('id',$id);
        $this->meta_title = $type[$id].'配置';
        $this->display();
    }

    public function save($config){
        if($config && is_array($config)){
            $Config = M('Config');
            foreach ($config as $name => $value) {
                $map = array('name' => $name);
                $Config->where($map)->setField('value', $value);
            }
        }
        S('DB_CONFIG_DATA',null);
        $this->success('保存成功！');
    }

    public function permissionSet(){
        if(IS_POST){
            $id = I('get.gid');
            $data = implode(',',I('post.rule'));
            $insert = array(
                'id'=>$id,
                'rules'=>$data
            );
            $res = M('AuthGroup')->data($insert)->save();
            if(!$res){
                $this->error('保存失败!');
            }else{
                $this->success('保存成功 ');
            }

        }else{
            $groupId = I('get.gid');
            $group = M('AuthGroup')->where(array('id'=>$groupId))->find();
            $auth_list = M('AuthRule')->where(array('status'=>1,'module'=>$group['module']))->field('id,name,title,type')->select();
            $this_group = M('AuthGroup')->where(array('id'=>$groupId))->getField('rules');
            $this->assign('groupName', $group['title']);
            $this->assign('authList', $auth_list);
            $this->assign('thisGroup', $this_group);
            $this->display();
        }
    }

    //用户列表页面
    public function user(){
        $map = array();
        $keyword=trim(I('request.keyword'));//搜索框
        if($keyword){
            $_GET['p']=0;//初始化分页
            $map['username']=array("like","%$keyword%");
        }
        $keyword=I('request.keyword');
        $where['_string']='(title = "普通用户组") OR (title = "系统调度中心组")';
        $map['username']=array("like","%$keyword%");
//        $map['status']=1;
        $User = M('User');
        $Group=M('auth_group_access')->select();
        $Group_user=M('auth_group')->where($where)->select();
        $arr=array();
        foreach($Group_user as $vo){//组合数组用户组数据
            foreach($Group as $vv){
                if($vo['id']==$vv['group_id']){
                    $arr[$vv['uid']]=$vo['title'];
                }
            }
        }
        $resu=M('auth_group')->where($where)->select();
        foreach($resu as $sv){
            $sql[]=$sv['id'];
        }
        $sql=implode(",",$sql);
        $maps['group_id']=array('in',"$sql");
        $res= M('auth_group_access')->field('uid')->distinct(true)->where($maps)->select();
        foreach($res as $svv){
            $sqls[]=$svv['uid'];
        }
        $map['id']=array('in',implode(",",$sqls));
        $list = $this->lists($User,$map);
        $this->assign('_list',$list);
        $this->assign('meta_title', '用户信息');
        $this->assign('arr',$arr);
        $this->display();
    }
//用户添加
    public function add(){
        if(IS_POST){
            $data = I('post.');
            $User = D('User');
            $uid = $User->register_com($data,'123456');//新增用户初始密码为123456
            if(0 < $uid) {
                $this->success('添加成功',U('Config/user'));
            }else{
                $this->error($this->showRegError($uid));
            }
        }
    }
    public function useradd(){

        $this->assign('meta_title', '用户信息');
        $this->display();
    }
    private function showRegError($code = 0){
        switch ($code) {
            case -1:  $error = '用户名长度范围1-16个字符！'; break;
            case -2:  $error = '用户名被禁止注册！'; break;
            case -3:  $error = '用户名被占用！'; break;
            case -4:  $error = '密码长度必须在6-30个字符之间！'; break;
            case -5:  $error = '邮箱格式不正确！'; break;
            case -6:  $error = '邮箱长度必须在1-32个字符之间！'; break;
            case -7:  $error = '邮箱被禁止注册！'; break;
            case -8:  $error = '邮箱被占用！'; break;
            case -9:  $error = '手机格式不正确！'; break;
            case -10: $error = '手机被禁止注册！'; break;
            case -11: $error = '手机号被占用！'; break;
            case -12: $error = '通讯地址范围1-200个字符！'; break;
            case -13: $error = '城市长度范围1-100个字符！'; break;
            case -14: $error = '姓名长度范围1-16个字符！'; break;
            case -15: $error = '添加到普通用户组失败！'; break;
            case -16: $error = '添加普通户失败！'; break;
            default:  $error = '未知错误';
        }
        return $error;
    }
}

