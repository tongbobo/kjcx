<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/7/9
 * Time: 11:26
 */

namespace Admin\Controller;
use Think\Controller;

class TplController extends AdminController{

	/**
	 *  模块详情页
	 * 2016.7.2 teng
	 */
	public function index()
	{
		$order = 'is_top,datetime desc';
		$model = M('Templete');
		$list  = $model->select();
		$this->assign('_list',$list);
		$this->meta_title = '模块列表';
		$this->display();

	}
	/**
	 *  模块修改
	 * 2016.7.2 teng
	 */
	public function tpl_menu ()
	{
		if(IS_POST)
		{
			$data    = $_POST;
			$model	 = M('Templete');
			if(I('id'))      //修改
			{
				$id 	= intval(I('id'));
				$info 	= $model->find($id);
				if($info)
				{
					$data['datetime'] = time();
					$n_id   = $model->where('id='.$id)->save($data);
					if($n_id>0)
					{
						$this->success('修改成功',U('index'));
					}
					else
					{
						$this->error('修改失败');
					}
				}
				else
				{
					$this->error('数据不存在');
				}
			}
			else
			{
				$this->error('数据不存在');
			}
		}
		else
		{
			if(I('id'))
			{
				$id 	= intval(I('id'));
				$info 	= M('templete')->find($id);
				if($info)
				{
					$this->assign('info',$info);
				}
				else
				{
					$this->error('数据不存在');
				}
			}
			$this->meta_title = '模块列表';
			$this->display();
		}
	}

//新闻删除
	public function news_del(){
		if(I('id'))
		{
			$id 	= intval(I('id'));
			$info 	= M('News')->find($id);
			if($info)
			{
				if(M('News')->delete($info['id']))
				{
					$this->success('删除成功',U('index'));
				}
				else
				{
					$this->error('删除失败');
				}
			}
			else
			{
				$this->error('数据不存在');
			}
		}
		else
		{
			$this->error('参数错误');
		}
	}

	private function showRegError($code = 0){
		switch ($code) {
			case -1:  $error = '标题长度必须在1-255个字符以内！'; break;
			case -2:  $error = '作者长度必须在1-100个字符以内！'; break;
			case -3:  $error = '来源长度必须在1-50个字符以内！'; break;
			case -4:  $error = '操作失败！'; break;
			case -5:  $error = '标题名称已被占用！'; break;
			case -6:  $error = '新闻内容不能为空！'; break;
			default:  $error = '未知错误';
		}
		return $error;
	}

}
