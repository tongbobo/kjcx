<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/7/1
 * Time: 17:25
 */
namespace Admin\Controller;
use Think\Controller;
use User\Api\UserApi;
use Api\Department\DepartmentApi;
class UnitController extends  AdminController{
    public function index(){
        $Unit = M('Department');
        $list = $this->lists($Unit);
        $this->assign('_list', $list);
        $this->assign('meta_title', '机构管理');
        $this->display();
    }
    /*
     * 审核机构
     */
    public function doAuditUnit(){

        $id = I('get.id');
        if(empty($id)){
            $return['status'] = 0;
            $retrun['data'] = '';
            echo json_encode($return);exit;
        }
        $unit = new DepartmentApi();
        //取得机构用户的
        $info = $unit->getInfo($id);
        //创建机构用户的账号
        $uid=$this->_addUser($info);
        if(0<$uid){
            //审核通过用户
            $result = $unit->audit($id);
            //添加用户权限
            $auth = D('AuthGroupAccess')->addGroup($uid,"1");
            if(!$auth){
                $this->error('对不起，添加用户权限失败');exit;
            }
            if(!$result){
                $this->error("审核用户没有通过");
            }else{
                $this->success('审核用户通过',U('Unit/index'));
            }
        }else{
            $this->error($this->showRegError($uid));
        }
    }

    /*
     * 机构编辑
     */
    public function edit(){
        if(IS_POST){
            $data = I('post.');
            $Department = new DepartmentApi();
            $departmentId = $Department->update($data);
            if($departmentId>0){
                $this->success('修改成功',U('Unit/index'));
            }else{
                $this->error($this->showRegError($departmentId));
            }
        }else{
            $id = I('get.id');
            $type = I('type');
            $Department = new DepartmentApi();
            $info = $Department->info($id);
            $this->getCategory();
            $this->assign('info', $info);
            $this->assign('type', $type);
            $this->meta_title = '机构编辑';
            $this->display();
        }
    }

    /*
     * 添加机构
     */
    public function add(){
        if(IS_POST){
            $data = I('post.');
            $Department = new DepartmentApi();
            $departmentId = $Department->register($data);
            $data['id'] = $departmentId;
            if(0<$departmentId){
                //机构添加成功的时候往unit中添加一条数据
                $arr['depart_id'] = $departmentId;
                M('Unit')->add($arr);
                $this->success('添加成功',U('Unit/index'));
            }else{
                $this->error($this->showRegError($departmentId));
            }
        }else{
            $this->getCategory();
            $Department = new DepartmentApi();
            $this->meta_title = '机构新增';
            $this->display('edit');
        }
    }
    /*
     * 机构信息
     */
    public function info(){
        $id= I('get.id');
        $type = I('type');
        $Department = new DepartmentApi();
        $info = $Department->info($id);
        $this->getCategory();
        $this->assign('info', $info);
        $this->assign('type',$type);
        $this->meta_title = '查看详情';
        $this->display();
    }

    /*
     * 机构人员管理
     */
    public function user(){

        $departmentId = M('User')->where("id=".UID)->getField('department');
        $map['department'] = $departmentId;
        $map['id'] = array('neq',UID);
        $list = D('User')->where($map)->select();
        $this->assign('department', $departmentId);
        $this->assign('list', $list);
        $this->display();
    }
    /*
     * 审核机构时添加的用户
     */
    private function _addUser($department){
        $user =D('User');
        $passoword = "123456";
        $department['city'] = getCityName($department['city']);
        $uid = $user->register($department,$passoword);
        if($uid>0)
        {
            $data1 = array(
                'id' => $department['id'],
                'uid' => $uid
            );
            $res = D('Department')->data($data1)->save();
            if ($res) {
                return $uid;
            } else {
                return -1;
            }
        }
        else
        {
            return $uid;
        }
    }

    /*
     * 机构IP配置
     */
    public function ipConfig(){
        if(IS_POST){
            $data = I('post.');
            $Department = M('Department');
            $res = $Department->save($data);
            if($res){
                $this->success('保存成功',U('Unit/index'));
            }else{
                $this->error('保存失败');
            }
        }else{
            $map['id'] = I('get.id');
            $info = M('Department')->where($map)->find();
            $this->assign('info',$info);
            $this->meta_title = 'IP匹配';
            $this->display();
        }
    }

    private function showRegError($code = 0){
        switch ($code) {
            case -1:  $error = '账号长度必须在16个字符以内！'; break;
            case -2:  $error = '账号被占用！'; break;
            case -3:  $error = '机构名称被占用！'; break;
            case -4:  $error = '密码长度必须在6-30个字符之间！'; break;
            case -5:  $error = '邮箱格式不正确！'; break;
            case -6:  $error = '邮箱长度必须在1-32个字符之间！'; break;
            case -7:  $error = '邮箱被禁止注册！'; break;
            case -8:  $error = '邮箱被占用！'; break;
            case -9:  $error = '手机格式不正确！'; break;
            case -10: $error = '手机被禁止注册！'; break;
            case -11: $error = '手机号被占用！'; break;
            case -12: $error = '机构名称必须在1-255个字符之间！'; break;
            case -119: $error = '添加用户出错';break;
            case -13: $error = '通讯地址必须在1-255个字符之间！';break;
            case -14: $error = '请选择挂靠单位！';break;
            case -15: $error = '请选择查新范围！';break;
            default:  $error = '未知错误';
        }
        return $error;
    }

    //获取所有的查新范围
    private function getCategory(){
//        $list = C('TYPEID');
        $list = M('school_type')->where('status=1')->select();
        $this->assign('category',$list);
    }
}