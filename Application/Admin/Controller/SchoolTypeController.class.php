<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/8/26
 * Time: 18:03
 */
namespace Admin\Controller;

class SchoolTypeController extends AdminController{
    public function index(){
        $data = I('post.');
        if(!empty($data['title'])){
            $where['title'] = $data['title'];
        }
        $SchoolType = M('SchoolType');
        $list = $this->lists($SchoolType,$where);
        $this->assign('_list', $list);
        $this->assign('meta_title', '学校所属学科管理');
        $this->display();
    }

    public function add(){
        if(IS_POST){
            $SchoolType=D('SchoolType');
            $data = $SchoolType->create();
            if($data['title'] == '')
            {
                $this->error('学科名称不能为空');
            }
            $result = $SchoolType->where("title='$data[title]'")->find();
            if($result['id'])
            {
                $this->error('学科名称已经存在！');
            }
            if($data){
                $id = $SchoolType->add();
                if($id){
                    action_log('add_school_type','SchoolType',$id,UID);
                    $this->success('新增成功',U('index'));
                }else{
                    $this->error('新增失败');
                }
            }else{
                $this->error($SchoolType->getError());
            }
        }else{
            $this->display('edit');
        }
    }

    /**
     * 删除数据  teng 2016.7.19
     */
    public function del(){
        $id     = intval(I('id'));
        $res    = M('SchoolType')->find($id);
        if(!$res['id'])
        {
            $this->error('参数错误！');
        }
        else
        {
            if(M('SchoolType')->delete($id)){
                //记录行为
                action_log('update_channel', 'channel', $id, UID);
                $this->success('删除成功');
            } else {
                $this->error('删除失败！');
            }
        }
    }

    public function edit($id=0){
        if(IS_POST){
            $SchoolType = D('SchoolType');
            $data = $SchoolType->create();
            if($data['title'] == '')
            {
                $this->error('学科名称不能为空');
            }
            $result = $SchoolType->where("title='$data[title]' and id!=$data[id]")->find();
            if($result['id'])
            {
                $this->error('学科名称已经存在！');
            }
            if($data){
                if($SchoolType->save()){
                    //记录行为
                    action_log('update_channel', 'SchoolType', $data['id'], UID);
                    $this->success('编辑成功', U('index'));
                } else {
                    $this->error('编辑失败');
                }

            } else {
                $this->error($SchoolType->getError());
            }
        } else {
            $info = array();
            /* 获取数据 */
            $info = M('SchoolType')->find($id);
            if(false === $info){
                $this->error('获取配置信息错误');
            }

            $pid = I('get.pid', 0);

            $this->assign('pid', $pid);
            $this->assign('info', $info);
            $this->meta_title = '编辑所属学科';
            $this->display();
        }
    }

    /****************************二级学科*****************************************************/
    public  function more()
    {
        $id     = intval(I('id'));
        $res    = M('SchoolType')->find($id);
        if(!$res['id'])
        {
            $this->error('参数错误！');
        }
        else
        {
            $SchoolType = M('SchoolMore');
            $where      = array('fid'=>$id);
            $list = $this->lists($SchoolType,$where);
            $this->assign('_list', $list);
            $this->assign('id', $id);
            $this->assign('res', $res);
            $this->display();
        }
    }
    //添加二级学科
    public function addMore(){
        if(IS_POST){
            $SchoolType=D('SchoolMore');
            $data = $SchoolType->create();
            if($data['title'] == '')
            {
                $this->error('学科名称不能为空');
            }
            if($data){
                $result = $SchoolType->where("title='$data[title]' and fid=$data[fid]")->find();
                if($result['id'])
                {
                    $this->error('学科名称已经存在！');
                }
                $id = $SchoolType->add();
                if($id){
                    action_log('add_school_more','SchoolMore',$id,UID);
                    $this->success('新增成功',U('SchoolType/more?id='.$data['fid']));
                }else{
                    $this->error('新增失败');
                }
            }else{
                $this->error($SchoolType->getError());
            }
        }else{
            $this->display('editMore');
        }
    }
    //编辑更多学科
    public function editMore($id=0){
        if(IS_POST){
            $SchoolType = D('SchoolMore');
            $data = $SchoolType->create();
            if($data['title'] == '')
            {
                $this->error('学科名称不能为空');
            }
            if($data){
                $result = $SchoolType->where("title='$data[title]' and id!=$data[id] and fid=$data[fid]")->find();
                if($result['id'])
                {
                    $this->error('学科名称已经存在！');
                }
                if($SchoolType->save()){
                    //记录行为
                    action_log('update_channel', 'SchoolMore', $data['id'], UID);
                    $this->success('编辑成功', U('SchoolType/more?id='.$data['fid']));
                } else {
                    $this->error('编辑失败');
                }

            } else {
                $this->error($SchoolType->getError());
            }
        } else {
            $info = array();
            /* 获取数据 */
            $info = M('SchoolMore')->find($id);
            if(false === $info){
                $this->error('获取配置信息错误');
            }

            $pid = I('get.pid', 0);

            $this->assign('pid', $pid);
            $this->assign('info', $info);
            $this->meta_title = '编辑所属学科';
            $this->display();
        }
    }
    /**
     * 删除数据  teng 2016.7.19
     */
    public function delmore(){
        $id     = intval(I('id'));
        $res    = M('SchoolMore')->find($id);
        if(!$res['id'])
        {
            $this->error('参数错误！');
        }
        else
        {
            if(M('SchoolMore')->delete($id)){
                //记录行为
                action_log('update_channel', 'channel', $id, UID);
                $this->success('删除成功');
            } else {
                $this->error('删除失败！');
            }
        }
    }
}