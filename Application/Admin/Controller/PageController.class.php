<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/7/30
 * Time: 18:01
 */

namespace Admin\Controller;
use Think\Controller;

class PageController extends Controller{

    const SERVER = '服务流程';
    const FEES = '收费标准';
    const USERINFORMATION='用户须知';
    const HELP = '使用帮助';
    //流程服务
    public function server(){
        $this->assign('title',self::SERVER);
        $this->display();
    }

    public function fees(){
        $this->assign('title',self::FEES);
        $this->display();
    }

    public function UserInformation(){
        $this->assign('title',self::USERINFORMATION);
        $this->display();
    }

    public function help(){
        $this->assign('title',self::HELP);
        $this->display();
    }
}