<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/7/9
 * Time: 11:26
 */

namespace Admin\Controller;
use Think\Controller;

class ContactController extends AdminController{

    //联系我们
    public function index () {
        if(IS_POST)
        {
            $data = $_POST;
//            if ($data['phone'] != '' )
//            {
//                if (!preg_match("/(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478])\\d{8}$)|(^1705\\d{7}$)/", $data['phone'])) {
//                    $this->error('请输入正确联系方式');
//                }
//            }

            if(strlen($data['code']) > 10)
            {
                $this->error('邮编10个字符以内');
            }
            if(strlen($data['phone']) > 20)
            {
                $this->error('联系电话20个字符以内');
            }
            if(strlen($data['email']) > 50)
            {
                $this->error('邮箱50个字符以内');
            }
            if ($data['email'] != '' )
            {
                if (!preg_match('/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/', $data['email'])) {
                    $this->error('请输入正确邮箱');
                }
            }
            if(strlen($data['url']) > 100)
            {
                $this->error('网址100个字符以内');
            }
            if(strlen($data['address']) > 100)
            {
                $this->error('地址100个字符以内');
            }
            $result = M('Contact')->where('id=1')->save($data);
            if($result)
            {
                $this->success('保存成功');
            }
            else
            {
                $this->error('保存失败');
            }
        }
        else
        {
            $info = M('Contact')->find(1);
            $this->meta_title = '联系我们';
            $this->assign('info',$info);
            $this->display();
        }


    }

}