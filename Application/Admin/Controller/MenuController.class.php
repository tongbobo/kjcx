<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/7/9
 * Time: 11:26
 */

namespace Admin\Controller;
use Think\Controller;

class MenuController extends AdminController{
    public function index(){
        $pid  = I('get.pid',0);
        if($pid){
            $data = M('AdminMenu')->where("id={$pid}")->field(true)->find();
            $this->assign('data',$data);
        }
        $title      =   trim(I('get.title'));
        $type       =   C('CONFIG_GROUP_LIST');
        $all_menu   =   M('AdminMenu')->getField('id,title');
        $map['pid'] =   $pid;
        if($title)
            $map['title'] = array('like',"%{$title}%");
        $list       =   M("AdminMenu")->where($map)->field(true)->order('sort asc,id asc')->select();
        int_to_string($list,array('hide'=>array(1=>'是',0=>'否'),'is_dev'=>array(1=>'是',0=>'否')));
        if($list) {
            foreach($list as &$key){
                if($key['pid']){
                    $key['up_title'] = $all_menu[$key['pid']];
                }
            }
            $this->assign('list',$list);
        }
        // 记录当前列表页的cookie
//        Cookie('__forward__',$_SERVER['REQUEST_URI']);

        $this->meta_title = '菜单列表';
        $this->display();
    }

    //是否启用菜单项目
    public function toogleHide($id,$value = 1){
        $res = D('AdminMenu')->toogleHide($id, $value);
        if($res){
            $this->success('操作成功',U('Menu/index'));
        }else{
            $this->error('操作失败',U('Menu/index'));
        }
    }

    //添加菜单
    public function add(){
        if(IS_POST){
            $Menu = D('AdminMenu');
            $data = $Menu->create();
            if($data){
                $id = $Menu->add();
                if($id){
                    session('ADMIN_MENU_LIST',null);
                    //记录行为
                    action_log('update_menu', 'Menu', $id, UID);
                    $this->success('新增成功');
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($Menu->getError());
            }
        }else {
            $this->assign('info',array('pid'=>I('pid')));
            $menus = M('AdminMenu')->field(true)->select();
            $menus = D('Common/Tree')->toFormatTree($menus);
            $menus = array_merge(array(0=>array('id'=>0,'title_show'=>'顶级菜单')), $menus);
            $this->assign('Menus', $menus);
            $this->meta_title = '新增菜单';
            $this->display('edit');
        }
    }

    //删除菜单
    public function del(){
        $id = array_unique((array)I('id',0));
        if ( empty($id) ) {
            $this->error('请选择要操作的数据!');
        }

        $map = array('id' => array('in', $id) );
        if(M('AdminMenu')->where($map)->delete()){
            session('ADMIN_MENU_LIST',null);
            //记录行为
            action_log('update_menu', 'Menu', $id, UID);
            $this->success('删除成功');
        } else {
            $this->error('删除失败！');
        }
    }

    //编辑菜单
    public function edit($id = 0){
        if(IS_POST){
            $Menu = D('AdminMenu');
            $data = $Menu->create();
            if($data){
                if($Menu->save()!== false){
                    session('ADMIN_MENU_LIST',null);
                    //记录行为
                    action_log('update_menu', 'Menu', $data['id'], UID);
                    $this->success('更新成功', Cookie('__forward__'));
                } else {
                    $this->error('更新失败');
                }
            } else {
                $this->error($Menu->getError());
            }
        } else {
            $info = array();
            /* 获取数据 */
            $info = M('AdminMenu')->field(true)->find($id);
            $menus = M('AdminMenu')->field(true)->select();
            $menus = D('Common/Tree')->toFormatTree($menus);

            $menus = array_merge(array(0=>array('id'=>0,'title_show'=>'顶级菜单')), $menus);
            $this->assign('Menus', $menus);
            if(false === $info){
                $this->error('获取后台菜单信息错误');
            }
            $this->assign('info', $info);
            $this->meta_title = '编辑后台菜单';
            $this->display();
        }
    }

}