<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/7/30
 * Time: 18:01
 */

namespace Home\Controller;
use Think\Controller;

class PageController extends Controller{
    const SERVER = '服务流程';
    const FEES = '收费标准';
    const USERINFORMATION='用户须知';
    const HELP = '使用帮助';
    public function _initialize(){
        $this->assign('username',$_SESSION['k_home_']['user_auth']['username']);
    }
    //流程服务
    public function server(){
//        echo(CONTROLLER_NAME."/".ACTION_NAME);die;
        Cookie('__forward__',U(CONTROLLER_NAME."/".ACTION_NAME));

        $this->assign('title',self::SERVER);
        $this->display();
    }

    public function fees(){
//        echo(CONTROLLER_NAME."/".ACTION_NAME);die;

        Cookie('__forward__',U(CONTROLLER_NAME."/".ACTION_NAME));
        $this->assign('title',self::FEES);
        $this->display();
    }

    public function UserInformation(){
//        echo(CONTROLLER_NAME."/".ACTION_NAME);die;

        Cookie('__forward__',U(CONTROLLER_NAME."/".ACTION_NAME));
        $this->assign('title',self::USERINFORMATION);
        $this->display();
    }

    public function help(){
//        echo(CONTROLLER_NAME."/".ACTION_NAME);die;

        Cookie('__forward__',U(CONTROLLER_NAME."/".ACTION_NAME));
        $this->assign('title',self::HELP);
        $this->display();
    }
}