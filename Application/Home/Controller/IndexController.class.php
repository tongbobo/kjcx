<?php
namespace Home\Controller;
use Think\Controller;
use User\Api\UserApi;
class IndexController extends Controller {
    public function index(){
        define('UID', is_login());
        if(UID){
            $this->redirect('Task/index');
        }else{
            $this->redirect('Desk/index');
        }
    }
}