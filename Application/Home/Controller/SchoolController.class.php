<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-16
 * Time: 下午4:53
 */
namespace Home\Controller;
use Think\Controller;
class SchoolController extends CommonController {
    public function index(){
        $map = array('status'=>1);
        $count = D('Task')->getCount($map);
        $list = D('Task')->getAll($map);
        $this->assign('list',$list);
        $this->assign('count',$count);
        $this->display();
    }

    public function success(){
        $map = array('status'=>2,'department'=>UID);
        $count = D('Task')->getCount($map);
        $list = D('Task')->getAll($map);
        $this->assign('list',$list);
        $this->assign('count',$count);
        $this->display();
    }

    public function finish(){
        $map = array('status'=>3,'department'=>UID);
        $count = D('Task')->getCount($map);
        $list = D('Task')->getAll($map);
        $this->assign('list',$list);
        $this->assign('count',$count);
        $this->display();
    }

    /*
     * 科技查新日志页面
     */
    public function log(){
        $map = array(
            'uid'=>UID
        );
        $list = D('Tasklog')->where($map)->select();
        $this->assign('list',$list);
        $this->display();
    }
    public function accept(){
        $data = array(
            'id'=>I('id'),
            'department' => UID,
            'status' => 2,
            'is_push'=> 2
        );
        $res = D('Task')->push($data);
        if(!$res){
            $this->error(D('Task')->getError());
        }else{
            D('Tasklog')->update('接收',$data['id']);
            $this->redirect('School/success');
        }
    }

    public function complete(){
        $data = array(
            'id'=>I('id'),
            'status' => 3,
        );
        $res = D('Task')->finish($data);
        if(!$res){
            $this->error(D('Task')->getError());
        }else{
            D('Tasklog')->update('完成任务',$data['id']);
            $this->redirect('School/finish');
        }
    }
    /*
     * 撤销任务
     */
    public function untread(){
        $data = array(
            'id'=>I('id'),
            'status'=>1
        );
        $res = D('Task')->untread($data);
        if(!$res){
            $this->error(D('Task')->getError());
        }else{
            D('Tasklog')->update('撤销任务',$data['id']);
            $this->redirect('School/index');
        }
    }


}