<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/8/3
 * Time: 12:11
 */

namespace Home\Controller;

class AccountController extends CommonController{
    //显示统计页面
    public function index(){
        $where = array();
        $search = I('get.');
        if(!empty($search['process_id'])){
            switch($search['process_id']){
                case '1':
                    $where['process_id'] = array('NEQ',14);
                        break;
                case '2':
                    $where['process_id'] = 14;
                        break;
                case '3':
                    $where['process_id'] = -1;
                    break;
            }
        }
        if(!empty($search['department']) && $search['department']!='all'){
            $where['department_id'] = $search['department'];
        }
        if(!empty($search['order']) && $search['department']!=3){
            if($search['order']==1){
                $order = 'charge  desc';
            }
        }
        $where['user_id'] = UID;
        $Task = M('Kjcx');
        $map['charge'] = array('exp','is not null');//查找不为空的费用
        $charge = $Task ->where($where)->where($map)->field('charge')->select();
        if($order&&$charge){//费用都为空的话还是按照id的降序
            $lists = $this->lists($Task,$where,$order);
        }else{
            $lists = $this->lists($Task,$where);
        }
        if($search['sub']=="导出"){
            $where = array();
            $search = I('get.');
            if(!empty($search['process_id'])){
                switch($search['process_id']){
                    case '1':
                        $where['process_id'] = array('NEQ',14);
                        break;
                    case '2':
                        $where['process_id'] = 14;
                        break;
                    case '3':
                        $where['process_id'] = -1;
                        break;
                }
            }
            if(!empty($search['department']) && $search['department']!='all'){
                $where['department_id'] = $search['department'];
            }
            if(!empty($search['order']) && $search['department']!=3){
                if($search['order']==1){
                    $order = 'charge  desc';
                }
            }
            $where['user_id'] = UID;
            $Task = M('Kjcx');
            $map['charge'] = array('exp','is not null');//查找不为空的费用
            $charge = $Task ->where($where)->where($map)->field('charge')->select();
            if($order&&$charge){//费用都为空的话还是按照id的降序
                $arr=M('Kjcx')->where($where)->order(array('id'=>UID))->select();
            }else{
                $arr=M('Kjcx')->where($where)->select();
            }
            $outputFileName=time().".xls";
            $path = "./uploads/extend/".date("Ym");
            if(!file_exists($path)){
                mkdir($path);
            }
            $outputFileName= iconv("utf-8","gb2312//IGNORE",$outputFileName);
            $down_file="$path/$outputFileName";
            vendor('PHPExcel.PHPExcel.PHPExcel');
            vendor('PHPExcel.PHPExcel.IOFactory');
            vendor('PHPExcel.Writer.Excel5');
            $objExcel = new \PHPExcel();
            $objExcel->setActiveSheetIndex(0);
            $objActSheet = $objExcel->getActiveSheet();
            $objActSheet->mergeCells('A1:R1');
            $objStyleA5 = $objActSheet->getStyle('A1');
            $objAlignA5 = $objStyleA5->getAlignment();
            $objAlignA5->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objAlignA5->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objActSheet->setCellValue('A2', "委托人");
            $objActSheet->setCellValue('B2', "委托人联系方式");
            $objActSheet->setCellValue('C2', "联系人");
            $objActSheet->setCellValue('D2', "联系人联系方式");
            $objActSheet->setCellValue('E2', "委托单位");
            $objActSheet->setCellValue('F2', "项目中文名称");
            $objActSheet->setCellValue('G2', "项目英文名称");
            $objActSheet->setCellValue('H2', "查新号");
            $objActSheet->setCellValue('I2', "查新所属学科");
            $objActSheet->setCellValue('J2', "查新目的");
            $objActSheet->setCellValue('K2', "详细查新目的");
            $objActSheet->setCellValue('L2', "项目级别");
            $objActSheet->setCellValue('M2', "查新要求");
            $objActSheet->setCellValue('N2', "查新期望完成日期");
            $objActSheet->setCellValue('O2', "查新处理机构");
            $objActSheet->setCellValue('P2', "查新员");
            $objActSheet->setCellValue('Q2', "审核员");
            $objActSheet->setCellValue('R2', "查新完成时间");
            $objActSheet->setCellValue('S2', "查新范围");
            $objActSheet->setCellValue('T2', "缴费方式");
            $objActSheet->setCellValue('U2', "报告领取方式");
            $i=3;
            foreach($arr as $key=>$vo){
                $objActSheet->setCellValue('A'.$i,$vo['wtrname']);
                $objActSheet->setCellValue('B'.$i,$vo['wtremail']);
                $objActSheet->setCellValue('C'.$i,$vo['lxrname']);
                $objActSheet->setCellValue('D'.$i,$vo['phone']);
                $objActSheet->setCellValue('E'.$i,$vo['wtdw']);
                $objActSheet->setCellValue('F'.$i,$vo['xmmc']);
                $objActSheet->setCellValue('G'.$i,$vo['xmmcen']);
                $objActSheet->setCellValue('H'.$i,$vo['cxh']);
                $arr=M('school_type')->where(array("id"=>$vo['xkfl']))->find();
                $objActSheet->setCellValue('I'.$i,$arr['title']);
                if($vo['cxmd']==1){
                    $objActSheet->setCellValue('J'.$i,"科研立项");
                }elseif($vo['cxmd']==2){
                    $objActSheet->setCellValue('J'.$i,"鉴定、验收、评价");
                }elseif($vo['cxmd']==3){
                    $objActSheet->setCellValue('J'.$i,"奖励申报");
                }elseif($vo['cxmd']==4){
                    $objActSheet->setCellValue('J'.$i,"申请专利");
                }elseif($vo['cxmd']==5){
                    $objActSheet->setCellValue('J'.$i,"博硕士论文开题");
                }else{
                    $objActSheet->setCellValue('J'.$i,"其他");
                }


                $objActSheet->setCellValue('K'.$i,$vo['cxmd_other']);
                if($vo['xmjb']==1){
                    $objActSheet->setCellValue('L'.$i,"国家级");
                }elseif($vo['xmjb']==2){
                    $objActSheet->setCellValue('L'.$i,"省部级");
                }elseif($vo['xmjb']==3){
                    $objActSheet->setCellValue('L'.$i,"市级");
                }else{
                    $objActSheet->setCellValue('L'.$i,"其他");
                }
                if($vo['cxyq']==1){
                    $objActSheet->setCellValue('M'.$i,"希望查新机构通过查新，证明在所查范围内国内外有无相同或类似研究");
                }elseif($vo['cxyq']==2){
                    $objActSheet->setCellValue('M'.$i,"希望查新机构对查新项目分别或综合进行国内外对比分析");
                }else{
                    $objActSheet->setCellValue('M'.$i,"查新委托人提出的其他愿望");
                }

                $objActSheet->setCellValue('N'.$i,$vo['lqrq']);

                $res=M('department')->where(array('id'=>$vo['department_id']))->find();
                $objActSheet->setCellValue('O'.$i,$res['name']);



                $objActSheet->setCellValue('P'.$i,$vo['user_id_6']);
                $objActSheet->setCellValue('Q'.$i,$vo['user_id_9']);
                $objActSheet->setCellValue('R'.$i,$vo['intime_13']);
                if($vo['cxfw']==1){
                    $objActSheet->setCellValue('S'.$i,"希望查新机构通过查新，证明在所查范围内国内外有无相同或类似研究 ");
                }else if($vo['cxfw']==2){
                    $objActSheet->setCellValue('S'.$i,"希望查新机构对查新项目分别或综合进行国内外对比分析");
                }else{
                    $objActSheet->setCellValue('S'.$i," 查新委托人提出的其他愿望 ");

                }
                if($vo['fkfs']==1){
                    $objActSheet->setCellValue('T'.$i,"网上支付");
                }elseif($vo['fkfs']==3){
                    $objActSheet->setCellValue('T'.$i,"现金支付");
                }else{
                    $objActSheet->setCellValue('T'.$i,"汇款凭证上传");
                }

                if($vo['fkfs']==1){
                    $objActSheet->setCellValue('U'.$i,"自取");
                }else if($vo['fkfs']==2){
                    $objActSheet->setCellValue('U'.$i,"快递");
                }
                $i++;
            }
            $objWriter = new \PHPExcel_Writer_Excel5($objExcel);     // 用于其他版本格式
            $objWriter->save($down_file);
            ob_end_clean();
            $fp = fopen($down_file, "r");
            header('Last-Modified: '.gmdate('D, d M Y H:i:s',time()).' GMT');
            header('Pragma: no-cache');
            header('Content-Encoding: none');
            header("Content-Type: application/force-download");
            header("Content-Transfer-Encoding: binary");
            header("Content-type: application/octet-stream");
            header("Accept-Ranges: bytes");
            header("Content-Disposition: attachment; filename=$outputFileName");
            header('Accept-Length: ' . filesize($down_file));
            while (!feof($fp)) {
                echo fread($fp, 1024);
            }
            fclose($fp);
            flush();
            ob_flush();
        }
        $this->assign('search',$search);
        $this->assign('_list',$lists);
        if(isset($_GET['p'])){
            $num = ($_GET['p']-1)*10;
            $this->assign('num', $num);
        }
        $this->assign('meta_title', '委托统计');
        $this->display();
    }

    //导出统计信息
    public function order_export(){
        $where = array();
        $condition = I('post.status');
        switch($condition){
            case '0':
                unset($where);
                break;
            case '1':
                $where['status'] = 4;
                break;
            case '2':
                $where['status'] = 1;
                break;
            case '3':
                $where['status'] = 5;
                break;
            case '4':
                $where['departmentId'] = 1;

        }
        $Task = M('Task');
        $orders = $Task->where($where)->select();
        $data = array();
        foreach ($orders as $k=>$goods_info){
            $data[$k][id] = $goods_info['id'];
            $data[$k][title] = $goods_info['title'];
            $data[$k][PNO] = $goods_info['PNO'];
            $data[$k][old_PNO] = $goods_info['old_PNO'];
            $data[$k][price]  = $goods_info['price'];
            $data[$k][brand_id]  = $goods_info['brand_id'];
            $data[$k][category_id]  = $goods_info['category_id'];
            $data[$k][type_ids] = $goods_info['id'];
            $data[$k][add_time] = $goods_info['add_time'];
        }
        foreach ($data as $field=>$v){
            if($field == 'id'){
                $headArr[]='报告编号';
            }

            if($field == 'departmentId'){
                $headArr[]='处理机构';
            }

            if($field == 'anencyId'){
                $headArr[]='查新员';
            }

            if($field == 'tel'){
                $headArr[]='联系方式';
            }

            if($field == ''){
                $headArr[]='处理状态';
            }

            if($field == 'status'){
                $headArr[]='完成情况';
            }

            if($field == ''){
                $headArr[]='完成日期';
            }
            if($field == 'price'){
                $headArr[]='费用';
            }
        }

        $filename="order_list";

        $this->getExcel($filename,$headArr,$data);
    }

    private  function getExcel($fileName,$headArr,$data){
        //导入PHPExcel类库，因为PHPExcel没有用命名空间，只能inport导入
        import("Org.Util.PHPExcel");
        import("Org.Util.PHPExcel.Writer.Excel5");
        import("Org.Util.PHPExcel.IOFactory.php");

        $date = date("Y_m_d",time());
        $fileName .= "_{$date}.xls";

        //创建PHPExcel对象，注意，不能少了\
        $objPHPExcel = new \PHPExcel();
        $objProps = $objPHPExcel->getProperties();

        //设置表头
        $key = ord("A");
        //print_r($headArr);exit;
        foreach($headArr as $v){
            $colum = chr($key);
            $objPHPExcel->setActiveSheetIndex(0) ->setCellValue($colum.'1', $v);
            $objPHPExcel->setActiveSheetIndex(0) ->setCellValue($colum.'1', $v);
            $key += 1;
        }

        $column = 2;
        $objActSheet = $objPHPExcel->getActiveSheet();

        //print_r($data);exit;
        foreach($data as $key => $rows){ //行写入
            $span = ord("A");
            foreach($rows as $keyName=>$value){// 列写入
                $j = chr($span);
                $objActSheet->setCellValue($j.$column, $value);
                $span++;
            }
            $column++;
        }

        $fileName = iconv("utf-8", "gb2312", $fileName);
        //重命名表
        //$objPHPExcel->getActiveSheet()->setTitle('test');
        //设置活动单指数到第一个表,所以Excel打开这是第一个表
        $objPHPExcel->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=\"$fileName\"");
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output'); //文件通过浏览器下载
        exit;
    }




}