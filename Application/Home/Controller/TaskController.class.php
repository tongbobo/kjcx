<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-15
 * Time: 下午6:22
 */
namespace Home\Controller;
use Admin\Model\UserModel;
use User\Api\UserApi;
use Think\Controller;
class TaskController extends CommonController {

    public function index(){
        $status = I('status')?I('status'):0;
        $search = I('get.');
        switch($status){
            //委托中
            case '1':
                $where['process_id'] = -2;
                break;
            //处理中
            case '2':
                $where['process_id'] = array(array('gt',-1), array('neq', 14));
                break;
            //已经完成
            case '3':
                $where['process_id'] = 14;
                break;
            //任务驳回
            case '4':
                $where['process_id'] = -1;
                break;
        }
        $where['user_id'] = UID;
        if(!empty($search['time'])){
            $mintime = trim($search['time']);
//            $maxtime = date('Y-m-d',(strtotime($search['time'])+86400));
//            $where['intime'] =  array(array('gt',$mintime),array('lt',$maxtime));
           $where['intime']=array('LIKE',"%$mintime%");
        }
        if(!empty($search['xmmc'])){
            $xmmc = $search['xmmc'];
            $where['xmmc'] = array('LIKE',"%$xmmc%");
        }
        if(!empty($search['cxh'])){
            $cxh =trim($search['cxh']);
            $where['cxh'] = array('LIKE',"%$cxh%");
        }

        //判断后台查新站默认设置并设置了学校  2016.7.20   teng
        $default = C('SCHOOL_CONFIG');
        if($default == 2)
        {
            $res = M('Option')->where('type=2 and name="school_config"')->select();
            if(!$res['value'])
            {
                foreach($res as $viem){
                    $arr['department_id']   = $viem['value'];
                    $arr['process_id']      = 0;
                    M('kjcx')->where("lqrq between '$viem[start_day]' and '$viem[end_day]'  and process_id = -2")->save($arr);  //符合条件的改变状态
                }
            }
            else
            {
                $this->error('请设置默认时间内的学校！');
            }
        }

        $Kjcx = M('Kjcx');
        $ros='10';
//        $res=M('cx_process')->select();
//        foreach($res as $key=>$vo){
//            if(preg_match('/\,/i',$vo['value'])){
//                $arr1 = explode(",",$vo['value']);
//                foreach($arr1 as $vo_1){
//                    $arr[$vo_1] = $vo['title'];
//                }
//            }else{
//                $arr[$vo['value']] = $vo['title'];
//            }
//
//        }
        $list = $this->lists($Kjcx,$where,'','',$ros);
		//var_dUMp($list);exit;
        $list = getRestTime($list);
//        print_r($list);die;
        $this->assign('_list',$list);
        $this->assign('status', $status);
        $this->assign('meta_title', '业务跟踪');
        if(isset($_GET['p'])){
            $num = ($_GET['p']-1)*10;
            $this->assign('num', $num);
        }
        $this->display();
    }

	public function set_queren()
	{
		$id = I('id');
		$process_id = I('process_id');
		$qryy = I('qryy');
		$M = M('Kjcx');

		$res = $M -> where( 'id = '.$id ) -> save( array('process_id' =>$process_id, 'qrneirong' =>$qryy ));

		echo json_encode(array('code' => $res ));
	}

    public function add(){
        if(session('user_auth') == '')
        {
            $this->error('请先登陆！');
        }
//        Cookie('__forward__',$_SERVER['REQUEST_URI']);
//        Cookie('__forward__',U(CONTROLLER_NAME."/".ACTION_NAME));
        $now	    =	date('Y-m-d H:i:s');//获取当前时间
        if(IS_POST){
            $taskData = I('post.');//260,259,293,282,243,292;
			
			$taskData[sf_user_name]	    ='259';	//收费人员
    		$taskData[intime]	    =	$now;//业务申请时间
            $taskData[intime_copy]  =   $now;//业务申请时间 备用
            $taskData['cxh']	    =   date('YmdHis');//直接生产查新号
            if($_SESSION['user_auth']['uid'])
            {
                $taskData['user_id'] = $_SESSION['user_auth']['uid'];
            }
            if($taskData['department_id'] ==''){
                $taskData[process_id]	=	-2 ;//查新委托未选择机构是状态则为：委托中
            }else{
                $taskData[process_id]	=	20;//查新委托选择机构是状态则为：人员分配
            }
            //基本信息过滤
            if(($taskData['cxfw'] == 2 || $taskData['cxfw'] == 3)  && $taskData['xmmcen'] == ''){
                $this->error('请填写项目英文名称');
                return false;
            }
            //if($taskData['xkfl'] == '')
            //{
            //    $this->error('请选择项目所属学科!');
            //}
            //if(in_array(1,$taskData['cxmd'])  && $taskData['xxcxmd'] == ''){
            //    $this->error('请填写详细查新目的');
            //    return false;
            //}
            if(in_array(3,$taskData['cxyq'])  && $taskData['qtyq'] == ''){
                $this->error('请填写委托人其他愿望');
                return false;
            }
			
            //if($taskData[cxmd]){$taskData[cxmd]=implode(',',$taskData[cxmd]);}//将查新目的格式化存到数组中
            if($taskData[cxyq]){$taskData[cxyq]=implode(',',$taskData[cxyq]);}//将查新要求格式化存到数组中
//            $array = $this->prove_array();
//            foreach($array as $key=>$vo){
//                if($taskData[$vo['title']] == ''){
//                    $this->error($vo['msg']);
//                    return false;
//                }
//            }
            if(($taskData['cxfw'] == 2 || $taskData['cxfw'] == 3)  && $taskData['keywords_en'] == ''){
                $this->error('请填写外文检索词');
                return false;
            }
            if($taskData['fkfs'] == 2 && $taskData['upload_ids'] == ''){
                $this->error('请上传汇款凭证！');
            }
            if($taskData['dwsh'] == 2 && $taskData['kpdw'] == ''){
                $this->error('请填写发票抬头！');
            }
            if($taskData['qbfs'] == 2 && $taskData['kddz'] == ''){
                $this->error('请填写收件地址！');
            }
            define('UID', is_login());
            if(UID)    //登录的人也可以在前台申请
            {
                $taskData['user_id'] = UID;
            }
			
            $kjcxId = D('Kjcx')->update($taskData);
            if($kjcxId>0)
            {
                //根据IP地址判断用户是否分配
                $auto = $this->auto($kjcxId);
                if($auto){
                    //如果IP地址存在,指派任务
                    $res = D('Kjcx')->call($kjcxId,$auto);
                    if($res){
                        action_log('grab_task','Task',$kjcxId,$auto);
                    }
                }
                $this->success('业务成功提交',U('Task/index'));
            }
            else
            {
                $this->error($this->showRegError($kjcxId));
            }
        }else{
            $templete_arr = M('Templete')->select();
            $this->assign('templete_arr',$templete_arr);

            $this->assign('user_message',$this->getUserInfo());
            //获取所有机构
            $jgList = S('Jiang_catch');
            $this->assign('list', $jgList);
            //获取省市
            $this->assign('cityList',getProvince());

            //获取学校学科类型
            $this->assign('schoolList',getSchoolType());

            $this->assign('now',$now);//返回当前时间到页面
            $this -> assign('kjcx_title','查新委托');//标题
            $this->display();
        }
    }

    //后台添加委托   2016.5.30  滕健
    public function a_add(){
        //用于新增委托时的的图片显示
        if(I('file_id'))
        {
            R('Task/down_file',array('file_id'=>I('file_id')));
        }
//        Cookie('__forward__',$_SERVER['REQUEST_URI']);
//        Cookie('__forward__',U(CONTROLLER_NAME."/".ACTION_NAME));
        if(I('user_type') == 1){
            $where['uid']    = UID;
            $list   = M('UserInfo')->where($where)->select();
            $this->assign('_list',$list);

            $this->assign('meta_title','用户基本信息');
            $this -> assign('user_type',I('user_type'));
            $this -> display('User/index');
        }else {
            $now = date('Y-m-d H:i:s');//获取当前时间
            if (IS_POST) {
                $taskData = I('post.');
				$taskData[sf_user_name]	=	259;//收费人员
                if(($taskData['cxmd']) == ''){
                    $taskData['cxmd'] = '';
                }
                if(($taskData['cxyq']) == ''){
                    $taskData['cxyq'] = '';
                }
                //基本信息过滤
                if(($taskData['cxfw'] == 2 || $taskData['cxfw'] == 3)  && $taskData['xmmcen'] == ''){
                    $this->error('请填写项目英文名称');
                    return false;
                }

                //if($taskData['xkfl'] == '')
                //{
                //    $this->error('请选择项目所属学科!');
                //}
                //if(in_array(1,$taskData['cxmd'])  && $taskData['xxcxmd'] == ''){
                //    $this->error('请填写详细查新目的');
                //    return false;
                //}
                if(in_array(3,$taskData['cxyq'])  && $taskData['qtyq'] == ''){
                    $this->error('请填写委托人其他愿望');
                    return false;
                }
                if($taskData[cxmd]){$taskData[cxmd]=implode(',',$taskData[cxmd]);}//将查新目的格式化存到数组中
                if($taskData[cxyq]){$taskData[cxyq]=implode(',',$taskData[cxyq]);}//将查新要求格式化存到数组中
                if(($taskData['cxfw'] == 2 || $taskData['cxfw'] == 3)  && $taskData['keywords_en'] == ''){
                    $this->error('请填写外文检索词');
                    return false;
                }
                //得到默认的地址和信息
                $where['uid']        = UID;
                $where['type']       = 1;
                $where['default']    = 1;
                $info = M('UserInfo')->where($where)->find();
                if(!$info){
                    $this -> error('请设置默认地址');
                }
                $where['type']       = 2;
                $info2 = M('UserInfo')->where($where)->find();
                if(!$info2){
                    $this -> error('请设置默认缴费方式');
                }
                //个人信息
                $taskData['wtrname']        = $info['wtrname'];
                $taskData['wtrphone']       = $info['wtrphone'];
                $taskData['lxrname']        = $info['lxrname'];
                $taskData['phone']          = $info['phone'];
                $taskData['wtdw']           = $info['wtdw'];
                $taskData['txdz']           = $info['txdz'];
                $taskData['lxryzbm']        = $info['lxryzbm'];
                $taskData['wtremail']       = $info['wtremail'];

                //上传信息
                $taskData['upload_jxjx'] = $info2['id'];
                $taskData['fkfs']        = $info2['fkfs'];
                $taskData['kpdw']        = $info2['kpdw'];
                $taskData['dwsh']        = $info2['dwsh'];
                $taskData['qbfs']        = $info2['qbfs'];
                $taskData['kddz']        = $info2['kddz'];
                $taskData['upload_ids']  = $info2['upload_ids'];

                $taskData['wtr_name']    = $info['wtrname'];
                $taskData['user_id']     = UID;
                //将查新目的格式化存到数组中
                $taskData[intime]       = date('Y-m-d H:i:s');//业务申请时间
                $taskData[intime_copy]  = date('Y-m-d H:i:s');//业务申请时间 备用
                $taskData['cxh']	    = date('YmdHis');//直接生产查新号
                if ($taskData['department_id'] == '') {
                    $taskData[process_id] = -2;//查新委托未选择机构是状态则为：委托中
                } else {
                    $taskData[process_id] = 20;//查新委托选择机构是状态则为：人员分配
                }
//                print_r($taskData);die;
                $kjcxId = D('Kjcx')->update($taskData);
                //2016.6.17 teng
                if($kjcxId>0)
                {
                    action_log('add_task', 'Home', $kjcxId, UID);
                    //根据IP地址判断用户是否分配
                    $auto = $this->auto($kjcxId);
                    if($auto){
                        //如果IP地址存在,指派任务
                        $res = D('Kjcx')->call($kjcxId,$auto);
                        if($res){
                            action_log('add_task', 'Home', $kjcxId, UID);
                        }
                    }
                    $log_kjcx['task_id'] = $kjcxId;
                    $log_kjcx['user_bh'] = UID;
                    $log_kjcx['ip'] = UID;
                    $log_kjcx['contents'] = '任务创建成功，等待受理';
                    $log_kjcx['process_id'] = 0;
                    $log_kjcx['intime'] = $taskData[intime];
                    M('log_kjcx')->data($log_kjcx)->add();
                    $this->success('业务成功提交',U('Task/index'));
                }
                else
                {
                    $this->error($this->showRegError($kjcxId));
                }
            } else {
                $where['uid']        = UID;
                $where['type']       = 1;
                $where['default']    = 1;
                $info = M('UserInfo')->where($where)->getField('wtrname');

                $this->assign('user_message', $this->getUserInfo());
                //获取所有机构
                $jgList = S('Jiang_catch');
                $this->assign('list', $jgList);

                //获取学校学科类型
                $this->assign('schoolList', getSchoolType());

                $this->assign('wtrname', $info);
                $this->assign('meta_title', '查新委托');
                $this->assign('now', $now);//返回当前时间到页面
                $this->display();
            }
        }
    }

    /**
     *判断是否默认值选中
     */
    public function set_moren (){
        if(IS_POST){
            //得到填写的委托人的信息
            $where['uid']        = UID;
            $where['type']       = 1;
            $where['default']    = 1;
            $info = M('UserInfo')->where($where)->getField('wtrname');
            if(!$info){
                $result['result']   = 0;
                $result['msg']   = '请设置委托人信息';
                $this -> ajaxReturn($result);
            }
            $where['type']       = 2;
            $info2 = M('UserInfo')->where($where)->getField('fkfs');
            if(!$info2){
                $result['result']   = 0;
                $result['msg']   = '请设置缴费方式';
                $this -> ajaxReturn($result);
            }
            $result['result'] =1;
            $this -> ajaxReturn($result);
        }
    }
    /**
    *得到用户之前填写的信息
     *
     */
    public function userinfo () {
        $where['uid']   = UID;
        $where['id']    = I('id');
        $model          = M('UserInfo');
        if(I('type') == 1){         //个人信息
            $where['type'] = 1;
            $arr = $model->where($where)->find();
        }else{                      //缴费方式
            $where['type'] = 2;
            $arr = $model->where($where)->find();
        }
        if($arr==array()){
            $this->ajaxReturn('');
        }else{
            $this->ajaxReturn($arr);
        }
    }
    //编辑
    public function edit(){
        $id = I('get.id');
        if(IS_POST){
            $data = I('post.');
            if(($data['cxmd']) == ''){
                $data['cxmd'] = '';
            }
            if(($data['cxyq']) == ''){
                $data['cxyq'] = '';
            }
            if($data['xkfl'] == ''){
                $this->error('请选择学科');
                return false;
            }
            if(($data['cxfw'] == 2 || $data['cxfw'] == 3)  && $data['xmmcen'] == ''){
                $this->error('请填写项目英文名称');
                return false;
            }

            if($data['cxmd']==1  && $data['xxcxmd'] == ''){
                $this->error('请填写详细查新目的');
                return false;
            }
            //if($data[cxmd]){$data[cxmd]=implode(',',$data[cxmd]);}//将查新目的格式化存到数组中
            if($data[cxyq]){$data[cxyq]=implode(',',$data[cxyq]);}//将查新要求格式化存到数组中
//            $array = $this->prove_array();
//            foreach($array as $key=>$vo){
//                if($data[$vo['title']] == ''){
//                    $this->error($vo['msg']);
//                    return false;
//                }
//            }
            if(($data['cxfw'] == 2 || $data['cxfw'] == 3)  && $data['keywords_en'] == ''){
                $this->error('请填写外文检索词');
                return false;
            }

            $kjcxId = D('Kjcx')->update($data);
            //2016.6.17 teng
            if($kjcxId>0)
            {
                action_log('add_task', 'Home', $kjcxId, UID);
                //根据IP地址判断用户是否分配
                $auto = $this->auto($kjcxId);
                if($auto){
                    //如果IP地址存在,指派任务
                    $res = D('Kjcx')->call($kjcxId,$auto);
                    if($res){
                        action_log('add_task', 'Home', $kjcxId, UID);
                    }
                }
                $this->success('业务成功提交',U('Task/index'));
            }
            else
            {
                $this->error($this->showRegError($kjcxId));
            }
        }else{
            $id   = intval(I('id'));
            $task = M('Kjcx')->find($id);
            if(empty($task))
            {
                $this->error('数据不存在');
            }
            else
            {
                //根据学科id得到对应的所有二级学科
                $subject_er = M('SchoolMore')->find($task['xkfl']);
                $task['subject_yj'] = $subject_er['fid'];
                $sub_arr = M('SchoolMore')->where("fid=$subject_er[fid] and status=1")->getField('id,title');
                $this->assign('sub_arr', $sub_arr);
                $this->assign('info', $task);
                $sqfj_arr = explode(',', $task['sqfj']);
                $sqfj_arr_new = array();
                //显示附件名称
                if (!empty($sqfj_arr)) {
                    foreach ($sqfj_arr as $k => $vo) {
                        if ($vo != '') {
                            $m = M('Attachment')->where('id=' . $vo)->field('id,title')->find();
                            $sqfj_arr_new[$k]['title'] = $m['title'];
                            $sqfj_arr_new[$k]['id'] = $m['id'];
                        }
                    }
                }
                $this->assign('sqfj_arr', $sqfj_arr_new);
                $this->assign('cxmd_arr', explode(',', $task['cxmd']));
				
                $upload_ids_arr = explode(',', $task['upload_ids']);
                $this->assign('upload_ids_arr', $upload_ids_arr);
                //得到查新机构
                if ($task['department_id'] != '') {
                    $jigou = M('Department')->where('id=' . $task['department_id'])->getField('name');
                    $this->assign('jigou', $jigou);
                }
                //查新要求
                $this->assign('cxyq', explode(',', $task['cxyq']));
                $this->assign('user_message', $this->getUserInfo());
               //获取所有机构
                $jgList = S('Jiang_catch');
                $this->assign('list', $jgList);

                $this->display();
            }
        }
    }
//浏览
    public function browser(){
        $id   = intval(I('id'));
    	$task = M('Kjcx')->find($id);
        if(I('tag'))                //显示高亮
        {
            $this->assign('tag', intval(I('tag')));
        }

        if(empty($task))
        {
            $this->error('数据不存在');
        }
        else
        {
            $this->assign('info', $task);
            $sqfj_arr = explode(',', $task['sqfj']);
            $sqfj_arr_new = array();
            //显示附件名称
            if (!empty($sqfj_arr)) {
                foreach ($sqfj_arr as $k => $vo) {
                    if ($vo != '') {
                        $m = M('Attachment')->where('id=' . $vo)->field('id,title')->find();
                        $sqfj_arr_new[$k]['title'] = $m['title'];
                        $sqfj_arr_new[$k]['id'] = $m['id'];
                    }
                }
            }
            $this->assign('sqfj_arr', $sqfj_arr_new);
            $upload_ids_arr = explode(',', $task['upload_ids']);
            $this->assign('upload_ids_arr', $upload_ids_arr);
            $this->display();
        }
    }

//进度跟踪
    public function p_track(){
    	$where['record_id']	=	I('id');
//        $where['uid'] = UID;
    	$list = M('ActionLog')->where($where)->select();
    	$this->assign('list',$list);
    	$this->display();
    }

//用户终止任务
	public function stop_task(){
    	$data['id']			=	I('id');
    	$data['process_id']	=	15;
        $res = D('Kjcx')->update($data);
        if($res){
            action_log('revoke_task','task',$data['id'],UID);
            $this->success('任务已终止',U('index'));
        }
	}

//重新提交任务
	public function tj_task_again(){
		$data['id']			=	I('id');
    	$data['process_id']	=	0;
        $res = D('Kjcx')->update($data);
        if($res){
            action_log('add_task','Task',$data['id'],UID);
            $this->success('重新提交成功');
        }
	}

    //重新提交任务
    public function tj_task_again2(){
        $data['id']			=	I('id');
        $data['process_id']	=	-2;
        $data['department_id']	=	'';
        $res = D('Kjcx')->update($data);
        if($res){
            action_log('add_task','Task',$data['id'],UID);
            $this->success('重新提交成功');
        }
    }

//    根据传入的id得到二级学科、
    public function subject()
    {
       $fid = $_POST['fid'];
       $list = M('SchoolMore')->where('fid='.$fid)->select();
        if($list)
        {
            $result['state'] = 1;
            $result['list']  = $list;
        }
        else
        {
            $result['state'] = '';
        }
        $this->ajaxReturn($result);

    }
    public function upload(){
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize   =     3145728 ;// 设置附件上传大小
        $tmp_can	=	$_GET['can']?$_GET['can']:0;
        if($tmp_can==1){
            $upload->exts      =     array('jpg','png','jpeg','gif','bmp');// 设置附件上传类型
        }else{
            $upload->exts      =     array('doc', 'rar','zip','docx');// 设置附件上传类型
        }
        $upload->rootPath  =     './uploads/user/'; // 设置附件上传根目录
        $upload->savePath  =     ''; // 设置附件上传（子）目录
		$tmp_name	=	$_GET['tmp_name'];

        // 上传文件
        $info   =   $upload->upload();
        if(!$info) {// 上传错误提示错误信息
            echo json_encode(array('state'=>$upload->getError()));
        }else{// 上传成功
            //print_R($info);exit;
            $file=array(
                'uid'=>UID,
                'title'=>$info[$tmp_name]['name'],
                'title_r'=>$info[$tmp_name]['savename'],
                'type'=>'1',
                'size'=>$info[$tmp_name]['size'],
                'dir'=>$upload->rootPath.$info[$tmp_name]['savepath'],
                'create_time'=>date('Y-m-d H:i:s')
            );
            $id = M('Attachment')->add($file);
            echo json_encode(array('state'=>'ok','upload_id'=>$id,'base_file_n'=>$file['title']));
        }
    }

    public function down_file(){
        $file_id=$_GET['file_id'];
//        $file_id=think_decrypt(intval(trim($_GET['file_id'])));
//        echo think_decrypt('MDAwMDAwMDAwMMh1bqE');die;
        if(empty($file_id)){
            redirect("文件不存在跳转页面");
        }else{//如果要加登录条件，这里可以自己加
            $map['id'] = $file_id;
            $list=D('Attachment')->where($map)->select();
            if ($list == false) {//文件不存在，可以跳转到其它页面
                header('HTTP/1.0 404 Not Found');
                header('Location: .');
            } else {
                $file_name="./".$list[0]['dir'].$list[0]['title_r'];//需要下载的文件
                $file_name=iconv("utf-8","gb2312","$file_name");
                if(file_exists($file_name)){
                    $fp=@fopen($file_name,"r+");//下载文件必须先要将文件打开，写入内存
                    if(!file_exists($file_name)){//判断文件是否存在
                        $this->error('文件不存在！');
                    }
                    $file_size=filesize($file_name);//判断文件大小
                    //返回的文件
                    Header("Content-type: application/octet-stream");
                    //按照字节格式返回
                    Header("Accept-Ranges: bytes");
                    //返回文件大小
                    Header("Accept-Length: ".$file_size);
                    //弹出客户端对话框，对应的文件名
                    Header("Content-Disposition: attachment; filename=".$list[0]['title']);
                    //防止<span id="2_nwp" style="width: auto; height: auto; float: none;"><a id="2_nwl" href="http://cpro.baidu.com/cpro/ui/uijs.php?c=news&cf=1001&ch=0&di=128&fv=16&jk=208efa6f3933ab0f&k=%B7%FE%CE%F1%C6%F7&k0=%B7%FE%CE%F1%C6%F7&kdi0=0&luki=3&n=10&p=baidu&q=06011078_cpr&rb=0&rs=1&seller_id=1&sid=fab33396ffa8e20&ssp2=1&stid=0&t=tpclicked3_hc&tu=u1922429&u=http%3A%2F%2Fwww%2Eadmin10000%2Ecom%2Fdocument%2F971%2Ehtml&urlid=0" target="_blank" mpid="2" style="text-decoration: none;"><span style="color:#0000ff;font-size:14px;width:auto;height:auto;float:none;">服务器</span></a></span>瞬时压力增大，分段读取
                    $buffer=1024;
                    while(!feof($fp)){
                        $file_data=fread($fp,$buffer);
                        echo $file_data;
                    }
                    //关闭文件
                    fclose($fp);
                }else{
                    $this->error('文件不存在！');
                }

            }
        }
    }

    //删除查新
    public function del($id){
        $map['kjcxid'] = $id;
        $map['userid'] = UID;
        $task = M('Task')->where($map)->count();
        if($task){
            $res = M('Task')->where($map)->delete();
            if($res){
                $this->success('删除成功');
            }else{
                $this->error('删除失败');
            }
        }else{
            $this->error('对不起，您无权删除非自己的业务');
        }
    }

    //科技查新添加任务表
    private function add_task($data){
        return D('Task')->update($data);
    }

    //科技查新添加内容表
    private function add_kjcx($data){
        return D('Kjcx')->update($data);
    }

    //获取用户信息
    private function getUserInfo(){
        $user = M('User')->find(UID);
        return  $user;
    }

    //判断是否根据IP分配
    private function auto(){
        $ip =  get_client_ip();
        $where['ip'] = array('like',"%$ip%");
        $id = M('Department')->where($where)->getField('id');
        if($id){
            return $id;
        }else{
            return false;
        }
    }

    function getCate($cate='',$city=''){
        $model = M('cxjg');
        if($cate){
            $map['cate'] = $cate;
            $data = $model->where($map)->select();
            $return_content['state']    = true;
            $return_content['result']   = $data;
        }elseif($city){
            $map['province'] = $city;
            $data = $model->where($map)->select();
            $return_content['state']    = true;
            $return_content['result']   = $data;
        }else{
            $return_content['state']    = false;
            $return_content['result']   = '';
        }
        $this->ajaxReturn($return_content);
    }

	/**
     * 根据城市查询学校
     * @param $city
     */
    function getCity($city){
        $model = M('department');
        if($city){
            if($city=='all'){
                $map = ' 1';
            }else{
                $_SESSION['city']   = $city;
                $map['city']        = $city;
                if($_SESSION['categoryid']){
                    $categoryid = $_SESSION['categoryid'];
                    $map['categoryid']    = array('LIKE',"%$categoryid%");
                }
            }
            $data = $model->where($map)->order('convert (name using gb2312) asc')->select();
            $return_content['state']    = true;
            $return_content['result']   = $data;
        }else{
            $return_content['state']    = false;
            $return_content['result']   = '';
        }
        $_SESSION['categoryid'] = null;
        $this->ajaxReturn($return_content);
    }

	/**
     * 根据学科类别查询学校
     * @param string $cate
     */
    function getCateType($cate=''){
        $model = M('department');
        if($cate){
            if($cate == 'all'){
                $map['city']    = 320000;
            }else{
                $map['categoryid']        = array('LIKE',"%$cate%");
                $_SESSION['categoryid']   = $cate;
                $map['city']    = 320000;
//                if($_SESSION['city']){
////                    $map['city']    = $_SESSION['city'];
//                    $map['city']    = 320000;
//                }
            }
            $data = $model->where($map)->order('convert (name using gb2312) asc')->select();
            $return_content['state']    = true;
            $return_content['result']   = $data;
        }else{
            $return_content['state']    = false;
            $return_content['result']   = '';
        }
        $_SESSION['city'] = null;
        $this->ajaxReturn($return_content);
    }
    /**
     * 验证数组  2016.6.8 teng
     */
    protected function prove_array()
    {
        $array = array(
            array('title'=>'xmmc',          'value'=>' ',     'msg'=>'请填写项目中文名称'),
            array('title'=>'cxmd',          'value'=>' ',     'msg'=>'请选择查新目的'),
            array('title'=>'xmjb',          'value'=>' ',     'msg'=>'请选择项目级别'),
            array('title'=>'show_add_cxd',  'value'=>' ',     'msg'=>'请填写查新点'),
            array('title'=>'jsd',           'value'=>' ',     'msg'=>'请填写技术要求'),
            array('title'=>'cxyq',          'value'=>' ',     'msg'=>'请选择查新要求'),
            array('title'=>'lqrq',          'value'=>' ',     'msg'=>'请填写完成日期'),
            array('title'=>'keywords_cn',   'value'=>' ',     'msg'=>'请填写中文检索词'),
            array('title'=>'wtr_name',      'value'=>' ',     'msg'=>'请输入委托人'),
        );
        return $array;
    }

    /**
     * 验证数组  2016.6.17 teng
     */
    private function showRegError($code = 0)
    {
        switch ($code) {
            case -1:  $error = '项目长度必须在1-255的字符以内！'; break;
            case -2:  $error = '项目名被占用！'; break;
            case -3:  $error = '委托人姓名必须在1-15的字符以内！'; break;
            case -4:  $error = '请输入正确的委托人号码！'; break;
            case -5:  $error = '联系人姓名必须在1-15的字符以内！'; break;
            case -6:  $error = '请输入正确的联系人号码！'; break;
            case -7:  $error = '委托单位必须在1-255的字符以内！'; break;
            case -8:  $error = '通讯地址必须在1-255的字符以内！'; break;
            case -9:  $error = '邮箱格式不正确！'; break;
            case -10: $error = '邮箱长度必须在1-32个字符之间！'; break;
            case -11: $error = '请选择查新目的！'; break;
            case -12: $error = '请选择项目级别！'; break;
            case -13: $error = '请填写查新点！'; break;
            case -14: $error = '请填写技术要求'; break;
            case -15: $error = '请选择查新要求'; break;
            case -16: $error = '请输入正确完成日期'; break;
            case -17: $error = '请填写中文检索词'; break;
            case -18: $error = '请选择缴费方式'; break;
            case -19: $error = '请选择是否需要发票'; break;
            case -20: $error = '请选择报告领取'; break;
            case -21: $error = '添加任务失败'; break;
            case -22: $error = '更新任务失败'; break;
            case -23: $error = '请选择项目学科'; break;
            default:  $error = '未知错误';
        }
        return $error;
    }

}
