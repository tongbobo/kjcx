<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/6/26
 * Time: 14:40
 */
namespace Home\Controller;
use Think\Controller;
class LogController extends CommonController{
    public function index(){
        //获取列表数据
        $tag = I('tag');
        if(!empty($tag)){
            switch($tag){
                case 'add_task':
                    $map['action_id'] = $this->getIdByName('add_task');
                    break;
                case 'modify_task':
                    $map['action_id'] = $this->getIdByName('modify_task');
                    break;
                case 'revoke_task':
                    $map['action_id']  = $this->getIdByName('revoke_task');
                    break;
            }
        }
        $map['user_id']    =   UID;
        $Log = M('ActionLog');
        $list   =   $this->lists($Log, $map);
        $this->assign('search', $tag);
        $this->assign('_list', $list);
        $this->meta_title = '行为日志';
        $this->display();
    }

    public function action(){
        //获取列表数据
        $tag = I('tag');
        if(!empty($tag)){
            switch($tag){
                case 'user_login':
                    $map['action_id'] = $this->getIdByName('user_login');
                    break;
                case 'user_logout':
                    $map['action_id'] = $this->getIdByName('user_logout');
                    break;
                case 'user_changePassword':
                    $map['action_id'] = $this->getIdByName('user_changePassword');
                    break;
                case 'modify_information':
                    $map['action_id'] = $this->getIdByName('modify_information');
                    break;
            }
        }
        $ros='10';
        $map['user_id']    =   UID;
        $Log = M('ActionLog');
        $list   =   $this->lists($Log, $map,'','',$ros);
         $this->assign('search', $tag);
        $this->assign('_list', $list);
        $this->meta_title = '行为日志';
        if(isset($_GET['p'])){
            $num = ($_GET['p']-1)*10;
            $this->assign('num', $num);
        }
        $this->display();
    }

    private function getIdByName($tag){
        $map['name'] = array('like',"%$tag%" );
        return M('Action')->where($map)->getField('id');
    }
}