<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-15
 * Time: 下午6:22
 */
namespace Home\Controller;
use Think\Controller;
use Common\Pagination;
class DeskController extends Controller {

    //调用模块名称
    public function _initialize()
    {
        $templete_arr = M('Templete')->select();
        $this->assign('templete_arr',$templete_arr);
        /* 读取数据库中的配置 */
        $config =   S('DB_CONFIG_DATA');
        if(!$config){
            $config =   api('Config/lists');
            S('DB_CONFIG_DATA',$config);
        }
        $min = (($_GET['p']-1)>0?$_GET['p']-1:0)*10;
        $this->assign('min',$min);
        $this->assign('config',$config);
    }
    /***
    *首页  2016.6.7 teng
     */
    public function index ()
    {
        //问题显示
        $arr['news']        = array_slice(S('News_catch'),0,7);
        $arr['friend']      = array_slice(S('Jiang_catch'),0,15);
        $constact           = M('Contact')->find(1);
		//echo json_encode($arr['friend']);die;
        $this -> assign('arr',$arr);
        $this -> assign('constact',$constact);
        $this -> assign('mean_title','首页');
        $this -> assign('kjcx_title','首页');//标题
        $this -> display();
    }

    /***
     *更多新闻信息   2016.6.7 teng
     */
    public function news_more()
    {
        $list       = S('News_catch');
        $new_list   = $this->deal_catch($list);
        $this->assign('arr',$new_list);
        $this->assign('mean_title','查新动态');
        $this -> display();
    }

    /***
     *新闻详细页面  2016.6.7 teng
     */
    public function news_info ()
    {
        $id     = intval(I('id'));
        $arr    = S('News_catch');
        if($arr[$id])
        {
            $this->assign('arr',$arr[$id]);
        }
        else
        {
            $this->error('数据不存在');
        }
        $this->assign('mean_title','新闻详细页');
        $this->display();
    }

    /***
     *更多问题信息   2016.6.7 teng
     */
    public function problem_more()
    {
        $list       = S('Problem_catch');
        $new_list   = $this->deal_catch($list);
        $this -> assign('arr',$new_list);
        $this->assign('mean_title','常见问题');
        $this -> display();
    }

    /***
     *新闻详细页面  2016.6.7 teng
     */
    public function problem_info ()
    {
        $id     = intval(I('id'));
        $arr    = S('Problem_catch');
        if($arr[$id])
        {
            $this->assign('arr',$arr[$id]);
        }
        else
        {
            $this->error('数据不存在');
        }
        $this->assign('mean_title','问题详细页');
        $this->display();
    }

    /***
     *查新简介
     */
    public function introduce () {
        $info = M('Templete')->find(1);
        $this->assign('info',$info);
        $this->assign('mean_title',$info['name']);
        $this -> assign('kjcx_title','系统介绍');//标题
        $this->display();
    }

    /***
     *查新对口单位
     */
    public function unit () {
        //读取所有机构名称
        $list = S('Jiang_catch');
        $this->assign('list',$list);
        $this->assign('mean_title','查新对口单位');
        $this -> assign('kjcx_title','查新对口单位');//标题
        $this->display();
    }

    /***
     *查新专家 列表
     */
    public function expert () {
        $list       = S('Expert_catch');

        $new_list   = $this->deal_catch($list);
        $this -> assign('info',$new_list);
        $this->assign('mean_title','查新专家');
        $this -> assign('kjcx_title','查新专家');//标题
        $this -> display();
    }

    /***
     *新闻动态 列表
     */
    public function casee () {
        $list       = S('News_catch');
        $new_list   = $this->deal_catch($list);
        $this -> assign('arr',$new_list);
        $this->assign('mean_title','新闻动态');
        $this -> assign('kjcx_title','新闻动态');//标题
        $this -> display();
    }

    /***
     *政策法规 列表
     */
    public function news () {
        $list       = S('Trends_catch');
        $new_list   = $this->deal_catch($list);
        $this -> assign('arr',$new_list);
        $this->assign('mean_title','政策法规');
        $this -> assign('kjcx_title','政策法规');//标题
        $this -> display();
    }

    /***
     *常见问题 列表
     */
    public function law () {
        $list       = S('Problem_catch');
        $new_list   = $this->deal_catch($list);
        $this -> assign('arr',$new_list);
        $this->assign('mean_title','常见问题');
        $this -> assign('kjcx_title','常见问题');//标题
        $this -> display();
    }

    /***
     *查新培训 列表
     */
    public function training () {
        $list       = S('Training_catch');
        $new_list   = $this->deal_catch($list);
        $this -> assign('arr',$new_list);
        $this->assign('mean_title','查新培训');
        $this -> assign('kjcx_title','查新培训');//标题
        $this -> display();
    }
    /***
     *优秀案列 列表
     */
    public function cases () {
        $list       = S('Case_catch');
        $new_list   = $this->deal_catch($list);
        $this -> assign('arr',$new_list);
        $this->assign('mean_title','优秀案列');
        $this -> assign('kjcx_title','优秀案列');//标题
        $this -> display();
    }
    //常见问题详情
    public function pro_detail()
    {
        $id     = intval(I('id'));
        $arr    = S('Problem_catch');
        if($arr[$id])
        {
            $this->assign('arr',$arr[$id]);
        }
        else
        {
            $this->error('数据不存在');
        }
        $this->assign('mean_title','常见问题');
        $this -> assign('kjcx_title','常见问题');//标题
        $this -> display('detail');
    }
    //政策法规详情
    public function news_detail()
    {
        $id     = intval(I('id'));
        $arr    = S('Trends_catch');
        if($arr[$id])
        {
            $this->assign('arr',$arr[$id]);
        }
        else
        {
            $this->error('数据不存在');
        }
        $this->assign('mean_title','政策法规');
        $this -> assign('kjcx_title','政策法规');//标题
        $this -> display('detail');
    }
    //新闻动态详情
    public function casee_detail()
    {
        $id     = intval(I('id'));
        $arr    = S('News_catch');
        if($arr[$id])
        {
            $this->assign('arr',$arr[$id]);
        }
        else
        {
            $this->error('数据不存在');
        }
        $this->assign('mean_title','新闻动态');
        $this -> assign('kjcx_title','新闻动态');//标题
        $this -> display('detail');
    }
    //查新培训详情
    public function training_detail()
    {
        $id     = intval(I('id'));
        $arr    = S('Training_catch');
        if($arr[$id])
        {
            $this->assign('arr',$arr[$id]);
        }
        else
        {
            $this->error('数据不存在');
        }
        $this->assign('mean_title','查新培训');
        $this -> assign('kjcx_title','查新培训');//标题
        $this -> display('detail');
    }
    //优秀案列详情
    public function cases_detail()
    {
        $id     = intval(I('id'));
        $arr    = S('Case_catch');
        if($arr[$id])
        {
            $this->assign('arr',$arr[$id]);
        }
        else
        {
            $this->error('数据不存在');
        }
        $this->assign('mean_title','优秀案列');
        $this -> assign('kjcx_title','优秀案列');//标题
        $this -> display('detail');
    }
    /**
     * @param $list  数组，总数
     * 2016.7.5 teng
     * 处理缓存数组的分页
     */
    protected function deal_catch($list){
        if(I('p'))
        {
            $p = intval(I('p'));
            $start = ($p-1)*10>0?($p-1)*10:0;
            $end = 10;
        }
        else
        {
            $start  = 0;
            $end 	= 10;
        }
        $total = count($list);
        // 分页
        $pageList = new Pagination($total,10);
        $page = $pageList->fpage(array(0,1,2,3,4,5,6,7,8));
        $this->assign('_page',$page);
        $this->assign('total',$total);
        $this->assign('pages',10);
        $new_list = array_slice($list,$start,$end);
        return $new_list;
    }
}
