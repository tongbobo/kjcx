<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-16
 * Time: 下午2:00
 */
namespace Home\Model;
use Think\Model;
class TaskModel extends Model {
    protected $_validate = array(
        array('content','require','内容是必须填写的')
    );
    protected $_auto = array(
        array('userid','session',self::MODEL_INSERT,'function', 'user_auth.uid'),
        array('username','session',self::MODEL_INSERT,'function','user_auth.username'),
        array('time',NOW_TIME,self::MODEL_INSERT),
    );
    public function update($data){
        $this->create($data);
        if(empty($data)){
            return false;
        }
        if(empty($data['id'])){
            $id = $this->add();
            if(!$id){
                $this->error = '添加任务失败';
                return false;
            }
        }else{
            $status = $this->save();
            if(false === $status){
                $this->error = '更新任务失败';
                return false;
            }
        }
        return $data;
    }

    /*
     * 统计符合要求的数目
     *$map 条件
     */
    public function getCount($map){
        $count = $this->where($map)->count();
        return $count;
    }
    /*
     * 查询符合条件的记录
     */
    public function getAll($map){
        $res = $this->where($map)->select();
        return $res;
    }

    /*
     * 接收任务
     */
    public function push($data){
        $res = $this->data($data)->save();
        return $res;
    }

    public function complete($data){
        $res = $this->data($data)->save();
        return $res;
    }

    public function finish($data){
        $res = $this->data($data)->save();
        return $res;
    }

    /*
     * 撤销任务
     */
    public function untread($data){
        $res = $this->data($data)->save();
        return $res;
    }
}