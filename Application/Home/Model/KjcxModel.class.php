<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/7/10
 * Time: 14:19
 */

namespace Home\Model;
use Think\Model;

class KjcxModel extends Model{

    /* 用户模型自动验证 */
    protected $_validate = array(
        array('xmmc', '1,255', -1, self::EXISTS_VALIDATE, 'length'), //项目长度1-255的字符
        //array('xmmc', '', -2, self::EXISTS_VALIDATE, 'unique'),     //项目名被占用
        //array('xkfl', 'require', -23), //请选择项目学科
        array('wtrname', '1,15', -3, self::EXISTS_VALIDATE, 'length'),  //委托人姓名2-6
        //array('wtrphone','/^[0-9]+/',-4,self::EXISTS_VALIDATE),//委托人号码不合法
        array('lxrname', '1,15', -5, self::EXISTS_VALIDATE, 'length'),  //联系人姓名2-6
        //array('phone','/^[0-9]+/',-6,self::EXISTS_VALIDATE),//联系人号码不合法
        array('wtdw', '1,255', -7, self::EXISTS_VALIDATE, 'length'), //委托单位1-255的字符
        array('txdz', '1,255', -8, self::EXISTS_VALIDATE, 'length'), //通讯地址1-255的字符
        array('wtremail', 'email', -9, self::EXISTS_VALIDATE), //邮箱格式不正确
        array('wtremail', '1,32', -10, self::EXISTS_VALIDATE, 'length'), //邮箱长度不合法
        array('cxmd', 'require', -11), //请选择查新目的
        array('xmjb', 'require', -12), //请选择项目级别
        array('show_add_cxd', 'require', -13), //请填写查新点
        array('jsd', 'require', -14), //请填写技术要求
        array('cxyq', 'require', -15), //请选择查新要求
        array('lqrq', '/([\d]+)-([\d]+)-([\d]+)/eis', -16, self::EXISTS_VALIDATE), //请输入正确完成日期
        array('keywords_cn', 'require', -17), //请填写中文检索词
        array('fkfs', 'require', -18), //请选择缴费方式
        array('dwsh', 'require', -19), //请选择是否需要发票
        array('qbfs', 'require', -20), //请选择报告领取

    );

    //添加任务
    public function TaskAdd($data){
        if($this->create($data)){
            $uid = $this->add();
            return $uid?$uid:0;
        }else{
            return $this->getError();
        }
    }

    //修改任务
    public function userUpdate($data){
        $data = $this->create($data);
        if($data){
            $res = $this->data($data)->save();
            if($res){
                return  true;
            }else{
                return false;
            }
        }else{
            return $this->getError();
        }
    }


    public function update($data){
        if($this->create($data)){
            if(empty($data['id'])){
                $id = $this->add();
                if(!$id){
                    return -21;
                }else{
                    return $id;
                }
            }else{
                $status = $this->save();
                if(false === $status){
                    return -22;
                }else{
                    return true;
                }
            }
        }else{
            return $this->getError();
        }
    }
    //指派任务
    public function call($id,$departmentId){
        $data = array(
            'id'=>$id,
            'process_id'=>0,
            'department_id'=>$departmentId
        );
        return  $this->save($data);
    }
}