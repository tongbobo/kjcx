<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-16
 * Time: 上午9:18
 */
namespace Home\Model;
use Think\Model;
class UserInfoModel extends Model {

    /* 用户模型自动验证 */
    protected $_validate = array(
        array('wtrname', '1,15', -3, self::EXISTS_VALIDATE, 'length'),  //委托人姓名2-6
        array('wtrphone','/^[0-9]+/',-4,self::EXISTS_VALIDATE),//委托人号码不合法
        array('lxrname', '1,15', -5, self::EXISTS_VALIDATE, 'length'),  //联系人姓名2-6
        array('phone','/^[0-9]+/',-6,self::EXISTS_VALIDATE),//联系人号码不合法
        array('wtremail', 'email', -9, self::EXISTS_VALIDATE), //邮箱格式不正确
        array('wtremail', '1,32', -10, self::EXISTS_VALIDATE, 'length'), //邮箱长度不合法
        array('wtdw', '1,255', -7, self::EXISTS_VALIDATE, 'length'), //委托单位1-255的字符
        array('txdz', '1,255', -8, self::EXISTS_VALIDATE, 'length'), //通讯地址1-255的字符
    );

    //添加人员
    public function UserAdd($data){
        if($this->create($data)){
            $uid = $this->add();
            return $uid?$uid:0;
        }else{
            return $this->getError();
        }
    }

    //修改人员
    public function userUpdate($data){
        $data = $this->create($data);
        if($data){
            $res = $this->data($data)->save();
            if($res){
                return  true;
            }else{
                return false;
            }
        }else{
            return $this->getError();
        }
    }
}