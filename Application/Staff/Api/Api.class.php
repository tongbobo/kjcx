<?php
/**
 * Created by PhpStorm.
 * User: sl
 * Date: 16.04.12
 * Time: 下午5:19
 */
namespace Staff\Api;
define('UC_CLIENT_PATH', dirname(dirname(__FILE__)));

//载入配置文件
require_cache(UC_CLIENT_PATH . '/Conf/config.php');

//载入函数库文件
require_cache(UC_CLIENT_PATH . '/Common/common.php');

abstract class Api{
    protected $model;

    public function __construct(){
        $this->_init();
    }

    abstract protected function _init();
}