<?php
/**
 * Created by PhpStorm.
 * User: sl
 * Date: 16.04.12
 * Time: 涓嬪崍5:19
 */
namespace Staff\Api;
use Staff\Api\Api;
use Staff\Model\StaffModel;

class StaffApi extends Api{
    protected function _init(){
            $this->model = new StaffModel();
    }
    /*
     *妫�娴嬬敤鎴锋槸鍚︾鍚堟敞鍐屾潯浠�
     */
    public function user_staff($username){
        return $this->model->user_staff($username);
    }

}