<?php
/**
 * Created by PhpStorm.
 * User: sl
 * Date: 16.04.12
 * Time: 下午5:19
 */
namespace Staff\Model;
use Think\Model;
class StaffModel extends Model{
    protected $tablePrefix = 'k_';
    protected $connection = UC_DB_DSN;
    /*
     * 检测用户是否符合注册条件
     */
    public function user_staff($username){
        $map['register_name'] = $username;
        $check = $this->where($map)->find();
        if(!$check){
            return -10;
        }else{
            return 10;
        }
    }
}