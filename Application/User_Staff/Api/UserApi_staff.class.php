<?php
/**
 * Created by PhpStorm.
 * User: sl
 * Date: 16.04.12
 * Time: 下午5:19
 */
namespace User_Staff\Api;
use User_Staff\Api\Api;
use User_Staff\Model\UserModel;

class UserApi_staff extends Api{
    protected function _init(){
        $this->model = new UserModel();
    }
    /*
     *检测用户是否符合注册条件
     */
    public function user_staff($username){
        return $this->model->user_staff($username);
    }

}