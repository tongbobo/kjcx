<?php
/**
 * @desc DocController.class.php
 * @author    Adrian Tian <atoat@qq.com>
 * @datetime  2018/6/25 16:05
 * @version   1.0
 * @copyright 2015-2018 atoat all rights reserved
 * @license   http://www.apache.org/licenses/LICENSE-2.0
 */

namespace Api\Controller;
use Think\Controller;

class DocController extends Controller
{
    private $return;
    private $apiUrl;

    public function __construct()
    {
        parent::__construct();
        $this->return = [
            'errno'  => '错误标识，0表示正确 非0表示有错误发生，查看所有<a href="'.U('doc/errlist').'"> 错误代码</a>',
            'errmsg' => '具体的错误提示',
            'data'   => '返回的数据'];
        $this->apiUrl = 'http://kjcx.jalis.nju.edu.cn/index.php';

        $this->assign('apiUrl',$this->apiUrl);
        $action = array_pop(explode('/',__ACTION__));
        $this->assign('action',$action);
    }

    public function index()
    {
        $this->display('doclist');
    }

    public function task()
    {
        $api = ['title'=>'我的委托','name'=>'api/task/list','desc'=>'我的委托'];

        $method = 'GET';
        $auth   = '是';
        $params = [
            ['param'=>'uid','must'=>'true','type'=>'integer','desc'=>'查新平台登录账号','default'=>''],
            ['param'=>'page','must'=>'false','type'=>'integer','desc'=>'当前页码','default'=>'1'],
            ['param'=>'nums','must'=>'false','type'=>'integer','desc'=>'每页记录数','default'=>'10']
        ];

        $example = '//我的委托<br />';
        $example .= $this->apiUrl.'/Api/Task/lists/page/1/nums/2';

        $this->return['data'] = '返回的数据信息，里面包含：<br />';
        $this->return['data'] .= '1. list 委托数据，["id"=>"记录ID","title"=>"项目名称","number"=>"查新号","process_org"=>"处理机构","process_status"=>"处理状态","report_status"=>"报告状态 -1:暂无状态 -2:可取报告 其它:剩余天数","cost"=>"费用","entrust_date"=>"委托日期","report_files"=>[查新报告文件地址(数组保存)],"edit_url"=>"编辑地址(可编辑时)","revocation_url"=>"撤销操作地址(可撤销时)","reapply_url"=>"重新申请操作地址(可重新申请时)","track_url"=>"追踪操作地址","confrim_url"=>"确认操作地址(可确认时)"]<br />';
        $this->return['data'] .= '2. total 记录总数<br />';

        $this->assign('api',$api);
        $this->assign('method',$method);
        $this->assign('auth',$auth);
        $this->assign('example',$example);
        $this->assign('return',$this->return);
        $this->assign('params',$params);
        $this->display('task');
    }

    public function errlist()
    {
        $errcode = [
            ['code'=>0,'desc'=>'接口调用成功','remark'=>'接口调用成功并返回相应的信息'],
            ['code'=>1000,'desc'=>'未访问具体接口','remark'=>'请访问具体的接口地址'],
            ['code'=>1001,'desc'=>'控制器不存在','remark'=>'访问了不存在的控制器'],
            ['code'=>1002,'desc'=>'方法不存在','remark'=>'访问了不存在的方法'],
            ['code'=>1003,'desc'=>'接口调用方式错误','remark'=>'需要post方式提交数据'],
            ['code'=>2001,'desc'=>'未登录状态','remark'=>'需要验证用户身份'],
        ];
        $this->assign('errcode',$errcode);

        $this->display('errlist');
    }

}
