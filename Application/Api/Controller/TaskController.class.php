<?php
/**
 * Created by PhpStorm.
 * User: Adrian Tian
 * Date: 18-6-25
 * Time: 下午14:05
 */

namespace Api\Controller;
use Think\Controller;

class TaskController extends Controller
{
    private $response = ['errno'=>-1,'errmsg'=>'未知错误'];

    public function index()
    {
        $this->lists();
    }

    //我的委托列表
    public function lists()
    {
        $status = I('status',0,'intval');
        $username = I('uid','');
        $search = I('get.');
        $page = I('page',1,'intval');
        $nums = I('nums',10,'intval');

        if($page<1){
            $page = 1;
        }

        if($nums<1){
            $nums = 10;
        }

        switch($status){
            //委托中
            case '1':
                $where['process_id'] = -2;
                break;
            //处理中
            case '2':
                $where['process_id'] = array(array('gt',-1), array('neq', 14));
                break;
            //已经完成
            case '3':
                $where['process_id'] = 14;
                break;
            //任务驳回
            case '4':
                $where['process_id'] = -1;
                break;
        }

        //用户登录信息
        /*$user = session('k_home_.user_auth');
        if(empty($user)){
            $this->response['errno'] = 20001;
            $this->response['errmsg'] = '未登录状态';
            echo $this->returnVal();
            exit();
        }*/

        //$where['user_id'] = $user['id'];
        //查询登录名
        if(empty($username)){
            $uid = -1;
        }else{
            $uid = M('User')->where(['username'=>$username])->getField('id');
            if(empty($uid)){
                $uid = -1;
            }
        }

        $where['user_id'] = $uid;

        if(!empty($search['time'])){
            $mintime = trim($search['time']);
            $where['intime'] = array('LIKE',"%$mintime%");
        }

        if(!empty($search['xmmc'])){
            $xmmc = $search['xmmc'];
            $where['xmmc'] = array('LIKE',"%$xmmc%");
        }

        if(!empty($search['cxh'])){
            $cxh =trim($search['cxh']);
            $where['cxh'] = array('LIKE',"%$cxh%");
        }

        $offset = ($page - 1)*$nums;
        $list = M('Kjcx')->field('id,xmmc,cxh,department_id,process_id,zfy,intime,cx_file_ids,user_id')->where($where)->limit($offset,$nums)->select();
        $total = M('Kjcx')->where($where)->count();
        $list = $this->processData($list);
        $this->response['errno'] = 0;
        $this->response['errmsg'] = '操作成功';
        $this->response['data'] = ['list'=>$list,'total'=>$total];
        echo json_encode($this->response,320);
    }

    //数据处理
    private function processData($list=[])
    {
        $timestamp = strtotime(date('Y-m-d',time()));
        $data = [];
        $i = 0;
        $department = [];
        $reportFiles = [];
        $url = 'http://kjcx.jalis.nju.edu.cn';

        foreach($list as $k=>$v){
            //报告状态
            if($v['process_id']==-2){
                $reportStatus = -1;
            }elseif($v['process_id']==13||$v['process_id']==16||$v['process_id']==11||$v['process_id']==0||$v['process_id']==17||$v['process_id']==20){//剩余天数
                $reportStatus = 0;

                if(!empty($v['lqrq'])){
                    $d = strtotime($v['lqrq']) - $timestamp;

                    if($d>0){
                        $reportStatus = date('d',$d);
                    }elseif(0==$d){
                        $reportStatus = 1;
                    }
                }
            }elseif($v['process_id']==14){//可取报告
                $reportStatus = -2;
            }

            if(isset($department[$v['department_id']])){
                $departName = $department[$v['department_id']];
            }else{
                $departName = getAgencynameById($v['department_id']);
                $department[$v['department_id']] = $departName;
            }

            $processStatus = getTitleByProcessId($v['process_id'],$v['department_id']);

            if(!empty($v['cx_file_ids'])){
                $fileIds = explode(',',$v['cx_file_ids']);

                $j = 0;
                foreach($fileIds as $file){
                    $reportFiles[$j++] = $url.U('Task/down_file',array('file_id'=>$file));
                }
            }

            $editUrl = '';
            $revocationUrl = '';
            $applyUrl = '';
            $confirmUrl = '';

            if($v['process_id']==-2||$v['process_id']==20||$v['process_id']==17||$v['process_id']==-1||$v['process_id']==15||$v['process_id']==17){
                $editUrl = $url.U('Home/Task/edit',array('id'=>$v['id'],'uid'=>idEncode($v['user_id'])));
            }

            if($v['process_id']==-2||$v['process_id']==20){
                $editUrl = $url.U('Home/Task/stop_task',array('id'=>$v['id'],'uid'=>idEncode($v['user_id'])));
            }

            if($v['process_id']==-1){
                $applyUrl = $url.U('Home/Task/tj_task_again',array('id'=>$v['id'],'uid'=>idEncode($v['user_id'])));
            }

            if($v['process_id']==15){
                $applyUrl = $url.U('Home/Task/tj_task_again2',array('id'=>$v['id'],'uid'=>idEncode($v['user_id'])));
            }

            $trackUrl = $url.U('Home/Task/p_track',array('id'=>$v['id'],'uid'=>idEncode($v['user_id'])));

            if($v['process_id']==40){
                $confirmUrl = '';
            }

            $data[$i++] = ['id'=>$v['id'],'number'=>$v['cxh'],'title'=>$v['xmmc'],'process_org'=>$departName,'process_status'=>$processStatus,'report_status'=>$reportStatus,'cost'=>$v['zfy'],'entrust_date'=>$v['intime'],'report_files'=>$reportFiles,'edit_url'=>$editUrl,'revocation_url'=>$revocationUrl,'reapply_url'=>$applyUrl,'track_url'=>$trackUrl,'confrim_url'=>$confirmUrl];
        }

        return $data;
    }

    //返回值处理
    private function returnVal()
    {
        return json_encode($this->response,320);
    }
}
