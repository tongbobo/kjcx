<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/7/1
 * Time: 11:37
 */
namespace Api\Department;
use Api\Api;
use Api\Department\Model\DepartmentModel;
class DepartmentApi extends Api{
    protected function _init(){
        $this->model = new DepartmentModel();
    }
    public function register($data){
        return $this->model->register($data);
    }

    public function audit($id){
        return $this->model->audit($id);
    }

    public function getInfo($id){
        return $this->model->getInfo($id);
    }

    public function update($data){
        return $this->model->update($data);
    }
    //可以根据$id 和$name来获取信息
    public function info($id){
        return $this->model->info($id);
    }
}
