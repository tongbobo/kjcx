<?php
namespace Word\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index($cx_message,$jigou){
        vendor('PHPWord.PHPWord');
        $PHPWord = new \PHPWord();
        $section = $PHPWord->createSection();
        /******封面区域******/
        //添加段落样式(水平居中)
        $PHPWord->addParagraphStyle('pStyle', array('align'=>'center'));
        $PHPWord->addParagraphStyle('rStyle', array('align'=>'right'));
        $fm_fontstyle	= array('color'=>'000000','bold'=>true,'size'=>34);
        $fm_fontstyle2	= array('color'=>'000000','size'=>15);
        $fm_fontstyle3	= array('color'=>'000000','size'=>22);
        $fm_fontstyle12	= array('color'=>'000000','size'=>11);
        $section->addText("报告编号：".$cx_message['cxh'],array('size'=>10));
        $section->addTextBreak(5);
        $section->addText('科 技 查 新 委 托',$fm_fontstyle,'pStyle');
        $section->addTextBreak(6);
        $section->addText('         项目名称：'.$cx_message['xmmc'],$fm_fontstyle2);
        $section->addTextBreak(3);
        $section->addText('         委托单位：'.$cx_message['wtdw'],$fm_fontstyle2);
        $section->addTextBreak(3);
        $section->addText('         委托日期：'.$cx_message['intime'],$fm_fontstyle2);
        $section->addTextBreak(3);
        $section->addText('         查新机构：教育部科技查新工作站（G02）',$fm_fontstyle2);
        $section->addTextBreak(3);
        $section->addText('         完成日期：'.date('Y年 m月 d日'),$fm_fontstyle2);
        $section->addTextBreak(9);

        $section->addText('教育部科技发展中心',$fm_fontstyle3,'pStyle');
        $section->addTextBreak(2);
        $section->addText('二〇一三年制',$fm_fontstyle3,'pStyle');
        $section->addPageBreak();


        /***********************正式内容开始**************************/
        //添加页脚
        $foot_fontstyle	= array('color'=>'000000','size'=>8);
        $title_fontstyle	= array('color'=>'000000','bold'=>true,'size'=>16);
        $footer = $section->createFooter();
//        $footer->addPreserveText('{PAGE}','','pStyle');
        $table	= $footer->addTable();
//
        //放置页码并居中
        $table->addRow();
        //$table->addCell(3500)->addText('教育部查新单位南京农业大学查新站',$foot_fontstyle);
        $obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
        for($i=0;$i<7;$i++)
        {
            $table->addCell(9900, array('cellMerge' => 'continue'));
        }

//        $obj->addText('咨询电话:0791-3969265                                填写后发送到电子信箱:ncucxzx@ncu.edu.cn',$foot_fontstyle,"pStyle");

        $styleCell = array('valign'=>'center');
        $styleTable = array('borderSize'=>6, 'borderColor'=>'000000', 'cellMargin'=>80,'alignMent'=>'center');

        $PHPWord->addTableStyle('myOwnTableStyle', $styleTable);
        $fontStyle	= array('color'=>'000000','bold'=>true,'size'=>13);
        $fontStyle2	= array('align'=>'center','size'=>11);
        $fontStyle21	= array('size'=>13);
//教育部科技查新工作站（L25）
//        $section->addText('查新委托书',$title_fontstyle,"pStyle");
//        $obj->addTextBreak(2);
//        $section->addText('    委托时间  '.$cx_message['intime'].'                                              编号：'.$cx_message['cxh'],$fm_fontstyle12,"pStyle");
        $table = $section->addTable("myOwnTableStyle");
        $table->addRow(400);
        $obj	= $table->addCell(1700,array('rowMerge' => 'restart','cellMerge' => 'restart','valign'=>'center'));
        $table->addCell(1700, array('cellMerge' => 'continue'));
        $obj->addText('查新项目名称',$fm_fontstyle12,"pStyle");
        $table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "left"))->addText('中文：'.$cx_message['xmmc'],$fm_fontstyle12);
        for($i=0;$i<5;$i++)
        {
            $table->addCell(8200, array('cellMerge' => 'continue'));
        }
        $table->addRow(400);
        $table->addCell(1700, array('rowMerge' => 'fusion','cellMerge' => 'restart'));
        $table->addCell(1700, array('cellMerge' => 'continue'));
        $table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "left"))->addText('英文：'.$cx_message['xmmcen'],$fm_fontstyle12);
        for($i=0;$i<5;$i++)
        {
            $table->addCell(8200, array('cellMerge' => 'continue'));
        }

        $table->addRow(400);
        $obj	= $table->addCell(1700,array('rowMerge' => 'restart','cellMerge' => 'restart','valign'=>'center'));
        $table->addCell(1700, array('cellMerge' => 'continue'));
        $obj->addText('查新机构',$fm_fontstyle12,"pStyle");
        $table->addCell(2000)->addText('名    称',$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText($jigou['name'],$fm_fontstyle12);
        for($i=0;$i<4;$i++)
        {
            $table->addCell(8200, array('cellMerge' => 'continue'));
        }

        $table->addRow(400);
        $table->addCell(1700, array('rowMerge' => 'fusion','cellMerge' => 'restart'));
        $table->addCell(1700, array('cellMerge' => 'continue'));
        $table->addCell(2000)->addText('通讯地址',$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText($jigou['address'],$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'continue'));
//        $table->addCell(8200, array('cellMerge' => 'continue'));
        $table->addCell(2000)->addText('邮政编码：',$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText($jigou['code'],$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'continue'));

        $table->addRow(400);
        $table->addCell(1700, array('rowMerge' => 'fusion','cellMerge' => 'restart'));
        $table->addCell(1700, array('cellMerge' => 'continue'));
        $table->addCell(2000)->addText('负 责 人',$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText($jigou['fzr'],$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'continue'));
        $table->addCell(2000)->addText('电 话：',$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText($jigou['fzr_tell'],$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'continue'));

        $table->addRow(400);
        $table->addCell(1700, array('rowMerge' => 'fusion','cellMerge' => 'restart'));
        $table->addCell(1700, array('cellMerge' => 'continue'));
        $table->addCell(2000)->addText('联 系 人',$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText($jigou['lxr'],$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'continue'));
        $table->addCell(2000)->addText('电 话：',$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText($jigou['lxr_tell'],$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'continue'));

        $table->addRow(400);
        $table->addCell(1700, array('rowMerge' => 'fusion','cellMerge' => 'restart'));
        $table->addCell(1700, array('cellMerge' => 'continue'));
        $table->addCell(2000)->addText('电子邮箱',$fm_fontstyle12);
        $table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText($jigou['email'],$fm_fontstyle12);
        for($i=0;$i<4;$i++)
        {
            $table->addCell(8200, array('cellMerge' => 'continue'));
        }


//        查新目的
        $table->addRow(600);
        $obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
        $obj->addText('一、查新目的',$fontStyle);
        $obj->addTextBreak(1);
        $cxmd_str='';
        $cxmd_arr=explode(',',$cx_message['cxmd']);
        $zd_message = getCxmdCheckbox(); //得到所有的查新目的
        foreach($zd_message as $key => $value){
            if(in_array($key, $cxmd_arr)){
                $cxmd_str.= '■'.$value.'   ';
            }
        }
        $obj->addText($cxmd_str,$fm_fontstyle12);
        for($i=0;$i<7;$i++)
        {
            $table->addCell(9900, array('cellMerge' => 'continue'));
        }
        if($cx_message['xxcxmd'] != '' && in_array(1,$cxmd_arr)){
            $obj->addTextBreak(1);
            $obj->addText('详细查新目的：'.$cx_message['xxcxmd'],$fm_fontstyle12);
        }
        if($cx_message['cxmd_other'] != ''){
            $obj->addTextBreak(1);
            $obj->addText('其他查新目的：'.$cx_message['cxmd_other'],$fm_fontstyle12);
        }

        //查新点和技术要求
        $table->addRow(600);
        $obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
        $obj->addText('二、项目的科学技术要点',$fontStyle);
        $obj->addTextBreak(1);
        $obj->addText($cx_message['jsd'],$fm_fontstyle12);
        for($i=0;$i<7;$i++)
        {
            $table->addCell(9900, array('cellMerge' => 'continue'));
        }
        $obj->addTextBreak(1);


        //查新点
        $table->addRow(600);
        $obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
        $obj->addText('三、查新点',$fontStyle);
        $obj->addTextBreak(1);
        $obj->addText($cx_message['show_add_cxd'],$fm_fontstyle12);

        for($i=0;$i<7;$i++)
        {
            $table->addCell(9900, array('cellMerge' => 'continue'));
        }
        $obj->addTextBreak(1);


        //四、查新范围要求
        $table->addRow(600);
        $obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
        $obj->addText('四、查新范围要求',$fontStyle);
        $cxyq = getCxyqCheckbox();
        $cxyq1 = $cxyq[$cx_message['cxyq']];
        if($cx_message['qtyq']!=''){
            $cxyq1 .= ';'.$cx_message['qtyq'];
        }
        $obj->addText($cxyq1,$fm_fontstyle12);
        for($i=0;$i<7;$i++)
        {
            $table->addCell(9900, array('cellMerge' => 'continue'));
        }

        //五、文献检索范围及检索策略
        $table->addRow(600);
        $obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
        $obj->addText('五、文献检索范围及检索策略',$fontStyle);
        $obj->addText('  国内数据库：',$fontStyle);
        $obj->addText('     1. 中国知网中国期刊全文数据库                    1994—2016.9',$fontStyle2);
        $obj->addText('     2. 重庆维普中文科技期刊数据库                    1989—2016.9',$fontStyle2);
        $obj->addText('     3. 万方数字化期刊全文数据库                      1983—2016.9',$fontStyle2);
        $obj->addText('     4. 重庆维普中国科技经济新闻数据库                1992—2016.9',$fontStyle2);
        $obj->addText('     5. 中国学术会议论文数据库（知网、万方）          1989—2016.9',$fontStyle2);
        $obj->addText('     6. 中国学位论文数据库（知网、万方）              1982—2016.9',$fontStyle2);
        $obj->addText('     7. 中国科技成果数据库                            1989—2016.9',$fontStyle2);
        $obj->addText('     8. 中国专利数据库                                1985—2016.9',$fontStyle2);
        $obj->addText('     9. 国家科技成果网（科学技术部）                  1978—2016.9',$fontStyle2);
        $obj->addText('     10. 中国科技论文在线（教育部科技发展中心）       2003—2016.9',$fontStyle2);
        $obj->addText('     11. 中国会议论文在线（教育部科技发展中心）       2003—2016.9',$fontStyle2);
        $obj->addText('     12. 中国科学文献服务系统                         1985—2016.9',$fontStyle2);
        $obj->addText('     www.baidu.com等网络资源搜索',$fontStyle2);
        for($i=0;$i<7;$i++)
        {
            $table->addCell(9900, array('cellMerge' => 'continue'));
        }
        //检索词
        $obj->addTextBreak(2);
        $obj->addText('检索词',$fontStyle);
        $obj->addText($cx_message['search_key'],$fontStyle2);
        $obj->addTextBreak(2);
        $obj->addText('检索式',$fontStyle);
        $obj->addText($cx_message['search_method'],$fontStyle2);

        $table->addRow(600);
        $obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
        $obj->addText('六、检索结果',$fontStyle);
        $obj->addTextBreak(1);
        $obj->addText('  '.$cx_message['search_result'],$fm_fontstyle12);
        for($i=0;$i<7;$i++)
        {
            $table->addCell(9900, array('cellMerge' => 'continue'));
        }

        $table->addRow(600);
        $obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
        $obj->addText('七、查新结论',$fontStyle);
        $obj->addTextBreak(1);
        $obj->addText('  '.$cx_message['cha_result'],$fm_fontstyle12);
        for($i=0;$i<7;$i++)
        {
            $table->addCell(9900, array('cellMerge' => 'continue'));
        }


        $obj->addTextBreak(5);
        $obj->addText('     查新员（签字）: '.$cx_message['chaxinyuan'].'            查新员职称：馆员',$fontStyle21);
        $obj->addText('     审核员（签字）: '.$cx_message['shenheyuan'].'          审核员职称：副研究馆员',$fontStyle21);

        $obj->addTextBreak(2);
        $obj->addText( '（科技查新专用章）',$fontStyle21,'rStyle');
        $obj->addText(date('Y年 m月 d日',$cx_message['chatime']),$fontStyle21,'rStyle');
        $obj->addTextBreak(5);

        //附件
        $table->addRow(600);
        $obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
        $obj->addText('八、查新员、审核员声明',$fontStyle);
        $obj->addTextBreak(1);
        $obj->addText('  1.查新报告中所陈述的内容均以客观文献为依据',$fontStyle2);
        $obj->addText('  2.我们按照科技查新技术规范进行查新、文献分析和审核，并做出上述查新结论；',$fontStyle2);
        $obj->addText('  3.我们获取的报酬与本报告中的分析、意见和结论无关，也与本报告的使用无关；',$fontStyle2);
        $obj->addText('  4.本报告仅用于鉴定、报奖。',$fontStyle2);
        for($i=0;$i<7;$i++)
        {
            $table->addCell(9900, array('cellMerge' => 'continue'));
        }

        $obj->addTextBreak(5);
        $obj->addText('查新员（签字）：'.$cx_message['chaxinyuan'].'              审核员（签字）：'.$cx_message['shenheyuan'],$fontStyle2,'pStyle');
        $obj->addTextBreak(2);
        $time1 = date('Y年 m月 d日',$cx_message['chatime']);
        if($cx_message['shentime']>0)
        {
            $time2 = date('Y年 m月 d日',$cx_message['shentime']);
        }
        else
        {
            $time2 = '';
        }
        $obj->addText('               '.$time1.'             '.$time2,$fontStyle2);

        $table->addRow(600);
        $obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
        $obj->addText('九、附件清单',$fontStyle);
        $obj->addTextBreak(1);
        $obj->addText(' 本报告无附件。',$fm_fontstyle12);
        for($i=0;$i<7;$i++)
        {
            $table->addCell(9900, array('cellMerge' => 'continue'));
        }
        $obj->addTextBreak(15);
        $obj->addText(' ',$fm_fontstyle12);

        $table->addRow(600);
        $obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
        $obj->addText('十、备注',$fontStyle);
        $obj->addTextBreak(1);
        $obj->addText(' 1.本查新报告无查新机构的“科技查新专用章”、骑缝章无效；',$fm_fontstyle12);
        $obj->addText(' 2.本查新报告无查新员和审核员签名无效；',$fm_fontstyle12);
        $obj->addText(' 3.本查新报告涂改无效；',$fm_fontstyle12);
        $obj->addText(' 4.本查新报告的检索结果及查新结论仅供参考。',$fm_fontstyle12);
        for($i=0;$i<7;$i++)
        {
            $table->addCell(9900, array('cellMerge' => 'continue'));
        }
        $obj->addTextBreak(15);

//        $fileName = "word报表".date("YmdHis");审核报告
        $outputFileName = time().'.docx';
        $outputFileName = iconv("utf-8","gb2312//IGNORE",$outputFileName);
        $filePath = "./uploads/report/".date("Ym");//要保存的文件夹路径
        if(!is_dir($filePath)){
            $filePath=makepath($filePath);
        }
        $upfile_file    = $filePath.'/'.$outputFileName;

        $objWriter = \PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
        $objWriter->save($upfile_file);
//        $objWriter = \PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
//        $objWriter->save('AdvancedTable.docx');
//        $fileName = "word报表".date("YmdHis");
//        header("Content-type: application/vnd.ms-word");
//        header("Content-Disposition:attachment;filename=".$fileName.".docx");
//        header('Cache-Control: max-age=0');
//        $objWriter = \PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
//        $objWriter->save('php://output');

        if(file_exists($upfile_file))
        {
            $data['title']  = $outputFileName;
            $data['dir']    = $filePath.'/';
            $data['size']    = filesize($upfile_file);
            $data['create_time']    = date('Y-m-d H:i:s');
            $data['type']    = 1;
            $s = M('Attachment')->add($data);
            return $s;
        }
    }

	function getBG($cx_message = array(),$path = '',$school_id = '',$zd_message=array())
	{
		vendor('PHPWord.PHPWord');
        $PHPWord = new \PHPWord();
        $section = $PHPWord->createSection();
		/******封面区域******/
		//添加段落样式(水平居中)
		$PHPWord->addParagraphStyle('pStyle', array('align'=>'center'));
		$fm_fontstyle	= array('color'=>'000000','bold'=>true,'size'=>34);
		$fm_fontstyle2	= array('color'=>'000000','bold'=>true,'size'=>22);
		$fm_fontstyle3	= array('color'=>'000000','size'=>22);
		$fm_fontstyle12	= array('color'=>'000000','size'=>11);

		//$section->addText("报告编号：".$cx_message['cxh']);
		//$section->addTextBreak(5);
		//$section->addText('科 技 查 新 合 同',$fm_fontstyle,'pStyle');
		//$section->addTextBreak(6);
		//$section->addText('    项目名称:'.$cx_message['xmmc'],$fm_fontstyle2);
		//$section->addTextBreak(3);
		//$section->addText('    委托单位:'.$cx_message['wtdw'],$fm_fontstyle2);
		//$section->addTextBreak(3);
		//$section->addText('    委托日期:'.$cx_message['intime'],$fm_fontstyle2);
		///$section->addTextBreak(3);
		//$section->addText('    查新机构(盖章):',$fm_fontstyle2);
		//$section->addTextBreak(3);
		//$section->addText('    查新完成日期:',$fm_fontstyle2);
		//$section->addTextBreak(9);

		//$section->addText('中 华 人 民 共 和 国 科 学 技 术 部',$fm_fontstyle3,'pStyle');
		//$section->addTextBreak(2);
		//$section->addText('二OOO年制',$fm_fontstyle3,'pStyle');
		//$section->addPageBreak();


		/***********************正式内容开始**************************/
		//添加页脚
		$foot_fontstyle	= array('color'=>'000000','size'=>8);
		$title_fontstyle	= array('color'=>'000000','bold'=>true,'size'=>16);
		$footer = $section->createFooter();
		//$footer->addPreserveText('{PAGE}','','pStyle');
		$table	= $footer->addTable();

		//放置页码并居中
		$table->addRow();
		//$table->addCell(3500)->addText('教育部查新单位南京农业大学查新站',$foot_fontstyle);
		$obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}

		$obj->addText('教育部科技查新工作站(G02)',$foot_fontstyle,"pStyle");
		
		$styleCell = array('valign'=>'center');
		$styleTable = array('borderSize'=>6, 'borderColor'=>'000000', 'cellMargin'=>80,'alignMent'=>'center');
	
		$PHPWord->addTableStyle('myOwnTableStyle', $styleTable);
		$fontStyle	= array('color'=>'000000','bold'=>true,'size'=>13);
		$fontStyle2	= array('align'=>'center');

		$section->addText('教育部科技查新工作站（G02）查新委托书',$title_fontstyle,"pStyle");
		$obj->addTextBreak(2);
		$section->addText('    委托时间  '.$cx_message['intime'].'                                              编号：'.$cx_message['cxh'],$fm_fontstyle12,"pStyle");
		$table = $section->addTable("myOwnTableStyle");
		$table->addRow(400);
		$obj	= $table->addCell(1700,array('rowMerge' => 'restart','cellMerge' => 'restart','valign'=>'center'));
		$table->addCell(1700, array('cellMerge' => 'continue'));
		$obj->addText('查新项目名称',$fm_fontstyle12,"pStyle");
		$table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "left"))->addText('中文：'.$cx_message['xmmc'],$fm_fontstyle12);
		for($i=0;$i<5;$i++)
		{
			$table->addCell(8200, array('cellMerge' => 'continue'));
		}
		$table->addRow(400);
		$table->addCell(1700, array('rowMerge' => 'fusion','cellMerge' => 'restart'));
		$table->addCell(1700, array('cellMerge' => 'continue'));
		$table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "left"))->addText('英文：'.$cx_message['xmmcen'],$fm_fontstyle12);
		for($i=0;$i<5;$i++)
		{
			$table->addCell(8200, array('cellMerge' => 'continue'));
		}

		$table->addRow(400);
		$obj	= $table->addCell(400,array('rowMerge' => 'restart','valign'=>'center'));
		$obj->addText('委托人',$fm_fontstyle12,"pStyle");
		$table->addCell(1300)->addText('名称(姓名)：',$fm_fontstyle12);
		$table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText($cx_message['wtrname'],$fm_fontstyle12);
		for($i=0;$i<5;$i++)
		{
			$table->addCell(8200, array('cellMerge' => 'continue'));
		}

		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('通讯地址',$fm_fontstyle12);
		$table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText($cx_message['wtdw'],$fm_fontstyle12);
		for($i=0;$i<5;$i++)
		{
			$table->addCell(8200, array('cellMerge' => 'continue'));
		}

		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('邮政编码',$fm_fontstyle12);
		$table->addCell(1500)->addText($cx_message['lxryzbm'],$fm_fontstyle12);
		$table->addCell(1300)->addText('电子信箱',$fm_fontstyle12);
		$table->addCell(5400, array('cellMerge' => 'restart', 'valign' => "center"))->addText($cx_message['wtremail'],$fm_fontstyle12);
		for($i=0;$i<3;$i++)
		{
			$table->addCell(5400, array('cellMerge' => 'continue'));
		}

		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('负责人',$fm_fontstyle12);
		$table->addCell(1500)->addText($cx_message['wtrname'],$fm_fontstyle12);
		$table->addCell(1300)->addText('电话',$fm_fontstyle12);
		$table->addCell(2000)->addText($cx_message['wtrphone'],$fm_fontstyle12);
		$table->addCell(700)->addText('传真',$fm_fontstyle12);
		$table->addCell(2700, array('cellMerge' => 'restart', 'valign' => "center"))->addText($cx_message['search_role'],$fm_fontstyle12);
		$table->addCell(2700, array('cellMerge' => 'continue'));

		
		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('联系人',$fm_fontstyle12);
		$table->addCell(1500)->addText($cx_message['lxrname'],$fm_fontstyle12);
		$table->addCell(1300)->addText('电话1',$fm_fontstyle12);
		$table->addCell(2000)->addText($cx_message['phone'],$fm_fontstyle12);
		$table->addCell(700)->addText('QQ',$fm_fontstyle12);
		$table->addCell(2700, array('cellMerge' => 'restart', 'valign' => "center"))->addText($cx_message['show_add_pubdate'],$fm_fontstyle12);
		$table->addCell(2700, array('cellMerge' => 'continue'));


		$table->addRow(600);
		$obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
		$obj->addText('查新目的',$fontStyle);
		$obj->addTextBreak(1);
		$cxmd_str='';
		$cxmd_arr=explode(',',$cx_message['cxmd']);
		$cxmd_array = array( '1' => '科研立项',
							'2' => '鉴定、验收、评价',
							'3' => '奖励申报',
							'4' => '申请专利',
							'5' => '博硕士论文开题',
							'6' => '其他'
							);
		foreach($cxmd_array as $key => $value){
			if(in_array($key, $cxmd_arr)){
				$cxmd_str.= '■'.$value.'   ';
			}else{
				$cxmd_str.= '□'.$value.'   ';
			}
		}
		$obj->addText($cxmd_str,$fm_fontstyle12);
		
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}
		if($cx_message['cxmd_other'] != ''){
			$obj->addTextBreak(1);
			$obj->addText('其他查新目的：'.$cx_message['xxcxmd'],$fm_fontstyle12);
		}

		$table->addRow(600);
		$obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
		$obj->addText('查新范围',$fontStyle);
		$obj->addTextBreak(1);
		$cxfw_str='';
		$cxfw_array = array( '1' => '国内','3' => '国内外');
		foreach($cxfw_array as $key => $value){
			if($cx_message['cxfw'] == $key){
				$cxfw_str.= '■'.$value.'   ';
			}else{
				$cxfw_str.= '□'.$value.'   ';
			}
		}
		$obj->addText($cxfw_str,$fm_fontstyle12);
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}

		$table->addRow(600);
		$obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
		$obj->addText('是否加急',$fontStyle);
		$obj->addTextBreak(1);
		$sfjj_str='';
		$cxjj_array = array( '1' => '加急','2' => '不加急');
		foreach($cxjj_array as $key => $value){
			if($cx_message['sfjj'] == $key){
				$sfjj_str.= '■'.$value.'   ';
			}else{
				$sfjj_str.= '□'.$value.'   ';
			}
		}
		$obj->addText($sfjj_str,$fm_fontstyle12);
	
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}

		$table->addRow(600);
		$obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
		$obj->addText('查新要求',$fontStyle);
		$obj->addTextBreak(1);
		$cxyq_str='';
		$cxyq_array = array( '1' => '希望查新机构通过查新，证明在所查范围内国内外有无相同或类似研究','2' => '希望查新机构对查新项目分别或综合进行国内外对比分析','3' => '查新委托人提出的其他愿望');
		foreach($cxyq_array as $key => $value){
			if($cx_message['cxyq'] == $key){
				$cxyq_str.= '■'.$value.'   ';
			}else{
				$cxyq_str.= '□'.$value.'   ';
			}
		}
		$obj->addText($cxyq_str,$fm_fontstyle12);
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}
		
		if($cx_message['cxmd_other'] != ''){
			$obj->addTextBreak(1);
			$obj->addText('其他查新要求：'.$cx_message['show_bz_finish_pubdate'],$fm_fontstyle12);
		}


		$table->addRow(600);
		$obj	= $table->addCell(9900,array('cellMerge' => 'restart', 'valign' => "center"));
		$obj->addText('查新项目的科学技术要点',$fontStyle);
		$obj->addText('（简述项目的背景、技术问题、解决技术问题所采用的方案、主要技术特征、技术参数或指标、应用范围。）',$fm_fontstyle12);
		$obj->addText('主要技术特点：'.$cx_message['jsd'],$fm_fontstyle12);
		$obj->addTextBreak(1);
		
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}

		$table->addRow(600);
		$obj	= $table->addCell(9900,array('cellMerge' => 'restart', 'valign' => "center"));
		$obj->addText('查新点',$fontStyle);
		$obj->addText('(着重说明要求查证课题新颖性、创新性的部分。)',$fm_fontstyle12);
		$obj->addText('查新点：'.$cx_message['show_add_cxd'],$fm_fontstyle12);
		$obj->addTextBreak(1);
		
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}		$table->addRow(600);
		$obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
		$obj->addText('付款方式',$fontStyle);
		$obj->addTextBreak(1);
		$zd_str='';
		$cxfkfs_array = array( '1' => '网上支付','3' => '现金支付');
		foreach($cxfkfs_array as $key => $value){
			if($cx_message['fkfs'] == $key){
				$zd_str.= '■'.$value.'   ';
			}else{
				$zd_str.= '□'.$value.'   ';
			}
		}
		$obj->addText($zd_str,$fm_fontstyle12);
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}

		$table->addRow(600);
		$obj	= $table->addCell(9900,array('cellMerge' => 'restart', 'valign' => "center"));
		$obj->addText('取报告方式',$fontStyle);
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}
		$obj->addTextBreak(2);
		$qbfs_str='';
		$cxqbgfs_array = array( '1' => '自取','2' => '快递');
		foreach($cxqbgfs_array as $key => $value){
			if($cx_message['qbfs'] == $key){
				$qbfs_str.= '■'.$value.'   ';
			}else{
				$qbfs_str.= '□'.$value.'   ';
			}
		}
		$obj->addText($qbfs_str,$fm_fontstyle12);
		
		$table->addRow(600);
		$obj	= $table->addCell(9900,array('cellMerge' => 'restart', 'valign' => "center"));
		$obj->addText('保密、法律责任',$fontStyle);
		
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}
		$obj->addTextBreak(2);
		$obj->addText('查新人员对项目技术内容和资料，有保守秘密的责任，若有泄露，应由其承担法律责任。',$fm_fontstyle12);
		
		$obj->addText('委托人应保证提供的资料真实可靠，若有虚假，所产生的一切后果由其承担法律责任。',$fm_fontstyle12);


		$table->addRow(400);
		$obj	= $table->addCell(4100, array('cellMerge' => 'restart'));
		$obj->addText('受理人(签名): ',$fontStyle);
		for($i=0;$i<3;$i++)
		{
			$table->addCell(4100, array('cellMerge' => 'continue'));
		}
		$obj->addTextBreak(2);
		$obj->addText('年              月                日',$fontStyle2,'pStyle');
		$obj	= $table->addCell(4100, array('cellMerge' => 'restart'));
		$obj->addText('委托人(签名):'.$cx_message['wtrname'],$fontStyle);
		for($i=0;$i<3;$i++)
		{
			$table->addCell(4100, array('cellMerge' => 'continue'));
		}
		$obj->addTextBreak(2);
		$obj->addText('年              月                日',$fontStyle2,'pStyle');


		$objWriter = \PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		//$this->file_name = $path.'/'.$school_id.'_kjcx'.$cx_message['id'].'.docx';
		$outputFileName = time().'.docx';
        $outputFileName = iconv("utf-8","gb2312//IGNORE",$outputFileName);
        $filePath = "./uploads/report/".date("Ym");//要保存的文件夹路径
		
        if(!is_dir($filePath)){
			
            $filePath=makepath($filePath);
        }
        $upfile_file    = $filePath.'/'.$outputFileName;
		//var_dump($upfile_file);exit;
		$objWriter->save($upfile_file);
		return $upfile_file;
	}

	function getht($cx_message = array(),$path = '',$school_id = '',$zd_message=array())
	{
		vendor('PHPWord.PHPWord');
        $PHPWord = new \PHPWord();
        $section = $PHPWord->createSection();
		/******封面区域******/
		//添加段落样式(水平居中)
		$PHPWord->addParagraphStyle('pStyle', array('align'=>'center'));
		$fm_fontstyle	= array('color'=>'000000','bold'=>true,'size'=>34);
		$fm_fontstyle2	= array('color'=>'000000','bold'=>true,'size'=>22);
		$fm_fontstyle3	= array('color'=>'000000','size'=>22);
		$fm_fontstyle12	= array('color'=>'000000','size'=>11);



		/***********************正式内容开始**************************/
		//添加页脚
		$foot_fontstyle	= array('color'=>'000000','size'=>8);
		$title_fontstyle	= array('color'=>'000000','size'=>22);
		$footer = $section->createFooter();
		$table	= $footer->addTable();

		//放置页码并居中
		$table->addRow();
		$obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}
		
		$styleCell = array('valign'=>'center');
		$styleTable = array('borderSize'=>6, 'borderColor'=>'000000', 'cellMargin'=>80,'alignMent'=>'center');	
		$PHPWord->addTableStyle('myOwnTableStyle', $styleTable);
		$fontStyle	= array('color'=>'000000','bold'=>true,'size'=>13);
		$fontStyle2	= array('align'=>'center');
		
		$section->addText('合同编号：'.$cx_message['cxh']);
		$section->addTextBreak(2);
		$section->addText('河海大学科技查新合同',$title_fontstyle,"pStyle");
		$section->addTextBreak(3);
		
		$table = $section->addTable("myOwnTableStyle");
		
		//查询项目名称
		$table->addRow(400);
		$obj	= $table->addCell(1700,array('rowMerge' => 'restart','cellMerge' => 'restart','valign'=>'center'));
		$table->addCell(1700, array('cellMerge' => 'continue'));
		$obj->addText('查新项目名称',$fm_fontstyle12,"pStyle");
		$table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "left"))->addText('中文：'.$cx_message['xmmc'],$fm_fontstyle12);
		for($i=0;$i<5;$i++)
		{
			$table->addCell(8200, array('cellMerge' => 'continue'));
		}
		$table->addRow(400);
		$table->addCell(1700, array('rowMerge' => 'fusion','cellMerge' => 'restart'));
		$table->addCell(1700, array('cellMerge' => 'continue'));
		$table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "left"))->addText('英文：'.$cx_message['xmmcen'],$fm_fontstyle12);
		for($i=0;$i<5;$i++)
		{
			$table->addCell(8200, array('cellMerge' => 'continue'));
		}
		
		//委托人
		$table->addRow(400);
		$obj	= $table->addCell(400,array('rowMerge' => 'restart','valign'=>'center'));
		$obj->addText('委托人',$fm_fontstyle12,"pStyle");
		$table->addCell(1300)->addText('单位名称：',$fm_fontstyle12);
		$table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText($cx_message['wtdw'],$fm_fontstyle12);
		for($i=0;$i<5;$i++)
		{
			$table->addCell(8200, array('cellMerge' => 'continue'));
		}

		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('通讯地址',$fm_fontstyle12);
		$table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText($cx_message['txdz'],$fm_fontstyle12);
		for($i=0;$i<5;$i++)
		{
			$table->addCell(8200, array('cellMerge' => 'continue'));
		}

		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('邮政编码',$fm_fontstyle12);
		$table->addCell(1500)->addText($cx_message['lxryzbm'],$fm_fontstyle12);
		$table->addCell(1300)->addText('电子信箱',$fm_fontstyle12);
		$table->addCell(5400, array('cellMerge' => 'restart', 'valign' => "center"))->addText($cx_message['wtremail'],$fm_fontstyle12);
		for($i=0;$i<3;$i++)
		{
			$table->addCell(5400, array('cellMerge' => 'continue'));
		}

		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('负责人',$fm_fontstyle12);
		$table->addCell(1500)->addText($cx_message['wtrname'],$fm_fontstyle12);
		$table->addCell(1300)->addText('电话',$fm_fontstyle12);
		$table->addCell(2000)->addText($cx_message['wtrphone'],$fm_fontstyle12);
		$table->addCell(700)->addText('传真',$fm_fontstyle12);
		$table->addCell(2700, array('cellMerge' => 'restart', 'valign' => "center"))->addText($cx_message['search_role'],$fm_fontstyle12);
		$table->addCell(2700, array('cellMerge' => 'continue'));

		
		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('联系人',$fm_fontstyle12);
		$table->addCell(1500)->addText($cx_message['lxrname'],$fm_fontstyle12);
		$table->addCell(1300)->addText('电话',$fm_fontstyle12);
		$table->addCell(2000)->addText($cx_message['phone'],$fm_fontstyle12);
		$table->addCell(700)->addText('传真',$fm_fontstyle12);
		$table->addCell(2700, array('cellMerge' => 'restart', 'valign' => "center"))->addText($cx_message['show_add_pubdate'],$fm_fontstyle12);
		$table->addCell(2700, array('cellMerge' => 'continue'));

		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('付款方式',$fm_fontstyle12);
		
		$zd_str = '';
		$cxfkfs_array = array( '1' => '网上支付','3' => '现金支付','4' => '校内转账');
		foreach($cxfkfs_array as $key => $value){
			if($cx_message['fkfs'] == $key){
				$zd_str.= $value.'（√ ）'.' ';
			}else{
				$zd_str.= $value.'（ ）';
			}
		}
		
		$table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText("{$zd_str}汇款后请将汇款凭证扫描件发至kejcx@163.com",$fm_fontstyle12);
		for($i=0;$i<5;$i++)
		{
			$table->addCell(8200, array('cellMerge' => 'continue'));
		}
		
		
				
		//查新机构
		$table->addRow(400);
		$obj	= $table->addCell(400,array('rowMerge' => 'restart','valign'=>'center'));
		$obj->addText('查新机构',$fm_fontstyle12,"pStyle");
		$table->addCell(1300)->addText('机构名称：',$fm_fontstyle12);
		$table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText('教育部科技查新工作站G02',$fm_fontstyle12);
		for($i=0;$i<5;$i++)
		{
			$table->addCell(8200, array('cellMerge' => 'continue'));
		}

		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('通信地址',$fm_fontstyle12);
		$table->addCell(8200, array('cellMerge' => 'restart', 'valign' => "center"))->addText('南京市西康路1号',$fm_fontstyle12);
		for($i=0;$i<5;$i++)
		{
			$table->addCell(8200, array('cellMerge' => 'continue'));
		}
		$nam = $emai = $lxrr = $dianh = '';
		if($cx_message['cx_user_name'] > 0 ){
			$rr  = M('user')->where(['id'=>$cx_message['cx_user_name']])->find();
			$nam = $rr['username'];
			if($rr['sarea'] == '10'){
				
				//$emai = 'hhuxxb@hhu.edu.cn';
				$emai = 'tsgxxb@hhu.edu.cn';
				
				$lxrr = $dianh = '025-883787301';
			}elseif($rr['sarea'] == '11'){
				$emai = 'kejcx@163.com';
				$lxrr = $dianh = '0519-85191970';
			}
			
		}
		

		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('邮政编码',$fm_fontstyle12);
		$table->addCell(1500)->addText('210098',$fm_fontstyle12);
		$table->addCell(1300)->addText('电子信箱',$fm_fontstyle12);
		$table->addCell(5400, array('cellMerge' => 'restart', 'valign' => "center"))->addText($emai,$fm_fontstyle12);
		for($i=0;$i<3;$i++)
		{
			$table->addCell(5400, array('cellMerge' => 'continue'));
		}

		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('负责人',$fm_fontstyle12);
		$table->addCell(1500)->addText('吴东敏',$fm_fontstyle12);
		$table->addCell(1300)->addText('电话',$fm_fontstyle12);
		$table->addCell(2000)->addText('',$fm_fontstyle12);
		$table->addCell(700)->addText('传真',$fm_fontstyle12);
		$table->addCell(2700, array('cellMerge' => 'restart', 'valign' => "center"))->addText('',$fm_fontstyle12);
		$table->addCell(2700, array('cellMerge' => 'continue'));

		
		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('联系人',$fm_fontstyle12);
		$table->addCell(1500)->addText($nam,$fm_fontstyle12);
		$table->addCell(1300)->addText('电话',$fm_fontstyle12);
		$table->addCell(2000)->addText($lxrr,$fm_fontstyle12);
		$table->addCell(700)->addText('传真',$fm_fontstyle12);
		$table->addCell(2700, array('cellMerge' => 'restart', 'valign' => "center"))->addText($lxrr,$fm_fontstyle12);
		$table->addCell(2700, array('cellMerge' => 'continue'));
	
		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('开 户 行',$fm_fontstyle12);
		$table->addCell(2800)->addText('中国工商银行南京宁海路支行',$fm_fontstyle12);
		$table->addCell(2000)->addText('户   名',$fm_fontstyle12);
		$table->addCell(5400, array('cellMerge' => 'restart', 'valign' => "center"))->addText('河海大学',$fm_fontstyle12);
		for($i=0;$i<3;$i++)
		{
			$table->addCell(5400, array('cellMerge' => 'continue'));
		}
		
		$table->addRow(400);
		$table->addCell(400, array('rowMerge' => 'fusion'));
		$table->addCell(1300)->addText('账   号',$fm_fontstyle12);
		$table->addCell(2800)->addText('4301011409001024513',$fm_fontstyle12);
		$table->addCell(2000)->addText('用   途',$fm_fontstyle12);
		$table->addCell(5400, array('cellMerge' => 'restart', 'valign' => "center"))->addText('查新复印费',$fm_fontstyle12);
		for($i=0;$i<3;$i++)
		{
			$table->addCell(5400, array('cellMerge' => 'continue'));
		}
		
		$section->addTextBreak(3);
		
		$section->addText("    依据《中华人民共和国合同法》的规定，查新合同双方  {$cx_message['xmmc']}  项目的查新事务，经协商一致，订立本合同。",$fm_fontstyle12);
		$section->addTextBreak(3);
		
//==第二页==
		$table = $section->addTable("myOwnTableStyle");
		
		$table->addRow(1200);
		$obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
		$obj->addText('一、查新目的及范围',$fontStyle);
		$obj->addText('	查新目的：',array('color'=>'000000','bold'=>true,'size'=>10));
		$cxmd_str='';
		$cxmd_arr=explode(',',$cx_message['cxmd']);
		$cxmd_array = array( '1' => '科研立项',
							'2' => '鉴定、验收、评价',
							'3' => '奖励申报',
							'4' => '申请专利',
							'5' => '博硕士论文开题',
							'6' => '其他'
							);
		foreach($cxmd_array as $key => $value){
			if(in_array($key, $cxmd_arr)){
				$cxmd_str.=$value. '（√）'.'  ';
			}else{
				$cxmd_str.= $value. '（）'.'  ';
			}
		}
		$obj->addText('	'.$cxmd_str);		
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}
		if($cx_message['cxmd_other'] != ''){
			$obj->addTextBreak(1);
			$obj->addText('	其他查新目的：'.$cx_message['xxcxmd']);
		}
		$obj->addTextBreak(1);
		$obj->addText('	查新范围：',array('color'=>'000000','bold'=>true,'size'=>10));
		$cxfw_str='';
		$cxfw_array = array( '1' => '国内','3' => '国内外');
		foreach($cxfw_array as $key => $value){
			if($cx_message['cxfw'] == $key){
				$cxfw_str.= $value. '（√）'.'  ';
			}else{
				$cxfw_str.= $value. '（）'.'  ';
			}
		}
		$obj->addText('	'.$cxfw_str);
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}

		
		
		
		
		//二二二二二二
		$table->addRow(1200);
		$obj	= $table->addCell(9900, array('cellMerge' => 'restart', 'valign' => "center"));
		$obj->addText('二、查新点和查新要求',$fontStyle);
		$obj->addText('（指需查证的创新点，控制在3点之内；查新点描述应简明扼要，且具对比性，详细描述可在科技要点中展开；常规技术不应作为查新点。）');	
		$obj->addTextBreak(1);
		$obj->addText('	查新点：');
		$obj->addText('	'.$cx_message['show_add_cxd']);
		$cxyq_str='';
		$cxyq_array = array( 
			'1' => '希望查新机构通过查新，证明在所查范围内国内外有无相同或类似研究',
			'2' => '希望查新机构对查新项目分别或综合进行国内外对比分析',
			'3' => '查新委托人提出的其他愿望'
		);
		foreach($cxyq_array as $key => $value){
			if($cx_message['cxyq'] == $key){
				$cxyq_str = $value;
			}
		}		
		$obj->addTextBreak(1);
		$obj->addText('	查新要求：'.$cxyq_str);
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}	
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}		
		if($cx_message['cxmd_other'] != ''){
			$obj->addTextBreak(1);
			$obj->addText('	其他查新要求：'.$cx_message['show_bz_finish_pubdate']);
		}

		//三三三三三三
		$table->addRow(1200);
		$obj	= $table->addCell(9900,array('cellMerge' => 'restart', 'valign' => "center"));
		$obj->addText('三、查新项目的科学技术要点',$fontStyle);
		$obj->addText('（着重说明项目的技术背景、主要科技特征、技术参数或指标、应用范围等，并对查新点作较为详细的描述，可加附页。）');
		$obj->addTextBreak(1);		
		$obj->addText('	主要技术特点：'.$cx_message['jsd'],$fm_fontstyle12);
		$obj->addTextBreak(1);
		$obj->addText('	关键词：'.$cx_message['keywords_cn'],$fm_fontstyle12);
		$obj->addTextBreak(1);		
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}
		for($i=0;$i<7;$i++)
		{
			$table->addCell(9900, array('cellMerge' => 'continue'));
		}
		


//==结尾签字==
		$section->addTextBreak(2);
        $styleTable1 = array( 'cellMargin'=>80,'alignMent'=>'center');
        $PHPWord->addTableStyle('myOwnTableStyle1', $styleTable1);
		$table = $section->addTable("myOwnTableStyle1");
		$table->addRow(400);
		$obj	= $table->addCell(4950, array('cellMerge' => 'restart'));
		$obj->addText('委托人（盖章）： '.$cx_message['wtrname'],$fm_fontstyle12);
		for($i=0;$i<3;$i++)
		{
			$table->addCell(4950, array('cellMerge' => 'continue'));
		}
		$obj->addTextBreak(1);
		$obj->addText('代  表（签字）：',$fm_fontstyle12);
		$obj->addTextBreak(1);
		$obj->addText('订立地点：',$fm_fontstyle12);
		
		$obj	= $table->addCell(4950, array('cellMerge' => 'restart'));
		$obj->addText('查新机构（盖章）：',$fm_fontstyle12);
		for($i=0;$i<3;$i++)
		{
			$table->addCell(4950, array('cellMerge' => 'continue'));
		}
		$obj->addTextBreak(1);
		$obj->addText('代  表（签字）：',$fm_fontstyle12);
		$obj->addTextBreak(1);
		$obj->addText('订立日期：'.$cx_message['intime'],$fm_fontstyle12);

		
//导出文档
		$objWriter = \PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		$outputFileName = time().'.docx';
        $outputFileName = iconv("utf-8","gb2312//IGNORE",$outputFileName);
        $filePath = "./uploads/report/".date("Ym");//要保存的文件夹路径
		
        if(!is_dir($filePath)){
			
            $filePath=makepath($filePath);
        }
        $upfile_file    = $filePath.'/'.$outputFileName;
		$objWriter->save($upfile_file);
		return $upfile_file;
	}

}