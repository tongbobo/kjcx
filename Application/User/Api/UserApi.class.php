<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-17
 * Time: 下午4:20
 */
namespace User\Api;
use User\Api\Api;
use User\Model\UserModel;

class UserApi extends Api{
    protected function _init(){
        $this->model = new UserModel();
    }
    /*
     *用户注册
     */
    public function register($username,$password,$email,$tel){
        return $this->model->register($username,$password,$email,$tel);
    }
    /*
     *用户注册
     */
    public function register1($depart,$password){
        return $this->model->register1($depart,$password);
    }
    /*
     * 用户登录认证
     */
    public function login($username, $password){
        return $this->model->login($username, $password);
    }
    /*
     * 获取用户个人信息
     */
    public function info($uid, $is_username = false){
        return $this->model->info($uid, $is_username);
    }

    /*
     * 修改用户个人信息
     */
    public function updateInfo($uid, $data){
        return $this->model->updateUserFields($uid, $data);
    }
    /*
     * 添加用户个人信息
     */
    public function addInfo($uid, $data){
        return $this->model->addInfo($uid, $data);
    }
    /*
    * 得到该机构的用户信息
    */
    public function selectInfo ($where=array(),$field=true){
        return $this->model->selectInfo($where, $field);
    }
    public function selectInfo1 ($where=array(),$field=true){
        return $this->model->selectInfo1($where, $field);
    }
    /*
     * 检测用户名是否已经注册
     */
    public function checkUsername($username){
        return $this->model->checkUsername($username);
    }

}