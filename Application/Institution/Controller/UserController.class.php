<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-23
 * Time: 上午9:18
 */
namespace Institution\Controller;
use Think\Controller;
use User\Api\UserApi;
use User\Model\UserModel;

class UserController extends AdminController{
    /*
     * 用户首页
     */
    public function index(){
        $nickname = I('nickname');
        $map['status'] = array('egt',0);
        $map['nickname']    =   array('like', '%'.(string)$nickname.'%');
//        $list = $this->lists('Member', $map);
        $this->meta_title = '用户信息';
//        $this->assign('_list', $list);
        $this->display();
    }

    /*
     * 机构用户修改密码
     */
    public function changePwd(){
        if(IS_POST){
            $password = I('post.old');
            empty($password) && $this->error('请输入原始密码');
            if(!$this->checkpwd($password)){
                $this->error('原始密码输入不正确');
            }
            $data['password'] = I('post.pwd');
            empty($data['password']) && $this->error('请输入新密码');
            $repassword = I('post.repwd');
            empty($repassword) && $this->error('请输入确认密码');
            if($data['password'] !== $repassword){
                $this->error('两次输入的密码不一样');
            }
            $data['password'] = md5($data['password']);
            $user = new UserApi();
            $res= $user->updateInfo(UID, $data);
            if($res){
                $this->success('密码已经修改成功',__APP__.'/Institution/Public/logout');
            }else{
                $this->error('密码修改失败');
            }

        }else{
            $this->assign('meta_title','修改密码');
            $this->display();
        }
    }

    /**
     * 检查原始密码是否正确
     * @param $password
     * @return bool
     */
    function checkpwd($password){
        $userModel = new UserModel();
        $map['id'] = UID;
        $userInfo = $userModel->where($map)->find();
        if($userInfo){
            if(md5($password)==$userInfo['password']){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    /*
     * 机构用户添加
     */
    public function unitAdd(){
        if(I('type'))
        {
            $this->assign('type',I('type'));
        }
        $user = M('User');
        $department = $user->find(UID);
        $map['module']='Institution';
        $map['status']=1;
        $map['sid']=$department['department'];
        $group=M('auth_group')->where($map)->select();
		$departmentId = M('User')->where("id=".UID)->getField('department');		
		$map12['school_id'] = $departmentId;
		$sarea = M('school_area')->where($map12)->select();
        if(IS_POST){
            $data = I('post.');
            $array = array(
                'department'=>$department['department'],
                'username'=>$data['username'],
                'password'=>md5('123456'),
                'name'=>$data['name'],
                'city'=>$data['city'],
                'address'=>$data['address'],
                'tel'=>$data['tel'],
                'email'=>$data['email'],
                'typeid'=>$data['typeid'],
                'status'=>'1'
            );
            $uid=D('User')->userAdd($array);
            if(0<$uid){
                $u_g['uid']=$uid;
                $u_g['group_id']=$data['typeid'];
                if( M('auth_group_access')->add($u_g)){
                    if(I('type'))
                    {
                        $this->success('添加成功', Cookie('__forward__'));
                    }
                    else
                    {
                        $this->success('添加成功',U('Unit/user'));
                    }
                }else{
                    $this->error('用户没有添加进用户组');
                }
            }else{
                $this->error($this->showRegError($uid));
            }
        }
		$this->assign('sarea', $sarea);
        $this->assign('meta_title', '人员管理');
        $this->assign('group', $group);
        $this->display();
    }

    public function unitEdit(){
		
        $department = M('User')->where('id='.UID)->getField('department');
        $map['module']='Institution';
        $map['status']=1;
        $map['sid']=$department;
        $group=M('auth_group')->where($map)->select();
        $User = M('User');
		$departmentId = M('User')->where("id=".UID)->getField('department');		
		$map12['school_id'] = $departmentId;
		$sarea = M('school_area')->where($map12)->select();
		
        if(IS_POST){
            $data = I('post.');
			if($data['tel'] == ''){
				$this->error('联系方式不能为空');
			}
			if($data['email'] == ''){
				$this->error('Email不能为空');
			}
            if($res = D('User')->userUpdate($data)){
                $this->success('修改成功',U('Unit/user'));
            }else{
                $this->error('修改失败');
            }
        }else{
            $id = I('get.id');
            $info = $User->find($id);
            $this->assign('info', $info);
            $this->assign('sarea', $sarea);
            $this->assign('group', $group);
            $this->assign('meta_title', '人员管理');
            $this->display('unitAdd');
        }
    }

    //得到用户信息
    public function info(){
        $id = I('get.id');
        $User = M('User');
        if($info = $User->find($id)){
            $this->assign('info', $info);
            $this->display();
        }else{
            $this->error('对不起，无效参数，没有查找到用户');
        }
    }
    public function changeStatus($method,$id){
        $map['id'] = $id;
        switch($method){
            case 'forbidUser':
                $r = $this->forbidUser1($id);
                if($r>0){
                    $this->success('状态禁用成功',U('Unit/user'));
                }else{
                    if($r=='-1'){
                        $this->error('该用户不存在');
                    }else{
                        $this->error('状态禁用失败');
                    }
                }
                break;
            case 'resumeUser':
                $r = $this->resumeUser1($id);
                if($r>0){
                    $this->success('启用用户成功',U('Unit/user'));
                }else{
                    if($r=='-1'){
                        $this->error('该用户不存在');
                    }else{
                        $this->error('启用用户失败');
                    }
                }
                break;
            default :
                $this->error('非法参数');
        }
    }
    /*
     * 禁用用户
     * $model 模块
     * $map条件
     */
    protected function forbidUser($model, $map){
        $data['status'] = 0;
        $re = M()->where('id='.$data['id'])->find();
        if(!re){
            $this->error('该用户不存在');
        }
        else
        {
            $res = M($model)->where($map)->data($data)->save();
            if($res){
//            action_log($data['id'],'用户被')
                $this->success('状态禁用成功');
            }else{
                $this->error('状态禁用失败');
            }
        }

    }
    //2016.5.4  禁用用户
    protected function forbidUser1($id){
        $data['status'] = 0;
        $user   = M('User');
        $re     = $user->find($id);
        if(!$re['id']){
            return -1;
        }else{
            $res    = $user->where('id='.$re['id'])->save($data);
            if($res){
                return 1;
            }else{
                return -2;
            }
        }

    }
    //2016.5.4  启用用户
    protected function resumeUser1($id){
        $data['status'] =1;
        $user   = M('User');
        $re     = $user->find($id);
        if(!$re['id']){
            return -1;
        }else{
            $res    =  $user->where('id='.$re['id'])->save($data);
            if($res){
                return 1;
            }else{
                return -2;
            }
        }

    }

    /*
     * 删除用户
     */
    protected function delete($model,$map){
        $res = M($model)->where($map)->delete();
        if($res){
            $this->success('用户删除成功');
        }else{
            $this->error('用户删除失败');
        }
    }

    /*
     * 启用用户
     */
    protected function resumeUser($model,$map){
        $data['status'] =1;
        $re = M()->where('id='.$data['id'])->find();
        if(!re){
            $this->error('该用户不存在');
        }
        else
        {
            $res = M($model)->where($map)->data($data)->save();
            if ($res) {
                $this->success('启用用户成功');
            } else {
                $this->error('启用用户失败');
            }
        }
    }

    private function showRegError($code = 0){
        switch ($code) {
            case -1:  $error = '用户名长度必须在16个字符以内！'; break;
            case -2:  $error = '用户名被禁止注册！'; break;
            case -3:  $error = '用户名被占用！'; break;
            case -4:  $error = '密码长度必须在6-30个字符之间！'; break;
            case -5:  $error = '邮箱格式不正确！'; break;
            case -6:  $error = '邮箱长度必须在1-32个字符之间！'; break;
            case -7:  $error = '邮箱被禁止注册！'; break;
            case -8:  $error = '邮箱被占用！'; break;
            case -9:  $error = '手机格式不正确！'; break;
            case -10: $error = '手机被禁止注册！'; break;
            case -11: $error = '手机号被占用！'; break;
            case -12: $error = '机构名称不能为空！'; break;
            case -13: $error = '通讯地址不能为空！'; break;
            case -14: $error = '请填写挂靠单位'; break;
            case -15: $error = '请填写城市'; break;
            case -16: $error = '姓名长度必须在15个字符以内'; break;
            default:  $error = '未知错误';
        }
        return $error;
    }
}