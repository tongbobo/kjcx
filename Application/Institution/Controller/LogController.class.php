<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/6/26
 * Time: 14:40
 */
namespace Institution\Controller;
use Think\Controller;
class LogController extends AdminController{
    public function index(){
        $map['user_id'] = UID;
        $Log = M('ActionLog');
        $list = $this->lists($Log,$map);
        $this->assign('_list', $list);
        $this->assign('meta_title', '日志');
        $this->display('index');
    }
}