<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/7/2
 * Time: 19:38
 */
namespace Institution\Model;
use Think\Model;
class AuthGroupAccessModel extends Model{
    public function addGroup($uid,$groupId){
        if($uid && $groupId){
            $this->create();
            $data = array(
                'uid'=>$uid,
                'group_id'=>$groupId
            );
            $id = $this->add($data);
            return $id;
        }
    }
}