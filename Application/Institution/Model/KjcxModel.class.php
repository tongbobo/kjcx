<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 2015/8/28
 * Time: 14:35
 */
namespace Institution\Model;
use Think\Model;

class KjcxModel extends Model{
    /*
     * 抢单模式时候的抢单
     * @id 科技查新ID号
     * @department_id  查新机构ID号
     */
    public function call($id,$department_id){
        $data = array(
            'id'=>$id,
            'process_id'=>20,
            'department_id'=>$department_id
        );
        return  $this->save($data);
    }

    public function checkStatus($id){
        $where = array(
            'id'=>$id,
            'process_id'=>-2
        );
        $res = $this->where($where)->select();
        if($res){
            return false;
        }else{
            return true;
        }
    }
}