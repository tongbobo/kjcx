<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-6-17
 * Time: 下午4:45
 */
/*
 * 检查验证码
 */
function check_verify($code, $id = 1){
    $verify = new \Think\Verify();
    return $verify->check($code, $id);
}

/*
   * 检测是否是机构管理员
   */
function check_admin($id){
    $map['uid'] = $id;
    $res = M('Department')->where($map)->find();
    if($res){
        return true;
    }else{
        return false;
    }
}


//获得流程名称
function get_process_name($id){
    $map['id'] = $id;
    $res = M('CxProcess')->where($map)->find();
    if($res){
        return $res['title'];
    }else{
        return '未知';
    }
}
//获得流程名称
function get_process_name1($id){
    $map['process_id'] = $id;
    $res = M('CxProcess')->where($map)->find();
    if($res){
        return $res['title'];
    }else{
        return '未知';
    }
}