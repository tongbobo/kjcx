<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

namespace Common\Api;
class ConfigApi {
    /**
     * 获取数据库中的配置列表
     * @return array 配置数组
     */
    public static function lists(){
        $map    = array('status' => 1);
        $data   = M('Config')->where($map)->field('type,name,value')->select();
        
        $config = array();
        if($data && is_array($data)){
            foreach ($data as $value) {
                $config[$value['name']] = self::parse($value['type'], $value['value']);
            }
        }
        return $config;
    }
    /**
     * 获取数据库中的option列表
     * @return array 配置数组
     */
    public static function option_lists(){
        $map    = array('type' => 1);
        $data   = M('option')->where($map)->field('desc,type,sort,name,value')->select();
        $data_2 =M('option')->where(array('type'=>'2'))->field('desc,type,name,value,start_day,end_day')->select();

        $option = array();
        if($data && is_array($data)){
            foreach ($data as $value) {
                $option[1][$value['name']] = self::parse($value['type'], $value);
            }
        }
        if($data_2 && is_array($data_2)){//下标为2的学校设置
            foreach ($data_2 as $value) {
                $option[2][$value['value']] = self::parse($value['type'], $value);
            }
        }
        return $option;
    }
    /**
     * 根据配置类型解析配置
     * @param  integer $type  配置类型
     * @param  string  $value 配置值
     */
    private static function parse($type, $value){
        switch ($type) {
            case 3: //解析数组
                $array = preg_split('/[,;\r\n]+/', trim($value, ",;\r\n"));
                if(strpos($value,':')){
                    $value  = array();
                    foreach ($array as $val) {
                        list($k, $v) = explode(':', $val);
                        $value[$k]   = $v;
                    }
                }else{
                    $value =    $array;
                }
                break;
        }
        return $value;
    }
}