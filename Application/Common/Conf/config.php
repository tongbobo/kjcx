<?php
return array(
	//'配置项'=>'配置值'
    'DB_TYPE'   => 'mysql', // 数据库类型
    'DB_HOST'   => 'localhost', // 服务器地址
    'DB_NAME'   => 'kjcx', // 数据库名
    'DB_USER'   => 'root', // 用户名
    'DB_PWD'    => 'root', // 密码
    'DB_PORT'   => 3306, // 端口
    'DB_PREFIX' => 'k_', // 数据库表前缀
    'DB_CHARSET'=> 'utf8', // 字符集
    'DB_DEBUG'  =>  false, //

    //用户相关设置
    'USER_ADMINISTRATOR' => 1, //管理员用户ID
//    'SHOW_PAGE_TRACE' =>true,
    'LOG_RECORD' =>false,
    'LOG_TYPE'              =>  'File', // 日志记录类型 默认为文件方式
    'LOG_LEVEL'             =>  'EMERG,ALERT,CRIT,ERR',// 允许记录的日志级别
    'LOG_FILE_SIZE'         =>  2097152,	// 日志文件大小限制
    'LOG_EXCEPTION_RECORD'  =>  false,
    //邮件配置
    'THINK_EMAIL' => array(
        'SMTP_HOST'   => 'smtp.qq.com', //SMTP服务器
        'SMTP_PORT'   => '25', //SMTP服务器端口
        'SMTP_USER'   => '923392881@qq.com', //SMTP服务器用户名
        'SMTP_PASS'   => 'xiong19A', //SMTP服务器密码
        'FROM_EMAIL'  => '923392881@qq.com', //发件人EMAIL
        'FROM_NAME'   => '英年早肥', //发件人名称
        'REPLY_EMAIL' => '', //回复EMAIL（留空则为发件人EMAIL）
        'REPLY_NAME'  => '', //回复名称（留空则为发件人名称）
    ),
    //开启子域名部署
	/*
    'APP_SUB_DOMAIN_DEPLOY'   =>    1,
	
    'APP_SUB_DOMAIN_RULES'    =>    array(
        //'admin.plat.com'  => 'Admin',
        'plat.com'   => '',
        'server.plat.com'=>'Institution'
    ),*/
    //异常模板
//    'TMPL_EXCEPTION_FILE' => '404.html',
    //
);
